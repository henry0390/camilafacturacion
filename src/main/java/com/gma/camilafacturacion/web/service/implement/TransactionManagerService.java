/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.implement;

import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

/**
 *
 * @author Ing. Carlos Loor
 */
@Singleton
@Lock(LockType.READ)
public class TransactionManagerService implements EntityManagerLocal {

    @PersistenceContext(unitName = "camilafacturacion")
    private EntityManager em;

    @Override
    public EntityManager getEm() {
        return em;
    }

    @Override
    public <T> T find(Class<T> entity, Object id) {
        T o = null;
        try {
            o = em.find(entity, id);
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public <T> T findObject(Class<T> entity, Map<String, Object> parametros) {
        T o = null;
        try {
            Session sess = em.unwrap(Session.class);
            Criteria cq = sess.createCriteria(entity);
            cq.add(Restrictions.allEq(parametros));
            o = (T) cq.uniqueResult();
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public <T> List<T> findAllByParameter(Class entity, Map<String, Object> parametros, String order) {
        List<T> lo = null;
        Map<String, Object> p = new HashMap<>();
        List<Criterion> lc = new ArrayList();
        try {
            Session sess = em.unwrap(Session.class);
            Criteria cq = sess.createCriteria(entity);
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        lc.add(Restrictions.in(entry.getKey(), (Collection) entry.getValue()));
                    } else {
                        p.put(entry.getKey(), entry.getValue());
                    }
                });
                cq.add(Restrictions.allEq(p));
                if (!lc.isEmpty()) {
                    lc.forEach((c) -> {
                        cq.add(c);
                    });
                }
            }
            if (order != null && order.length() > 0) {
                cq.addOrder(Order.asc(order));
            }
            lo = (List<T>) cq.list();
            lo.size();
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lo;
    }

    @Override
    public Object persist(Object object) {
        Object o = null;
        try {
            o = em.merge(object);
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public Object findObject(String tipoQuery, String query, Map<String, Object> parametros, Class modelo, Boolean requireTransaction) {
        Object o = null;
        try {
            Session sess = em.unwrap(Session.class);
            org.hibernate.query.Query q;
            if (tipoQuery.equalsIgnoreCase("SQL")) {
                q = sess.createSQLQuery(query);
            } else//HQL
            {
                q = sess.createQuery(query).setMaxResults(1);
            }
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));
            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        if (tipoQuery.equalsIgnoreCase("SQL")) {
                            q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                        } else {
                            q.setParameter(entry.getKey(), entry.getValue());
                        }
                    }
                });
            }
            o = (Object) q.getSingleResult();
            Hibernate.initialize(o);
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public List findAllSQL(String query, Map<String, Object> parametros, Class modelo) {
        List l = null;
        try {
            Session sess = em.unwrap(Session.class);
            org.hibernate.query.Query q = sess.createSQLQuery(query);
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));

            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            l = q.getResultList();
        } catch (Exception e) {
            Logger.getLogger(TransactionManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return l;
    }
}
