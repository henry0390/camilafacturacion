/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.local;

import com.gma.camilafacturacion.web.models.datasource.CxctDocumento;
import com.gma.camilafacturacion.web.models.datasource.CxptComprobanteRetencion;
import com.gma.camilafacturacion.web.models.datasource.FactCabventa;
import com.gma.camilafacturacion.web.models.datasource.InvtCabplandes;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Acer
 */
@Local
public interface AutorizacionDataSourceLocal {
    public void cargarDatos(Date desde, Date hasta);
    
    public List<FactCabventa> getFacturas(Date desde, Date hasta);
    public List<FactCabventa> getFacturas(String ids);
    public List<CxptComprobanteRetencion> getRetenciones(Date desde, Date hasta);
    public List<InvtCabplandes> getGuiasRemision(Date desde, Date hasta);
    public List<CxctDocumento> getNotasCredito(Date desde, Date hasta);
    
    public List<FactCabventa> consultarXmlFacturas(List<FactCabventa> facturas);
    public List<CxptComprobanteRetencion> consultarXmlRetenciones(List<CxptComprobanteRetencion> retenciones);
    public List<InvtCabplandes> consultarXmlGuiasRemision(List<InvtCabplandes> guiasRemision);
    public List<CxctDocumento> consultarXmlNotasCredito(List<CxctDocumento> notasCredito);
    
    public void actualizarFactura(int secFactura, String autorizacion, Date fechaAutorizacion);
    public void actualizarRetencion(String secDocumento, String autorizacion, Date fechaAutorizacion);
    public void actualizarGuiaRemision(String secDocumento, String autorizacion, Date fechaAutorizacion);
    public void actualizarNotaCredito(String secDocumento, String autorizacion, Date fechaAutorizacion);
    public void actualizarFactura(String claveAcceso, String autorizacion, Date fechaAutorizacion);
}
