/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.local;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import java.util.List;
import java.util.concurrent.Future;
import javax.ejb.Local;
import javax.faces.context.ExternalContext;

/**
 *
 * @author Acer
 */
@Local
public interface FacturacionLocal {
    
    public void descargarDocumentoPdf(TbAutorizados documento, Boolean generar, ExternalContext externalContext);
    public void generarDocumentoPdf(String claveAcceso, String documento, String logo, String reporte);
    public void descargarDocumentoXml(TbAutorizados documento);
    public void generarXmlBuilder(TbAutorizados documento);
    public void enviarCorreo(String correo, String subject, String detalle,String ruta, String nombreArchivo);
    public Future<Integer> envioCorreos(List<TbAutorizados> docAutorizados);
    public DocumentoAutorizado consultarDocumentoAutorizado(String ruta);
    public String consultarTextDocumentoAutorizado(DocumentoAutorizado docAutorizado);    
}
