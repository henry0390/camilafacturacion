/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.implement;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.EmailRunnable;
import com.gma.camilafacturacion.util.Propiedades;
import com.gma.camilafacturacion.util.Utilidades;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.io.IOUtils;
import java.io.StringWriter;
import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.faces.context.ExternalContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Acer
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class FacturacionService implements FacturacionLocal {

    @Override
    public void descargarDocumentoPdf(TbAutorizados documento, Boolean generar, ExternalContext externalContext) {
        HttpServletResponse response;
        OutputStream outputStream = null;
        OutputStream outputStream2 = null;
        Map<String, Object> parametros = new HashMap<>();
        
        try {
            DocumentoAutorizado docAut = Utilidades.getObjectByString(DocumentoAutorizado.class, documento.getTbDocumentoXML().getDocumentoXML());
            docAut.cargarComprobante();
            List<DocumentoAutorizado> lf = new ArrayList<>();
            lf.add(docAut);
            JRDataSource dataSource = new JRBeanCollectionDataSource(lf);
            
            parametros.put("LOGO", SisVar.rutaLogo.replaceAll("empresa", documento.getEmpresaId().getIdentificacion()) + documento.getEmpresaId().getIdentificacion().trim() + ".png");
            parametros.put("SUBREPORT_DIR", SisVar.rutaReportes);
            parametros.put("FECHA_AUTORIZADO", Utilidades.fechaFormatoDeFechaString(SisVar.tipo, docAut.getFechaAutorizacion()));
            byte[] bytes = JasperExportManager.exportReportToPdf(
                    JasperFillManager.fillReport(
                            SisVar.rutaReportes + (documento.getDocumento().equalsIgnoreCase("FAC") ? "factura"
                            : documento.getDocumento().equalsIgnoreCase("RET") ? "retencion"
                            : documento.getDocumento().equalsIgnoreCase("NCR") ? "notaCredito"
                            : documento.getDocumento().equalsIgnoreCase("GUI") ? "guiaRemision" : "factura") + ".jasper",
                            parametros,
                            dataSource
                    )
            );
            if (bytes == null) {
                return;
            }
            if (generar) {
                outputStream2 = new FileOutputStream(SisVar.rutaConsulta.replaceAll("empresa", documento.getEmpresaId().getIdentificacion()) + documento.getClaveAcceso() + ".pdf");
                outputStream2.write(bytes, 0, bytes.length);
            } else {
                response = (HttpServletResponse) externalContext.getResponse();
                response.reset();
                response.setContentType("application/pdf");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-disposition", "attachment; filename=" + documento.getDocumento() + "_" + docAut.getNumeroDocumento() + ".pdf");
                response.setContentLength(bytes.length);
                outputStream = response.getOutputStream();
                outputStream.write(bytes, 0, bytes.length);
            }
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (outputStream2 != null) {
                try {
                    outputStream2.close();
                } catch (IOException ex) {
                    Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (!generar) {
                FacesContext.getCurrentInstance().responseComplete();
            }
        }
    }

    @Override
    public void generarDocumentoPdf(String claveAcceso, String documento, String logo, String reporte) {
        OutputStream outputStream = null;
        Map<String, Object> parametros = new HashMap<>();
        try {
            DocumentoAutorizado docAut = this.consultarDocumentoAutorizado(claveAcceso);
            docAut.cargarComprobante();
            List<DocumentoAutorizado> lf = new ArrayList<>();
            lf.add(docAut);
            JRDataSource dataSource = new JRBeanCollectionDataSource(lf);
            parametros.put("LOGO", logo);
            parametros.put("FECHA_AUTORIZADO", Utilidades.fechaFormatoDeFechaString(SisVar.tipo, docAut.getFechaAutorizacion()));
            byte[] bytes = JasperExportManager.exportReportToPdf(JasperFillManager.fillReport(reporte + (documento.equalsIgnoreCase("FAC") ? "factura" : documento.equalsIgnoreCase("RET") ? "retencion" : "factura") + ".jasper", parametros, dataSource));
            if (bytes == null) {
                return;
            }
            outputStream = new FileOutputStream(SisVar.rutaConsulta.replaceAll("empresa", docAut.getRucEmpresa()) + claveAcceso + ".pdf");
            outputStream.write(bytes, 0, bytes.length);
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void descargarDocumentoXml(TbAutorizados documento) {
        HttpServletResponse response;
        OutputStream outputStream = null;
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 2);
        try {
            DocumentoAutorizado docAut = Utilidades.getObjectByString(DocumentoAutorizado.class, documento.getTbDocumentoXML().getDocumentoXML());
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult xmlOutput = new StreamResult(new StringWriter());
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            Document document = documentBuilderFactory.newDocumentBuilder().newDocument();
            document.setXmlStandalone(true);
            Element element3 = document.createElement("autorizacion");
            document.appendChild(element3);
            Element element4 = document.createElement("estado");
            element4.appendChild(document.createTextNode(docAut.getEstado()));
            element3.appendChild(element4);
            Element element2 = document.createElement("numeroAutorizacion");
            element2.appendChild(document.createTextNode(docAut.getNumeroAutorizacion()));
            element3.appendChild(element2);
            Element element1 = document.createElement("fechaAutorizacion");
            element1.appendChild(document.createTextNode(docAut.getFechaAutorizacion()));
            element3.appendChild(element1);
            element1 = document.createElement("comprobante");
            CDATASection cDATASection = document.createCDATASection(docAut.getComprobante());
            element1.appendChild(cDATASection);
            element3.appendChild(element1);
            DOMSource dOMSource = new DOMSource(document);
            transformer.transform(dOMSource, xmlOutput);
            byte[] bytes = IOUtils.toByteArray(xmlOutput.getWriter().toString());
            if (bytes == null) {
                return;
            }
            response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.reset();
            response.setContentType("text/xml");
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=" + documento.getDocumento() + "_" + documento.getNumeroDocumento() + ".xml");
            response.setContentLength(bytes.length);
            outputStream = response.getOutputStream();
            outputStream.write(bytes, 0, bytes.length);
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            FacesContext.getCurrentInstance().responseComplete();
        }
    }

    @Override
    public void generarXmlBuilder(TbAutorizados documento) {
        try {
            DocumentoAutorizado docAut = Utilidades.getObjectByString(DocumentoAutorizado.class, documento.getTbDocumentoXML().getDocumentoXML());
            String rutaConsulta = SisVar.rutaConsulta.replaceAll("empresa", documento.getEmpresaId().getIdentificacion());
            File file = new File(rutaConsulta + documento.getClaveAcceso() + ".xml");

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            Document document = documentBuilderFactory.newDocumentBuilder().newDocument();
            Element element3 = document.createElement("autorizacion");
            document.appendChild(element3);
            Element element4 = document.createElement("estado");
            element4.appendChild(document.createTextNode(docAut.getEstado()));
            element3.appendChild(element4);
            Element element2 = document.createElement("numeroAutorizacion");
            element2.appendChild(document.createTextNode(docAut.getNumeroAutorizacion()));
            element3.appendChild(element2);
            Element element1 = document.createElement("fechaAutorizacion");
            element1.appendChild(document.createTextNode(docAut.getFechaAutorizacion()));
            element3.appendChild(element1);
            element1 = document.createElement("comprobante");
            CDATASection cDATASection = document.createCDATASection(docAut.getComprobante());
            element1.appendChild(cDATASection);
            element3.appendChild(element1);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", 2);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource dOMSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(file);
            transformer.transform(dOMSource, streamResult);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void enviarCorreo(String correo, String subject, String detalle, String ruta, String nombreArchivo) {
        String[] listCorreos;
        String cadenaCorreos = "";
        try {
            correo = correo.replaceAll(";", ",")
                    .replaceAll(" ", "");
            //correo = correo.trim();
            listCorreos = (SisVar.ambiente.equalsIgnoreCase("2") ? correo : "henrypilco333@gmail.com").split(",");
            //listCorreos = "henrypilco333@gmail.com".split(",");
            for (String c : listCorreos) {
                if (Utilidades.validarEmailConExpresion(c)) {
                    cadenaCorreos = cadenaCorreos + c + ",";
                }
            }
            if (cadenaCorreos.length() > 1) {
                cadenaCorreos = cadenaCorreos.substring(0, cadenaCorreos.length() - 1);
                Runnable emailThread;
                Thread thread;
                emailThread = new EmailRunnable(cadenaCorreos, subject, detalle, Propiedades.bcc, Propiedades.cc, ruta, nombreArchivo);
                thread = new Thread(emailThread);
                thread.start();
            }
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

//    @Asynchronous
    @Override
    public Future<Integer> envioCorreos(List<TbAutorizados> docAutorizados) {
        String[] listCorreos;
        String cadenaCorreos;
        try {
            Runnable emailThread;
            Thread thread;
            for (TbAutorizados d : docAutorizados) {
                if (d.getId() != null) {
                    //VALIDAR CORREO
                    cadenaCorreos = "";
                    listCorreos = (SisVar.ambiente.equalsIgnoreCase("2") ? d.getUsuarioId().getCorreo() : "henrypilco333@gmail.com").split(",");
                    for (String correo : listCorreos) {
                        if (Utilidades.validarEmailConExpresion(correo)) {
                            cadenaCorreos = cadenaCorreos + correo + ",";
                        }
                    }
                    if (cadenaCorreos.length() > 1) {
                        cadenaCorreos = cadenaCorreos.substring(0, cadenaCorreos.length() - 1);
                        emailThread = new EmailRunnable(cadenaCorreos, d.getDocumento() + "-" + d.getNumeroDocumento(), Propiedades.plantilla ? Propiedades.formatoEmail : "<h2>TEST</h2>", Propiedades.bcc, Propiedades.cc, SisVar.rutaConsulta.replaceAll("empresa", d.getEmpresaId().getIdentificacion()), d.getClaveAcceso());
                        thread = new Thread(emailThread);
                        thread.start();
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return new AsyncResult<>(1);
    }

    @Override
    public DocumentoAutorizado consultarDocumentoAutorizado(String ruta) {
        try {
            JAXBContext context = JAXBContext.newInstance(DocumentoAutorizado.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (DocumentoAutorizado) unmarshaller.unmarshal(new File(ruta));
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    public String consultarTextDocumentoAutorizado(DocumentoAutorizado docAutorizado) {
        try {
            StringWriter sw = new StringWriter();
            JAXBContext context = JAXBContext.newInstance(DocumentoAutorizado.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(docAutorizado, sw);
            return sw.toString();
        } catch (Exception e) {
            Logger.getLogger(FacturacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
}
