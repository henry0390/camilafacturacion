/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.local;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author Acer
 */
public interface EntityManagerLocal {
    
    public EntityManager getEm();

    public <T extends Object> T find(Class<T> entity, Object id);

    public <T extends Object> T findObject(Class<T> entity, Map<String, Object> parametros);

    public <T> List<T> findAllByParameter(Class entity, Map<String, Object> parametros, String order);

    public Object persist(Object object);

    public Object findObject(String tipoQuery, String query, Map<String, Object> parametros, Class modelo, Boolean requireTransaction);

    public List findAllSQL(String query, Map<String, Object> parametros, Class modelo);
}
