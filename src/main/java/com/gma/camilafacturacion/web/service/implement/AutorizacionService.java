/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.implement;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.entidades.facturacion.TbDocumentoXML;
import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.AutorizacionRunnable;
import com.gma.camilafacturacion.web.models.consulta.TbEmpresaUsuario;
import com.gma.camilafacturacion.web.models.datasource.CxctDocumento;
import com.gma.camilafacturacion.web.models.datasource.CxptComprobanteRetencion;
import com.gma.camilafacturacion.web.models.datasource.FactCabventa;
import com.gma.camilafacturacion.web.models.datasource.FacturaXml;
import com.gma.camilafacturacion.web.models.datasource.GuiaRemisionXml;
import com.gma.camilafacturacion.web.models.datasource.InvtCabplandes;
import com.gma.camilafacturacion.web.models.datasource.NotaCreditoXml;
import com.gma.camilafacturacion.web.models.datasource.RetencionXml;
import com.gma.camilafacturacion.web.models.logica.SriRespuestaConsulta;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
//import ec.sri.firma.principal.SriIntegracion;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.xml.sax.InputSource;

/**
 *
 * @author Acer
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class AutorizacionService implements AutorizacionLocal{
    @EJB
    private EntityManagerLocal manager;

    @Override
    public <T> List<T> registarUsuarios(int tipo, List documentos, TbEmpresa empresa) {
        List<T> list = new ArrayList<>();
        FacturaXml fxml;
        RetencionXml rxml;
        GuiaRemisionXml gxml;
        NotaCreditoXml nxml;
        try{
            JAXBContext context;
            Unmarshaller unmarshaller;
            switch(tipo){
                case 1:
                    context = JAXBContext.newInstance(FacturaXml.class);
                    unmarshaller = context.createUnmarshaller();
                    for (Object f : documentos) {                
                        fxml= (FacturaXml) unmarshaller.unmarshal(new InputSource(new StringReader(((FactCabventa)f).getXmlText())));
                        fxml.cargarDatosAdicionales();
                        fxml.setUsuario(this.registrarUsuarioCore(empresa, fxml.getInfoFactura().getIdentificacionComprador(), fxml.getInfoFactura().getRazonSocialComprador(), fxml.getCorreo()==null?"":fxml.getCorreo(), fxml.getInfoFactura().getIdentificacionComprador()));
                        list.add((T) fxml);
                    }
                    break;
                case 2:
                    context = JAXBContext.newInstance(RetencionXml.class);
                    unmarshaller = context.createUnmarshaller();
                    for (Object r : documentos) {                
                        rxml= (RetencionXml) unmarshaller.unmarshal(new InputSource(new StringReader(((CxptComprobanteRetencion)r).getXmlText())));   
                        rxml.cargarDatosAdicionales();
                        rxml.setUsuario(this.registrarUsuarioCore(empresa, rxml.getInfoCompRetencion().getIdentificacionSujetoRetenido(), rxml.getInfoCompRetencion().getRazonSocialSujetoRetenido(), rxml.getCorreo()==null?"":rxml.getCorreo(), rxml.getInfoCompRetencion().getIdentificacionSujetoRetenido()));
                        list.add((T) rxml);
                    }
                    break;
                case 3:
                    context = JAXBContext.newInstance(GuiaRemisionXml.class);
                    unmarshaller = context.createUnmarshaller();
                    for (Object g : documentos) {                
                        gxml= (GuiaRemisionXml) unmarshaller.unmarshal(new InputSource(new StringReader(((InvtCabplandes)g).getXmlText())));   
                        gxml.cargarDatosAdicionales();
                        list.add((T) gxml);
                    }
                    break;
                case 4:
                    context = JAXBContext.newInstance(NotaCreditoXml.class);
                    unmarshaller = context.createUnmarshaller();
                    for (Object n : documentos) {                
                        nxml= (NotaCreditoXml) unmarshaller.unmarshal(new InputSource(new StringReader(((CxctDocumento)n).getXmlText())));   
                        nxml.cargarDatosAdicionales();
                        nxml.setUsuario(this.registrarUsuarioCore(empresa, nxml.getInfoNotaCredito().getIdentificacionComprador(), nxml.getInfoNotaCredito().getRazonSocialComprador(), nxml.getCorreo()==null?"":nxml.getCorreo(), nxml.getInfoNotaCredito().getIdentificacionComprador()));
                        list.add((T) nxml);
                    }
                    break;
                case 5:
                    for (FacturaXml f : (List<FacturaXml>)documentos) {                
                        f.cargarDatosAdicionales();
                        f.setUsuario(this.registrarUsuarioCore(empresa, f.getInfoFactura().getIdentificacionComprador(), f.getInfoFactura().getRazonSocialComprador(), f.getCorreo()==null?"":f.getCorreo(), f.getInfoFactura().getIdentificacionComprador()));
                        list.add((T) f);
                    }
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }
    
    @Override
    public TbUsuario registrarUsuarioCore(TbEmpresa empresa, String identificacion, String razonSocial, String correo, String clave){
        Map<String, Object> parametros = new HashMap<>();
        Map<String, Object> parametros2 = new HashMap<>();
        TbUsuario u=null;
        try{
            parametros.put("identificacion", identificacion);
            u=this.manager.findObject(TbUsuario.class,parametros);
            if (u==null) {            
                u= new TbUsuario(null, identificacion, razonSocial, correo, clave, "A");
                u= (TbUsuario) this.manager.persist(u);
            }else{
                if(!u.getCorreo().equalsIgnoreCase(correo)){
                    u.setCorreo(correo);
                    u=(TbUsuario) this.manager.persist(u);
                }
            }
            parametros2.put("0",empresa.getId());
            parametros2.put("1", u.getId());
            TbEmpresaUsuario empresaUsuario= (TbEmpresaUsuario)this.manager.findObject("SQL", "SELECT eu.empresaid, eu.usuarioid "
                    + "FROM autorizacion.tbempresausuario eu WHERE eu.empresaid=? AND eu.usuarioid=?", 
                    parametros2, TbEmpresaUsuario.class, Boolean.FALSE);
            if(empresaUsuario==null){
                this.manager.findObject("SQL", "INSERT INTO autorizacion.tbempresausuario VALUES (?,?) RETURNING usuarioid", parametros2, null, Boolean.TRUE);
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return u;
    }
    
    @Override
    public void consultar(List<FactCabventa> facturas, String ambiente, String tipo, String rutaConsulta) {
        try{
            String estado;
            System.out.println(ambiente+"-"+tipo+"-"+rutaConsulta);
            //SriIntegracion s = new SriIntegracion();
            JAXBContext context = JAXBContext.newInstance(FacturaXml.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            FacturaXml facturaxml;
            for (FactCabventa factura : facturas) {
                facturaxml= (FacturaXml) unmarshaller.unmarshal(new InputSource(new StringReader(factura.getXmlText())));
                //System.out.println("\nAutorizacionService consultar CA:"+facturaxml.getInfoTributaria().getClaveAcceso());
              //  s.setConsultaclaveAcceso(facturaxml.getInfoTributaria().getClaveAcceso());
              //  s.setRutaManualArchivoRespuesta(rutaConsulta+facturaxml.getInfoTributaria().getRuc()+"_"+facturaxml.getInfoTributaria().getClaveAcceso()+".xml");
                /**
                 * En el metodo de Consulta de la Libreria SRI
                 * Si en la varible de Estado el valor es AUTORIZADO o NO AUTORIZADO (FUE ENVIADA LA CLAVE ANTERIORMENTE // EXISTE EN EL SRI)
                 * Si es null o vacia (LA CLAVE NO SE HA ENVIADO // NO EXISTE EN EL SRI)
                 */
                switch (ambiente){
                    case "1"://DESARROLLO
                        switch (tipo){
                            case "1"://ONLINE
                                //s.validaComprobante();
                                break;
                            case "2"://OFFLINE
                                //s.validaComprobanteOffline();
                                break;
                            default:
                                break;
                        }
                        break;                    
                    case "2"://PRODUCCION
                        switch (tipo){
                            case "1"://ONLINE
                                //s.validaComprobanteProduccion();
                                break;
                            case "2"://OFFLINE
                                //s.validaComprobanteOfflineProd();
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                //estado=s.getConsultaEstado();
                /*System.out.println("CLAVE DE ACCESO"+facturaxml.getInfoTributaria().getClaveAcceso()+
                        "\nESTADO:"+((estado==null || estado.trim().length()==0)?"NO ENVIADA-NO EXISTE EN SRI-NO TRAE DATOS EL METODO":
                         (estado+"FEC:"+s.getConsultaFechaAutorizacion()+"\tNUM:"+s.getConsultaNumeroAutorizacion())+(
                          estado.equalsIgnoreCase("AUTORIZADO")?"":("\n"+s.getConsultaMensajeAutorizacion()+"-"+s.getConsultaMensajeAutorizacionAdic())))
                );*/
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override    
    public SriRespuestaConsulta consultarByClaveAcceso(String documento,String claveAcceso, String ambiente, String tipo, String rutaConsulta) {
        SriRespuestaConsulta sric= null;
        try{
            /*SriIntegracion s = new SriIntegracion();
            this.inicializarLibreria(s);
            s.setConsultaclaveAcceso(claveAcceso);
            System.out.println(rutaConsulta+"C A: "+claveAcceso);
            s.setRutaManualArchivoRespuesta(rutaConsulta+claveAcceso+".xml");
            switch (ambiente){
                case "1"://DESARROLLO
                    switch (tipo){
                        case "1"://ONLINE
                            s.validaComprobante();
                            break;
                        case "2"://OFFLINE
                            s.validaComprobanteOffline();
                            break;
                        default:
                            break;
                    }
                    break;                    
                case "2"://PRODUCCION
                    switch (tipo){
                        case "1"://ONLINE
                            s.validaComprobanteProduccion();
                            break;
                        case "2"://OFFLINE
                            s.validaComprobanteOfflineProd();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            //CARGAR DOCUMENTO
            System.out.println("-"+s.getConsultaEstado()+" "+s.getConsultaNumeroAutorizacion()+" "+s.getConsultaFechaAutorizacion()+" *"+s.getConsultaMensajeAutorizacion());
            if(s!=null && s.getConsultaEstado().equalsIgnoreCase("AUTORIZADO"))
                sric = new SriRespuestaConsulta(s.getConsultaEstado(), s.getConsultaNumeroAutorizacion(), s.getConsultaFechaAutorizacion(), s.getConsultaMensajeAutorizacion(), s.getConsultaMensajeAutorizacionAdic(), claveAcceso, documento, null, null);            
            else
                sric = new SriRespuestaConsulta();*/
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return sric;
    }
    
    /**
     * Listado de Objetos con atributo String en formato xml del cual se crea el archivo para proceder a firmarlo
     * @param <T>
     * @param tipo
     * @param clase
     * @param documentos
     * @param rutaGenerado
     * @param rutaFirma
     * @param empresa
     * @return  
     */
    @Override
    public <T> List<T> firmar(TbEmpresa empresa, int tipo,Class<T> clase, List documentos,String rutaGenerado, String rutaFirma) {        
        List<T> list = new ArrayList<>();
        /*try{
            SriIntegracion s = new SriIntegracion();
            JAXBContext context;
            Marshaller marshaller;
            //INICIALIZACION
            this.inicializarLibreria(s);
            
            context = JAXBContext.newInstance(clase);
            marshaller=context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            
            s.setRutaCertificadoP12(empresa.getRutaCertificado());
            s.setContraseniaCertificado(empresa.getClave());
            switch(tipo){
                case 1:
                    for (FacturaXml factura : (List<FacturaXml>)documentos) {
                        //OBJETO A FILE XML
                        marshaller.marshal(factura, new File(rutaGenerado+factura.getInfoTributaria().getClaveAcceso()+".xml"));
                        //XML A XML FIRMADO
                        s.setArchivoFirmar(rutaGenerado+factura.getInfoTributaria().getClaveAcceso()+".xml");
                        s.setRutaSalida(rutaFirma);                        
                        String r= s.firmarArchivo();
                        if(r!=null)
                            System.out.println("RESP. ERROR FIRMA: "+r);
                        else
                            list.add((T) factura);
                    }
                    break;
                case 2:
                    for (RetencionXml retencion : (List<RetencionXml>)documentos) {
                        //OBJETO A FILE XML
                        marshaller.marshal(retencion, new File(rutaGenerado+retencion.getInfoTributaria().getClaveAcceso()+".xml"));
                        //XML A XML FIRMADO
                        s.setArchivoFirmar(rutaGenerado+retencion.getInfoTributaria().getClaveAcceso()+".xml");
                        s.setRutaSalida(rutaFirma);
                        String r= s.firmarArchivo();
                        if(r!=null)
                            System.out.println("RESP. ERROR FIRMA: "+r);
                        else
                            list.add((T) retencion);
                    }
                    break;
                case 3:
                    for (GuiaRemisionXml guia : (List<GuiaRemisionXml>)documentos) {
                        marshaller.marshal(guia, new File(rutaGenerado+guia.getInfoTributaria().getClaveAcceso()+".xml"));                        
                        s.setArchivoFirmar(rutaGenerado+guia.getInfoTributaria().getClaveAcceso()+".xml");
                        s.setRutaSalida(rutaFirma);
                        String r= s.firmarArchivo();
                        if(r!=null)
                            System.out.println("RESP. ERROR FIRMA: "+r);
                        else
                            list.add((T) guia);
                    }
                    break;
                case 4:                    
                    for (NotaCreditoXml nota : (List<NotaCreditoXml>)documentos) {
                        marshaller.marshal(nota, new File(rutaGenerado+nota.getInfoTributaria().getClaveAcceso()+".xml"));                        
                        s.setArchivoFirmar(rutaGenerado+nota.getInfoTributaria().getClaveAcceso()+".xml");
                        s.setRutaSalida(rutaFirma);
                        String r= s.firmarArchivo();
                        if(r!=null)
                            System.out.println("RESP. ERROR FIRMA: "+r);
                        else
                            list.add((T) nota);
                    }
                    break;
            }
        } catch (JAXBException e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }*/
        return list;
    }

    @Override
    public <T> List<T> autorizar(TbEmpresa empresa, int tipoProceso, List<FacturaXml> facturas, List<RetencionXml> retenciones, String ambiente, String tipo, String rutaFirmado, FacturacionLocal facturacionService, AutorizacionLocal autorizacionMainService) {
        List<T> list = null;
        try{
            String archivo;
            Runnable autorizacionThread;
            Thread thread;
            List<String> clavesAcceso= new ArrayList<>();
            List<TbAutorizados> listAutorizados;
            int vueltas=0;
            switch(tipoProceso){
                case 1:
                    facturas.forEach((factura) -> { clavesAcceso.add(factura.getInfoTributaria().getClaveAcceso()); });
                    listAutorizados = this.consultarDocumentoAutorizado(clavesAcceso);
                    if(listAutorizados!=null && !listAutorizados.isEmpty())
                        for (TbAutorizados autorizado : listAutorizados) {
                            for (FacturaXml factura : facturas) {
                                if(factura.getInfoTributaria().getClaveAcceso().equalsIgnoreCase(autorizado.getClaveAcceso())){
                                    facturas.remove(factura);
                                    break;
                                }
                            }
                        }    
                    for (FacturaXml factura : facturas) {
                        archivo=rutaFirmado+"/"+factura.getInfoTributaria().getClaveAcceso()+".xml";
                        autorizacionThread = new AutorizacionRunnable(ambiente, tipo, archivo, factura.getInfoTributaria().getClaveAcceso(), facturacionService, autorizacionMainService, empresa, factura.getUsuario());
                        thread = new Thread(autorizacionThread);
                        thread.start();
                    }
                    Thread.sleep((facturas.size()<5?5000:25000)*(1+facturas.size()/50));              
                    break;
                case 2:
                    retenciones.forEach((retencion) -> { clavesAcceso.add(retencion.getInfoTributaria().getClaveAcceso()); });
                    listAutorizados = this.consultarDocumentoAutorizado(clavesAcceso);
                    if(listAutorizados!=null && !listAutorizados.isEmpty())
                        for (TbAutorizados autorizado : listAutorizados) {
                            for (RetencionXml retencion : retenciones) {
                                if(retencion.getInfoTributaria().getClaveAcceso().equalsIgnoreCase(autorizado.getClaveAcceso())){
                                    retenciones.remove(retencion);
                                    break;
                                }
                            }
                        }
                    for (RetencionXml retencion : retenciones) {
                        archivo=rutaFirmado+"/"+retencion.getInfoTributaria().getClaveAcceso()+".xml";
                        autorizacionThread = new AutorizacionRunnable(ambiente, tipo, archivo, retencion.getInfoTributaria().getClaveAcceso(), facturacionService, autorizacionMainService, empresa, retencion.getUsuario());
                        thread = new Thread(autorizacionThread);
                        thread.start();
                    }
                    Thread.sleep(25000*(1+retenciones.size()/50));
                    break;
            }
            do{
                vueltas++;
                listAutorizados = this.consultarDocumentoAutorizado(clavesAcceso);
                if(listAutorizados.size()>=clavesAcceso.size())
                    break;                        
                if(vueltas==2){
                    for (TbAutorizados a : listAutorizados)
                        clavesAcceso.remove(a.getClaveAcceso());
                    for (int i = 0; i < clavesAcceso.size(); i++)
                        listAutorizados.add(new TbAutorizados(null,clavesAcceso.get(i), null, tipoProceso==1?"FAC":tipoProceso==2?"RET":"DOC", null, null, Integer.parseInt(ambiente), null, null));                            
                }else{
                    Thread.sleep(3000);
                }
            }while(vueltas<2 && listAutorizados.size()<clavesAcceso.size());
            list = (List<T>) listAutorizados;
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }
    
    @Override
    public void grabarAutorizado(TbEmpresa empresa, TbUsuario usuario, SriRespuestaConsulta sric, String xml){
        try{
            System.out.println(" ########### GRABA SRI AUTORIZACION ############ ");
            if(sric!=null && sric.getEstado().equalsIgnoreCase("AUTORIZADO")){
                TbAutorizados docAut = new TbAutorizados(null, sric.getClaveAcceso(), sric.getNumeroAutorizacion(), sric.getDocumento(), new Date(), sric.getFechaAutorizacion(), Integer.valueOf(SisVar.ambiente), sric.getNumeroDocumento(), sric.getTotal());
                docAut.setEmpresaId(empresa);
                docAut.setUsuarioId(usuario);                
                docAut=(TbAutorizados) this.manager.persist(docAut);
                TbDocumentoXML docXml = new TbDocumentoXML(docAut.getId(), xml);
                this.manager.persist(docXml);
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    public void grabarXmlAutorizado(TbAutorizados docAut, String xml){
        try{
            System.out.println(" ########### GRABA SRI AUTORIZACION XML ############ ");
            if(xml!=null){
                TbDocumentoXML docXml = new TbDocumentoXML(docAut.getId(), xml);
                this.manager.persist(docXml);
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /*public void inicializarLibreria(SriIntegracion s){
        try{
            s.setArchivoFirmar(SisVar.rutaInicializacion+SisVar.archivoInicializacion+".xml");
            s.setRutaSalida(SisVar.rutaInicializacion+"firma");
            s.setRutaCertificadoP12(SisVar.rutaCertificadoInicializacion);
            s.setContraseniaCertificado(SisVar.passwordCertificadoInicializacion);
            s.firmarArchivo();
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
    }*/
    
    @Override
    public List<TbAutorizados> consultarDocumentoAutorizado(List<String> clavesAcceso){
        List<TbAutorizados> autorizados= null;
        Map<String, Object> p = new HashMap<>();
        try{
            p.put("claveAcceso", clavesAcceso);
            autorizados = this.manager.findAllByParameter(TbAutorizados.class, p, null);
        } catch (Exception e) {
            Logger.getLogger(AutorizacionService.class.getName()).log(Level.SEVERE, null, e);
        }
        return autorizados;
    }
}
