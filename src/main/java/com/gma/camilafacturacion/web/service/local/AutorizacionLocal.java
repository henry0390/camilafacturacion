/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.local;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.web.models.datasource.FactCabventa;
import com.gma.camilafacturacion.web.models.datasource.FacturaXml;
import com.gma.camilafacturacion.web.models.datasource.RetencionXml;
import com.gma.camilafacturacion.web.models.logica.SriRespuestaConsulta;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Acer
 */
@Local
public interface AutorizacionLocal {
    public <T> List<T> registarUsuarios(int tipo, List documentos, TbEmpresa empresa);
    public TbUsuario registrarUsuarioCore(TbEmpresa empresa, String identificacion, String razonSocial, String correo, String clave);
    public void consultar(List<FactCabventa> facturas, String ambiente, String tipo, String rutaConsulta);
    public SriRespuestaConsulta consultarByClaveAcceso(String documento, String claveAcceso, String ambiente, String tipo, String rutaConsulta);
    public <T> List<T> firmar(TbEmpresa empresa, int tipo,Class<T> clase, List documentos, String rutaGenerado, String rutaFirma);
    public <T> List<T> autorizar(TbEmpresa empresa, int tipoProceso, List<FacturaXml> facturas, List<RetencionXml> retenciones, String ambiente, String tipo, String rutaFirmado, FacturacionLocal facturacionService, AutorizacionLocal autorizacionMainService);
    public void grabarAutorizado(TbEmpresa empresa, TbUsuario usuario, SriRespuestaConsulta sric, String xml);
    public void grabarXmlAutorizado(TbAutorizados docAut, String xml);
    public List<TbAutorizados> consultarDocumentoAutorizado(List<String> clavesAcceso);    
}
