/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.service.implement;

import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.web.models.datasource.CxctDocumento;
import com.gma.camilafacturacion.web.models.datasource.CxptComprobanteRetencion;
import com.gma.camilafacturacion.web.models.datasource.FactCabventa;
import com.gma.camilafacturacion.web.models.datasource.InvtCabplandes;
import com.gma.camilafacturacion.web.service.local.AutorizacionDataSourceLocal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

/**
 *
 * @author Acer
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class AutorizacionDataSourceService implements AutorizacionDataSourceLocal {

    protected BasicDataSource init(int tipo) {
        BasicDataSource bds = new BasicDataSource();
        switch (tipo) {
            case 1:
                bds.setUrl(SisVar.urlFrom);
                bds.setDriverClassName(SisVar.driverClassFrom);
                bds.setUsername(SisVar.usernameFrom);
                bds.setPassword(SisVar.passwordFrom);
                bds.setMaxActive(300);
                bds.setMinIdle(5);
                bds.setDefaultAutoCommit(false);
                break;
            case 2:
                bds.setUrl(SisVar.urlTo);
                bds.setDriverClassName(SisVar.driverClassTo);
                if(SisVar.integratedSecurityTo.equalsIgnoreCase("true")){
                    bds.setUrl(SisVar.urlTo+"integratedSecurity=true");
                }else{
                    bds.setUsername(SisVar.usernameTo);
                    bds.setPassword(SisVar.passwordTo);
                }                
                bds.setMaxActive(300);
                bds.setMinIdle(5);
                bds.setDefaultAutoCommit(false);
                break;
            default:
                break;
        }
        return bds;
    }

    @Override
    public void cargarDatos(Date desde, Date hasta) {
        try {
            System.out.println("EJB cargarDatos "+new SimpleDateFormat("dd/MM/yyyy").format(desde)+" - "+new SimpleDateFormat("dd/MM/yyyy").format(hasta));            
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("EXEC SP_SEGP_MIGRACION_SISTEMA_CENTRAL ?,? ");
                ps.setString(1, new SimpleDateFormat("yyyyMMdd").format(desde));
                ps.setString(2, new SimpleDateFormat("yyyyMMdd").format(hasta));
                ps.execute();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public List<FactCabventa> getFacturas(Date desde, Date hasta) {
        List<FactCabventa> facturas = new ArrayList<>();
        try {
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                //AND A.FACCVT_fechaautoriza IS NULL
                PreparedStatement ps = cx.prepareStatement("SELECT A.*, B.CXCCLI_razonsocia FROM FACT_CABVENTA A (NOLOCK), CXCM_CLIENTE B (NOLOCK) WHERE A.CXCCLI_codigo=B.CXCCLI_codigo AND A.FACCVT_fechaautoriza IS NULL AND GENTDO_codigo='FAC' "+((desde==null&&hasta==null)?"":" AND A.FACCVT_fechaemisi BETWEEN ? AND ? ") +" ORDER BY FACCVT_fechaemisi DESC, FACCVT_numsecuenc DESC ");
                if(desde!=null && hasta!=null){
                    ps.setString(1, new SimpleDateFormat("yyyyMMdd").format(desde));
                    ps.setString(2, new SimpleDateFormat("yyyyMMdd").format(hasta));
                }
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    facturas.add(new FactCabventa(rs.getInt("FACCVT_numsecuenc"), rs.getString("GENCIA_codigo"), rs.getString("GENOFI_codigo"), rs.getString("GENMOD_codigo"),
                            rs.getString("GENTDO_codigo"), rs.getString("FACCVT_numdocumen"), rs.getInt("FACCVT_numrelplan"), rs.getString("GENTDO_docrelacio"), rs.getString("CXCRUT_rutarela"),
                            rs.getInt("FACCVT_numdocrela"), rs.getInt("FACCVT_secdocurel"), rs.getDate("FACCVT_fechaemisi"), rs.getDate("FACCVT_fechadespa"), rs.getString("FACVDR_codigo"),
                            rs.getString("CXCCLI_codigo"), rs.getString("CXCFPG_codigo"), rs.getString("CXCTPG_codigo"), rs.getString("FACCVT_codrutavta"), rs.getString("FACCVT_rutdistcli"),
                            rs.getString("FACCVT_descrilarg"), rs.getString("FACCVT_motvdevolu"), rs.getBigDecimal("FACCVT_subtotamov"), rs.getBigDecimal("FACCVT_desctomov"), rs.getBigDecimal("FACCVT_totimpumov"),
                            rs.getBigDecimal("FACCVT_netomov"), rs.getBigDecimal("FACCVT_transpmov"),
                            rs.getBigDecimal("FACCVT_recargomov"), rs.getString("FACCVT_estado"), rs.getString("FACCVT_usuaingreg"), rs.getString("FACCVT_usuamodreg"), rs.getDate("FACCVT_fechingreg"),
                            rs.getDate("FACCVT_fechmodreg"), rs.getString("FACDVT_listaprecio"), rs.getInt("FACCVT_idsecuencia"), rs.getString("FACCVT_autorizacion"), rs.getDate("FACCVT_fechaautoriza"),
                            rs.getString("FACCVT_claveacceso"), rs.getString("FACGUI_autorizacion"), rs.getDate("FACGUI_fechaautoriza"), rs.getString("FACGUI_claveacceso"), rs.getString("CXCCLI_razonsocia")));
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return facturas;
    }

    @Override
    public List<FactCabventa> getFacturas(String ids) {
        List<FactCabventa> facturas = new ArrayList<>();
        try {
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("SELECT A.*, B.CXCCLI_razonsocia FROM FACT_CABVENTA A (NOLOCK), CXCM_CLIENTE B (NOLOCK) WHERE A.CXCCLI_codigo=B.CXCCLI_codigo AND A.FACCVT_numsecuenc IN ("+ids+") AND GENTDO_codigo='FAC' ORDER BY FACCVT_fechaemisi DESC ");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    facturas.add(new FactCabventa(rs.getInt("FACCVT_numsecuenc"), rs.getString("GENCIA_codigo"), rs.getString("GENOFI_codigo"), rs.getString("GENMOD_codigo"),
                            rs.getString("GENTDO_codigo"), rs.getString("FACCVT_numdocumen"), rs.getInt("FACCVT_numrelplan"), rs.getString("GENTDO_docrelacio"), rs.getString("CXCRUT_rutarela"),
                            rs.getInt("FACCVT_numdocrela"), rs.getInt("FACCVT_secdocurel"), rs.getDate("FACCVT_fechaemisi"), rs.getDate("FACCVT_fechadespa"), rs.getString("FACVDR_codigo"),
                            rs.getString("CXCCLI_codigo"), rs.getString("CXCFPG_codigo"), rs.getString("CXCTPG_codigo"), rs.getString("FACCVT_codrutavta"), rs.getString("FACCVT_rutdistcli"),
                            rs.getString("FACCVT_descrilarg"), rs.getString("FACCVT_motvdevolu"), rs.getBigDecimal("FACCVT_subtotamov"), rs.getBigDecimal("FACCVT_desctomov"), rs.getBigDecimal("FACCVT_totimpumov"),
                            rs.getBigDecimal("FACCVT_netomov"), rs.getBigDecimal("FACCVT_transpmov"),
                            rs.getBigDecimal("FACCVT_recargomov"), rs.getString("FACCVT_estado"), rs.getString("FACCVT_usuaingreg"), rs.getString("FACCVT_usuamodreg"), rs.getDate("FACCVT_fechingreg"),
                            rs.getDate("FACCVT_fechmodreg"), rs.getString("FACDVT_listaprecio"), rs.getInt("FACCVT_idsecuencia"), rs.getString("FACCVT_autorizacion"), rs.getDate("FACCVT_fechaautoriza"),
                            rs.getString("FACCVT_claveacceso"), rs.getString("FACGUI_autorizacion"), rs.getDate("FACGUI_fechaautoriza"), rs.getString("FACGUI_claveacceso"), rs.getString("CXCCLI_razonsocia")));
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return facturas;
    }

    @Override
    public List<CxptComprobanteRetencion> getRetenciones(Date desde, Date hasta) {
        List<CxptComprobanteRetencion> retenciones = new ArrayList<>();
        try {
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                /*
                "SELECT  A.GENCIA_codigo,CXPDOC_codigo,A.CXPPRO_fechadocum,  CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9) AS secuencia," +
                            "A.CXPPRO_codigo, B.CXPPRO_razonsocia , SUM(CXPRDO_valor) AS valor " +
                            "FROM	CXPT_COMPROBANTERETENCION A, CXPM_PROVEEDOR B " +
                            "WHERE	CXPRDO_fechaautoriza IS NULL AND A.CXPPRO_codigo = B.CXPPRO_codigo " +((desde==null&&hasta==null)?"":"AND A.CXPPRO_fechadocum BETWEEN ? AND ? ")+
                            "GROUP BY A.GENCIA_codigo,CXPDOC_codigo,A.CXPPRO_fechadocum,  CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9), A.CXPPRO_codigo, B.CXPPRO_razonsocia " +
                            "ORDER BY  A.CXPPRO_fechadocum DESC , CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9)"
                */
                PreparedStatement ps = cx.prepareStatement("SELECT  A.GENCIA_codigo,A.GENOFI_codigo, CXPDOC_codigo,  " +
                                        "CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9) AS secuencia," +
                                        "SUM(CXPRDO_valor) AS valor " +
                                        "FROM	CXPT_COMPROBANTERETENCION A " +
                                        "WHERE	CXPRDO_fechaautoriza IS NULL " +
                                        "GROUP BY A.GENCIA_codigo,A.GENOFI_codigo,CXPDOC_codigo," +
                                        " CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9) " +
                                        "ORDER BY  A.GENCIA_codigo,A.GENOFI_codigo,CXPRDO_codestablecim + CXPRDO_codptoemision + RIGHT('000000000' + CONVERT(VARCHAR(9), CXPRDO_numretencion), 9)");
                /*if(desde!=null && hasta!=null){
                    ps.setString(1, new SimpleDateFormat("yyyyMMdd").format(desde));
                    ps.setString(2, new SimpleDateFormat("yyyyMMdd").format(hasta));
                }*/
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    retenciones.add(new CxptComprobanteRetencion(rs.getString("GENCIA_codigo"),rs.getString("GENOFI_codigo"),null, rs.getString("CXPDOC_codigo"), rs.getString("secuencia"), rs.getBigDecimal("valor"),
                            null, null));
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return retenciones;
    }
    
    @Override
    public List<InvtCabplandes> getGuiasRemision(Date desde, Date hasta) {
        List<InvtCabplandes> guiasRemision = new ArrayList<>();
        try {
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("SELECT * FROM INVT_CABPLANDES WHERE INVCPF_fechaautoriza IS NULL " + ((desde==null&&hasta==null)?"":"AND INVCPF_fecha BETWEEN ? AND ? ")+
                                                            "ORDER BY INVCPF_fecha DESC");
                if(desde!=null && hasta!=null){
                    ps.setString(1, new SimpleDateFormat("yyyyMMdd").format(desde));
                    ps.setString(2, new SimpleDateFormat("yyyyMMdd").format(hasta));
                }
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    guiasRemision.add(new InvtCabplandes(rs.getString("GENCIA_codigo"),rs.getString("GENOFI_codigo"),rs.getInt("INVCPF_numguia"),rs.getDate("INVCPF_fecha"),rs.getString("INVCAM_codigo"),
                    rs.getString("CXCRUT_codrutadis"),rs.getString("INVCPF_motivo"),rs.getBigDecimal("INVCPF_totalpeso"),rs.getBigDecimal("INVCPF_totalvolum"),rs.getInt("INVCPF_totaldocum"),rs.getString("INVCPF_claveacceso"),
                    rs.getDate("INVCPF_fechaautoriza"),rs.getString("INVCPF_autorizacion")));
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return guiasRemision;
    }
    
    @Override
    public List<CxctDocumento> getNotasCredito(Date desde, Date hasta) {
        List<CxctDocumento> notasCredito = new ArrayList<>();
        try {
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("SELECT *  FROM CXCT_DOCUMENTO " +
                                        "WHERE GENTDO_codigo='NCR'  AND CXCDEU_fechaautoriza IS NULL " + ((desde==null&&hasta==null)?"":"AND CXCDEU_fechaemisi BETWEEN ? AND ? ")+
                                        "ORDER BY CXCDEU_fechaemisi DESC");
                if(desde!=null && hasta!=null){
                    ps.setString(1, new SimpleDateFormat("yyyyMMdd").format(desde));
                    ps.setString(2, new SimpleDateFormat("yyyyMMdd").format(hasta));
                }
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    notasCredito.add(new CxctDocumento(rs.getString("GENCIA_codigo"),rs.getBigDecimal("CXCDEU_numesecuen"),rs.getString("GENOFI_codigo"),
                            rs.getString("CXCCLI_codigo"),rs.getDate("CXCDEU_fechaemisi"),rs.getString("CXCDEU_numdocumen"),
                            rs.getBigDecimal("CXCDEU_imptomov"),rs.getString("CXCDEU_concepto")));
                    
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return notasCredito;
    }
    
    @Override
    public List<FactCabventa> consultarXmlFacturas(List<FactCabventa> facturas){
        List<FactCabventa> lf=new ArrayList<>();
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("DECLARE @DATOS_XML VARCHAR(MAX) EXEC SP_FACC_CONSFACTURAXML ?,?,?,?,?, @DATOS_XML OUT SELECT @DATOS_XML AS DATOS_XML");
                for (FactCabventa f : facturas) {
                    ps.setString(1, f.getGENCIA_codigo());
                    ps.setString(2, f.getGENOFI_codigo());
                    ps.setInt(3, f.getFACCVT_numsecuenc());
                    ps.setString(4, SisVar.ambiente);
                    ps.setString(5, "N");
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        f.setXmlText(rs.getString("DATOS_XML"));
                    }
                    lf.add(f);
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (SQLException e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lf;
    }    
    
    @Override
    public List<CxptComprobanteRetencion> consultarXmlRetenciones(List<CxptComprobanteRetencion> retenciones){
        List<CxptComprobanteRetencion> lr= new ArrayList<>();
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("DECLARE @DATOS_XML VARCHAR(MAX) "
                        + "EXEC SP_FACC_CONSRETENCIONESXML ?,?,?,?,?, @DATOS_XML OUT SELECT @DATOS_XML AS DATOS_XML");
                for (CxptComprobanteRetencion r : retenciones) {
                    ps.setString(1, r.getGENCIA_codigo());
                    ps.setString(2, r.getGENOFI_codigo());
                    ps.setString(3, r.getCXPDOC_codigo());
                    ps.setString(4, SisVar.ambiente);
                    ps.setString(5, "N");
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        r.setXmlText(rs.getString("DATOS_XML"));
                    }
                    lr.add(r);
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lr;
    }
    
    @Override
    public List<InvtCabplandes> consultarXmlGuiasRemision(List<InvtCabplandes> guiasRemision){
        List<InvtCabplandes> lgr= new ArrayList<>();
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("DECLARE @DATOS_XML VARCHAR(MAX) "
                        + "EXEC SP_FACC_CONSGUIAREMISIONXML ?,?,?,?,?, @DATOS_XML OUT SELECT @DATOS_XML AS DATOS_XML");
                for (InvtCabplandes gr : guiasRemision) {
                    ps.setString(1, gr.getGENCIA_codigo());
                    ps.setString(2, gr.getGENOFI_codigo());
                    ps.setInt(3, gr.getINVCPF_numguia());
                    ps.setString(4, SisVar.ambiente);
                    ps.setString(5, "N");
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        gr.setXmlText(rs.getString("DATOS_XML"));
                    }
                    if(gr.getXmlText()!=null && gr.getXmlText().trim().length()>0)
                        lgr.add(gr);
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lgr;
    }
    
    @Override
    public List<CxctDocumento> consultarXmlNotasCredito(List<CxctDocumento> notasCredito){
        List<CxctDocumento> lnc= new ArrayList<>();
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("DECLARE @DATOS_XML VARCHAR(MAX) "
                        + "EXEC SP_FACC_CONSNOTACREDITOXML @GENCIA_codigo=?,@GENOFI_codigo=?,@FACCVT_numsecuenc=?,@STR_AMBIENTE=?,@GENERA_XML=?, @DATOS_XML OUT SELECT @DATOS_XML AS DATOS_XML");
                for (CxctDocumento nc : notasCredito) {
                    ps.setString(1, nc.getGENCIA_codigo());
                    ps.setString(2, nc.getGENOFI_codigo());
                    ps.setInt(3, nc.getCXCDEU_numesecuen().intValue());
                    ps.setString(4, SisVar.ambiente);
                    ps.setString(5, "N");
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        nc.setXmlText(rs.getString("DATOS_XML"));
                    }
                    lnc.add(nc);
                }
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lnc;
    }

    @Override
    public void actualizarFactura(int secFactura, String autorizacion, Date fechaAutorizacion) {
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("UPDATE FACT_CABVENTA SET FACCVT_autorizacion=? , FACCVT_fechaautoriza=? WHERE FACCVT_numsecuenc=?");
                ps.setString(1, autorizacion);
                //ps.setDate(2, new java.sql.Date(fechaAutorizacion.getTime()));
                ps.setTimestamp(2, new Timestamp(fechaAutorizacion.getTime()));
                ps.setInt(3, secFactura);
                ps.executeUpdate();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
            System.out.println("actualizarFactura "+secFactura+new Date());
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void actualizarRetencion(String secDocumento, String autorizacion, Date fechaAutorizacion) {
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("UPDATE CXPT_COMPROBANTERETENCION SET CXPRDO_autorizacion=? , CXPRDO_fechaautoriza=? WHERE CXPDOC_codigo=?");
                ps.setString(1, autorizacion);
                ps.setTimestamp(2, new Timestamp(fechaAutorizacion.getTime()));
                ps.setString(3, secDocumento);
                ps.executeUpdate();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    public void actualizarGuiaRemision(String secDocumento, String autorizacion, Date fechaAutorizacion) {
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("UPDATE CXPT_COMPROBANTERETENCION SET CXPRDO_autorizacion=? , CXPRDO_fechaautoriza=? WHERE CXPDOC_codigo=?");
                ps.setString(1, autorizacion);
                ps.setTimestamp(2, new Timestamp(fechaAutorizacion.getTime()));
                ps.setString(3, secDocumento);
                ps.executeUpdate();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    public void actualizarNotaCredito(String secDocumento, String autorizacion, Date fechaAutorizacion) {
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("UPDATE CXPT_COMPROBANTERETENCION SET CXPRDO_autorizacion=? , CXPRDO_fechaautoriza=? WHERE CXPDOC_codigo=?");
                ps.setString(1, autorizacion);
                ps.setTimestamp(2, new Timestamp(fechaAutorizacion.getTime()));
                ps.setString(3, secDocumento);
                ps.executeUpdate();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @Override
    public void actualizarFactura(String claveAcceso, String autorizacion, Date fechaAutorizacion) {
        try{
            DataSource dataSource = this.init(2);
            Connection cx = dataSource.getConnection();
            if (cx != null) {
                PreparedStatement ps = cx.prepareStatement("UPDATE FACT_CABVENTA SET FACCVT_autorizacion=? , FACCVT_fechaautoriza=? WHERE FACCVT_claveacceso=?");
                ps.setString(1, autorizacion);
                //ps.setDate(2, new java.sql.Date(fechaAutorizacion.getTime()));
                ps.setTimestamp(2, new Timestamp(fechaAutorizacion.getTime()));
                ps.setString(3, claveAcceso);
                ps.executeUpdate();
                ps.getConnection().commit();
                ps.close();
                cx.close();
            }
            System.out.println("actualizarFactura "+claveAcceso+" "+new Date());
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDataSourceService.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
