package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.SisVar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoTributaria",
    "infoNotaCredito",
    "detalles",
    "infoAdicional"
})
@XmlRootElement(name = "notaCredito")
public class NotaCreditoXml {
	@XmlElement(required = true)
    protected NotaCreditoXml.InfoTributaria infoTributaria;
    @XmlElement(required = true)
    protected NotaCreditoXml.InfoNotaCredito infoNotaCredito;
    @XmlElement(required = true)
    protected NotaCreditoXml.Detalles detalles;
    @XmlElement(required = true)
    protected NotaCreditoXml.InfoAdicional infoAdicional;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlTransient
    protected List<DatoAdicional> listDatos;
    @XmlTransient
    protected String correo;
    @XmlTransient
    protected String secuencia;
    @XmlTransient
    protected String numdoc;
    @XmlTransient
    private TbUsuario usuario;
    
    public void cargarDatosAdicionales(){
        this.listDatos = new ArrayList<>();
        this.numdoc=this.infoTributaria.getEstab()+this.infoTributaria.getPtoEmi()+this.infoTributaria.getSecuencial();
        for (InfoAdicional.CampoAdicional campoAdicional : this.infoAdicional.campoAdicional) {
            String[] cadenaValues=campoAdicional.getValue().split("\\|");
            for (int i = 0; i < cadenaValues.length; i++) {                
                String[] cadena  = cadenaValues[i].split(":");
                if(cadena.length>0 && SisVar.etiquetasInformacionAdicional.contains(cadena[0].toUpperCase())){
                    listDatos.add(new DatoAdicional(cadenaValues[i]));
                    if("EMAILCLIENTE".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.correo=cadena[1];
                    }
                    if("SECFACTURA".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.secuencia=cadena[1];
                    }
                }
            }
        }    
    }

    /**
     * Gets the value of the infoTributaria property.
     * 
     * @return
     *     possible object is
     *     {@link NotaCreditoXml.InfoTributaria }
     *     
     */
    public NotaCreditoXml.InfoTributaria getInfoTributaria() {
        return infoTributaria;
    }

    /**
     * Sets the value of the infoTributaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotaCreditoXml.InfoTributaria }
     *     
     */
    public void setInfoTributaria(NotaCreditoXml.InfoTributaria value) {
        this.infoTributaria = value;
    }

    /**
     * Gets the value of the infoNotaCreditoXml property.
     * 
     * @return
     *     possible object is
     *     {@link NotaCreditoXml.InfoNotaCreditoXml }
     *     
     */
    public NotaCreditoXml.InfoNotaCredito getInfoNotaCredito() {
        return infoNotaCredito;
    }

    /**
     * Sets the value of the infoNotaCreditoXml property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotaCreditoXml.InfoNotaCreditoXml }
     *     
     */
    public void setInfoNotaCredito(NotaCreditoXml.InfoNotaCredito value) {
        this.infoNotaCredito = value;
    }

    /**
     * Gets the value of the detalles property.
     * 
     * @return
     *     possible object is
     *     {@link NotaCreditoXml.Detalles }
     *     
     */
    public NotaCreditoXml.Detalles getDetalles() {
        return detalles;
    }

    /**
     * Sets the value of the detalles property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotaCreditoXml.Detalles }
     *     
     */
    public void setDetalles(NotaCreditoXml.Detalles value) {
        this.detalles = value;
    }

    /**
     * Gets the value of the infoAdicional property.
     * 
     * @return
     *     possible object is
     *     {@link NotaCreditoXml.InfoAdicional }
     *     
     */
    public NotaCreditoXml.InfoAdicional getInfoAdicional() {
        return infoAdicional;
    }

    /**
     * Sets the value of the infoAdicional property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotaCreditoXml.InfoAdicional }
     *     
     */
    public void setInfoAdicional(NotaCreditoXml.InfoAdicional value) {
        this.infoAdicional = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    public List<DatoAdicional> getListDatos() {
        return listDatos;
    }

    public void setListDatos(List<DatoAdicional> listDatos) {
        this.listDatos = listDatos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detalle"
    })
    public static class Detalles {

        @XmlElement(required = true)
        protected List<NotaCreditoXml.Detalles.Detalle> detalle;

        /**
         * Gets the value of the detalle property.
         * 
         * @return
         *     possible object is
         *     {@link NotaCreditoXml.Detalles.Detalle }
         *     
         */
        public List<NotaCreditoXml.Detalles.Detalle> getDetalle() {
            if(this.detalle==null)
                this.detalle = new ArrayList<>();
            return this.detalle;
        }

        /**
         * Sets the value of the detalle property.
         * 
         * @param value
         *     allowed object is
         *     {@link NotaCreditoXml.Detalles.Detalle }
         *     
         */
        public void setDetalle(List<NotaCreditoXml.Detalles.Detalle> value) {
            this.detalle = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigoInterno",
            "codigoAdicional",
            "descripcion",
            "cantidad",
            "precioUnitario",
            "descuento",
            "precioTotalSinImpuesto",
            "impuestos"
        })
        public static class Detalle {

            protected String codigoInterno;
            protected String codigoAdicional;
            @XmlElement(required = true)
            protected String descripcion;
            protected Double cantidad;
            protected Double precioUnitario;
            protected Double descuento;
            protected Double precioTotalSinImpuesto;
            @XmlElement(required = true)
            protected NotaCreditoXml.Detalles.Detalle.Impuestos impuestos;

            /**
             * Gets the value of the codigoInterno property.
             * 
             */
            public String getCodigoInterno() {
                return codigoInterno;
            }

            /**
             * Sets the value of the codigoInterno property.
             * 
             */
            public void setCodigoInterno(String value) {
                this.codigoInterno = value;
            }

            /**
             * Gets the value of the codigoAdicional property.
             * 
             */
            public String getCodigoAdicional() {
                return codigoAdicional;
            }

            /**
             * Sets the value of the codigoAdicional property.
             * 
             */
            public void setCodigoAdicional(String value) {
                this.codigoAdicional = value;
            }

            /**
             * Gets the value of the descripcion property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Sets the value of the descripcion property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

            /**
             * Gets the value of the cantidad property.
             * 
             */
            public Double getCantidad() {
                return cantidad;
            }

            /**
             * Sets the value of the cantidad property.
             * 
             */
            public void setCantidad(Double value) {
                this.cantidad = value;
            }

            /**
             * Gets the value of the precioUnitario property.
             * 
             */
            public Double getPrecioUnitario() {
                return precioUnitario;
            }

            /**
             * Sets the value of the precioUnitario property.
             * 
             */
            public void setPrecioUnitario(Double value) {
                this.precioUnitario = value;
            }

            /**
             * Gets the value of the descuento property.
             * 
             */
            public Double getDescuento() {
                return descuento;
            }

            /**
             * Sets the value of the descuento property.
             * 
             */
            public void setDescuento(Double value) {
                this.descuento = value;
            }

            /**
             * Gets the value of the precioTotalSinImpuesto property.
             * 
             */
            public Double getPrecioTotalSinImpuesto() {
                return precioTotalSinImpuesto;
            }

            /**
             * Sets the value of the precioTotalSinImpuesto property.
             * 
             */
            public void setPrecioTotalSinImpuesto(Double value) {
                this.precioTotalSinImpuesto = value;
            }

            /**
             * Gets the value of the impuestos property.
             * 
             * @return
             *     possible object is
             *     {@link NotaCreditoXml.Detalles.Detalle.Impuestos }
             *     
             */
            public NotaCreditoXml.Detalles.Detalle.Impuestos getImpuestos() {
                return impuestos;
            }

            /**
             * Sets the value of the impuestos property.
             * 
             * @param value
             *     allowed object is
             *     {@link NotaCreditoXml.Detalles.Detalle.Impuestos }
             *     
             */
            public void setImpuestos(NotaCreditoXml.Detalles.Detalle.Impuestos value) {
                this.impuestos = value;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "impuesto"
            })
            public static class Impuestos {

                @XmlElement(required = true)
                protected NotaCreditoXml.Detalles.Detalle.Impuestos.Impuesto impuesto;

                /**
                 * Gets the value of the impuesto property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link NotaCreditoXml.Detalles.Detalle.Impuestos.Impuesto }
                 *     
                 */
                public NotaCreditoXml.Detalles.Detalle.Impuestos.Impuesto getImpuesto() {
                    return impuesto;
                }

                /**
                 * Sets the value of the impuesto property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link NotaCreditoXml.Detalles.Detalle.Impuestos.Impuesto }
                 *     
                 */
                public void setImpuesto(NotaCreditoXml.Detalles.Detalle.Impuestos.Impuesto value) {
                    this.impuesto = value;
                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "codigo",
                    "codigoPorcentaje",
                    "tarifa",
                    "baseImponible",
                    "valor"
                })
                public static class Impuesto {

                    protected String codigo;
                    protected String codigoPorcentaje;
                    protected Double tarifa;
                    protected Double baseImponible;
                    protected Double valor;

                    /**
                     * Gets the value of the codigo property.
                     * 
                     */
                    public String getCodigo() {
                        return codigo;
                    }

                    /**
                     * Sets the value of the codigo property.
                     * 
                     */
                    public void setCodigo(String value) {
                        this.codigo = value;
                    }

                    /**
                     * Gets the value of the codigoPorcentaje property.
                     * 
                     */
                    public String getCodigoPorcentaje() {
                        return codigoPorcentaje;
                    }

                    /**
                     * Sets the value of the codigoPorcentaje property.
                     * 
                     */
                    public void setCodigoPorcentaje(String value) {
                        this.codigoPorcentaje = value;
                    }

                    /**
                     * Gets the value of the tarifa property.
                     * 
                     */
                    public Double getTarifa() {
                        return tarifa;
                    }

                    /**
                     * Sets the value of the tarifa property.
                     * 
                     */
                    public void setTarifa(Double value) {
                        this.tarifa = value;
                    }

                    /**
                     * Gets the value of the baseImponible property.
                     * 
                     */
                    public Double getBaseImponible() {
                        return baseImponible;
                    }

                    /**
                     * Sets the value of the baseImponible property.
                     * 
                     */
                    public void setBaseImponible(Double value) {
                        this.baseImponible = value;
                    }

                    /**
                     * Gets the value of the valor property.
                     * 
                     */
                    public Double getValor() {
                        return valor;
                    }

                    /**
                     * Sets the value of the valor property.
                     * 
                     */
                    public void setValor(Double value) {
                        this.valor = value;
                    }

                }

            }

        }

    }


    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "campoAdicional"
    })
    public static class InfoAdicional {

        protected List<NotaCreditoXml.InfoAdicional.CampoAdicional> campoAdicional;

        
        public List<NotaCreditoXml.InfoAdicional.CampoAdicional> getCampoAdicional() {
            if (campoAdicional == null) {
                campoAdicional = new ArrayList<NotaCreditoXml.InfoAdicional.CampoAdicional>();
            }
            return this.campoAdicional;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CampoAdicional {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "nombre")
            protected String nombre;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the nombre property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNombre() {
                return nombre;
            }

            /**
             * Sets the value of the nombre property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNombre(String value) {
                this.nombre = value;
            }

        }

    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fechaEmision",
        "dirEstablecimiento",
        "tipoIdentificacionComprador",
        "razonSocialComprador",
        "identificacionComprador",
        "contribuyenteEspecial",
        "obligadoContabilidad",
        "codDocModificado",
        "numDocModificado",
        "fechaEmisionDocSustento",
        "totalSinImpuestos",
        "valorModificacion",
        "moneda",
        "totalConImpuestos",
        "motivo"
    })
    public static class InfoNotaCredito {

        @XmlElement(required = true)
        protected String fechaEmision;
        @XmlElement(required = true)
        protected String dirEstablecimiento;
        protected String tipoIdentificacionComprador;
        @XmlElement(required = true)
        protected String razonSocialComprador;
        protected String identificacionComprador;
        protected String contribuyenteEspecial;
        @XmlElement(required = true)
        protected String obligadoContabilidad;
        protected String codDocModificado;
        @XmlElement(required = true)
        protected String numDocModificado;
        @XmlElement(required = true)
        protected String fechaEmisionDocSustento;
        protected Double totalSinImpuestos;
        protected Double valorModificacion;
        @XmlElement(required = true)
        protected String moneda;
        @XmlElement(required = true)
        protected NotaCreditoXml.InfoNotaCredito.TotalConImpuestos totalConImpuestos;
        @XmlElement(required = true)
        protected String motivo;

        /**
         * Gets the value of the fechaEmision property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaEmision() {
            return fechaEmision;
        }

        /**
         * Sets the value of the fechaEmision property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaEmision(String value) {
            this.fechaEmision = value;
        }

        /**
         * Gets the value of the dirEstablecimiento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirEstablecimiento() {
            return dirEstablecimiento;
        }

        /**
         * Sets the value of the dirEstablecimiento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirEstablecimiento(String value) {
            this.dirEstablecimiento = value;
        }

        /**
         * Gets the value of the tipoIdentificacionComprador property.
         * 
         */
        public String getTipoIdentificacionComprador() {
            return tipoIdentificacionComprador;
        }

        /**
         * Sets the value of the tipoIdentificacionComprador property.
         * 
         */
        public void setTipoIdentificacionComprador(String value) {
            this.tipoIdentificacionComprador = value;
        }

        /**
         * Gets the value of the razonSocialComprador property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocialComprador() {
            return razonSocialComprador;
        }

        /**
         * Sets the value of the razonSocialComprador property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocialComprador(String value) {
            this.razonSocialComprador = value;
        }

        /**
         * Gets the value of the identificacionComprador property.
         * 
         */
        public String getIdentificacionComprador() {
            return identificacionComprador;
        }

        /**
         * Sets the value of the identificacionComprador property.
         * 
         */
        public void setIdentificacionComprador(String value) {
            this.identificacionComprador = value;
        }

        /**
         * Gets the value of the contribuyenteEspecial property.
         * 
         */
        public String getContribuyenteEspecial() {
            return contribuyenteEspecial;
        }

        /**
         * Sets the value of the contribuyenteEspecial property.
         * 
         */
        public void setContribuyenteEspecial(String value) {
            this.contribuyenteEspecial = value;
        }

        /**
         * Gets the value of the obligadoContabilidad property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObligadoContabilidad() {
            return obligadoContabilidad;
        }

        /**
         * Sets the value of the obligadoContabilidad property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObligadoContabilidad(String value) {
            this.obligadoContabilidad = value;
        }

        /**
         * Gets the value of the codDocModificado property.
         * 
         */
        public String getCodDocModificado() {
            return codDocModificado;
        }

        /**
         * Sets the value of the codDocModificado property.
         * 
         */
        public void setCodDocModificado(String value) {
            this.codDocModificado = value;
        }

        /**
         * Gets the value of the numDocModificado property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumDocModificado() {
            return numDocModificado;
        }

        /**
         * Sets the value of the numDocModificado property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumDocModificado(String value) {
            this.numDocModificado = value;
        }

        /**
         * Gets the value of the fechaEmisionDocSustento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaEmisionDocSustento() {
            return fechaEmisionDocSustento;
        }

        /**
         * Sets the value of the fechaEmisionDocSustento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaEmisionDocSustento(String value) {
            this.fechaEmisionDocSustento = value;
        }

        /**
         * Gets the value of the totalSinImpuestos property.
         * 
         */
        public Double getTotalSinImpuestos() {
            return totalSinImpuestos;
        }

        /**
         * Sets the value of the totalSinImpuestos property.
         * 
         */
        public void setTotalSinImpuestos(Double value) {
            this.totalSinImpuestos = value;
        }

        /**
         * Gets the value of the valorModificacion property.
         * 
         */
        public Double getValorModificacion() {
            return valorModificacion;
        }

        /**
         * Sets the value of the valorModificacion property.
         * 
         */
        public void setValorModificacion(Double value) {
            this.valorModificacion = value;
        }

        /**
         * Gets the value of the moneda property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoneda() {
            return moneda;
        }

        /**
         * Sets the value of the moneda property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoneda(String value) {
            this.moneda = value;
        }

        /**
         * Gets the value of the totalConImpuestos property.
         * 
         * @return
         *     possible object is
         *     {@link NotaCreditoXml.InfoNotaCreditoXml.TotalConImpuestos }
         *     
         */
        public NotaCreditoXml.InfoNotaCredito.TotalConImpuestos getTotalConImpuestos() {
            return totalConImpuestos;
        }

        /**
         * Sets the value of the totalConImpuestos property.
         * 
         * @param value
         *     allowed object is
         *     {@link NotaCreditoXml.InfoNotaCreditoXml.TotalConImpuestos }
         *     
         */
        public void setTotalConImpuestos(NotaCreditoXml.InfoNotaCredito.TotalConImpuestos value) {
            this.totalConImpuestos = value;
        }

        /**
         * Gets the value of the motivo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMotivo() {
            return motivo;
        }

        /**
         * Sets the value of the motivo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMotivo(String value) {
            this.motivo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="totalImpuesto">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                   &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                   &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "totalImpuesto"
        })
        public static class TotalConImpuestos {

            @XmlElement(required = true)
            protected List<NotaCreditoXml.InfoNotaCredito.TotalConImpuestos.TotalImpuesto> totalImpuesto;

            public List<NotaCreditoXml.InfoNotaCredito.TotalConImpuestos.TotalImpuesto> getTotalImpuesto() {
                if(this.totalImpuesto==null)
                    this.totalImpuesto = new ArrayList<>();
                return totalImpuesto;
            }
            public void setTotalImpuesto(List<NotaCreditoXml.InfoNotaCredito.TotalConImpuestos.TotalImpuesto> value) {
                this.totalImpuesto = value;
            }
            
            public Double obtenerImpuesto(String cod){
                for (TotalImpuesto i : this.totalImpuesto) {
                    if(i.getCodigo().equalsIgnoreCase(cod))
                        return i.getValor();
                }
                return new Double("0.00");
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "codigoPorcentaje",
                "baseImponible",
                "valor"
            })
            public static class TotalImpuesto {

                protected String codigo;
                protected String codigoPorcentaje;
                protected Double baseImponible;
                protected Double valor;

                /**
                 * Gets the value of the codigo property.
                 * 
                 */
                public String getCodigo() {
                    return codigo;
                }

                /**
                 * Sets the value of the codigo property.
                 * 
                 */
                public void setCodigo(String value) {
                    this.codigo = value;
                }

                /**
                 * Gets the value of the codigoPorcentaje property.
                 * 
                 */
                public String getCodigoPorcentaje() {
                    return codigoPorcentaje;
                }

                /**
                 * Sets the value of the codigoPorcentaje property.
                 * 
                 */
                public void setCodigoPorcentaje(String value) {
                    this.codigoPorcentaje = value;
                }

                /**
                 * Gets the value of the baseImponible property.
                 * 
                 */
                public Double getBaseImponible() {
                    return baseImponible;
                }

                /**
                 * Sets the value of the baseImponible property.
                 * 
                 */
                public void setBaseImponible(Double value) {
                    this.baseImponible = value;
                }

                /**
                 * Gets the value of the valor property.
                 * 
                 */
                public Double getValor() {
                    return valor;
                }

                /**
                 * Sets the value of the valor property.
                 * 
                 */
                public void setValor(Double value) {
                    this.valor = value;
                }

            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ambiente" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="tipoEmision" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nombreComercial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ruc" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="claveAcceso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="codDoc" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="estab" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ptoEmi" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="secuencial" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="dirMatriz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ambiente",
        "tipoEmision",
        "razonSocial",
        "nombreComercial",
        "ruc",
        "claveAcceso",
        "codDoc",
        "estab",
        "ptoEmi",
        "secuencial",
        "dirMatriz"
    })
    public static class InfoTributaria {

        protected String ambiente;
        protected String tipoEmision;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected String nombreComercial;
        protected String ruc;
        @XmlElement(required = true)
        protected String claveAcceso;
        protected String codDoc;
        protected String estab;
        protected String ptoEmi;
        protected String secuencial;
        @XmlElement(required = true)
        protected String dirMatriz;

        /**
         * Gets the value of the ambiente property.
         * 
         */
        public String getAmbiente() {
            return ambiente;
        }

        /**
         * Sets the value of the ambiente property.
         * 
         */
        public void setAmbiente(String value) {
            this.ambiente = value;
        }

        /**
         * Gets the value of the tipoEmision property.
         * 
         */
        public String getTipoEmision() {
            return tipoEmision;
        }

        /**
         * Sets the value of the tipoEmision property.
         * 
         */
        public void setTipoEmision(String value) {
            this.tipoEmision = value;
        }

        /**
         * Gets the value of the razonSocial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocial() {
            return razonSocial;
        }

        /**
         * Sets the value of the razonSocial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocial(String value) {
            this.razonSocial = value;
        }

        /**
         * Gets the value of the nombreComercial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreComercial() {
            return nombreComercial;
        }

        /**
         * Sets the value of the nombreComercial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreComercial(String value) {
            this.nombreComercial = value;
        }

        /**
         * Gets the value of the ruc property.
         * 
         */
        public String getRuc() {
            return ruc;
        }

        /**
         * Sets the value of the ruc property.
         * 
         */
        public void setRuc(String value) {
            this.ruc = value;
        }

        /**
         * Gets the value of the claveAcceso property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public String getClaveAcceso() {
            return claveAcceso;
        }

        /**
         * Sets the value of the claveAcceso property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setClaveAcceso(String value) {
            this.claveAcceso = value;
        }

        /**
         * Gets the value of the codDoc property.
         * 
         */
        public String getCodDoc() {
            return codDoc;
        }

        /**
         * Sets the value of the codDoc property.
         * 
         */
        public void setCodDoc(String value) {
            this.codDoc = value;
        }

        /**
         * Gets the value of the estab property.
         * 
         */
        public String getEstab() {
            return estab;
        }

        /**
         * Sets the value of the estab property.
         * 
         */
        public void setEstab(String value) {
            this.estab = value;
        }

        /**
         * Gets the value of the ptoEmi property.
         * 
         */
        public String getPtoEmi() {
            return ptoEmi;
        }

        /**
         * Sets the value of the ptoEmi property.
         * 
         */
        public void setPtoEmi(String value) {
            this.ptoEmi = value;
        }

        /**
         * Gets the value of the secuencial property.
         * 
         */
        public String getSecuencial() {
            return secuencial;
        }

        /**
         * Sets the value of the secuencial property.
         * 
         */
        public void setSecuencial(String value) {
            this.secuencial = value;
        }

        /**
         * Gets the value of the dirMatriz property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirMatriz() {
            return dirMatriz;
        }

        /**
         * Sets the value of the dirMatriz property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirMatriz(String value) {
            this.dirMatriz = value;
        }

    }
}
