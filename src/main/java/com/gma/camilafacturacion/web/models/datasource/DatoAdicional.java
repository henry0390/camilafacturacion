/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class DatoAdicional implements Serializable{
    private static final long serialVersionUID = 1L;
    private String etiqueta;
    private String valor;

    public DatoAdicional() {
    }

    public DatoAdicional(String etiqueta, String valor) {
        this.etiqueta = etiqueta;
        this.valor = valor;
    }
    
    public DatoAdicional(String total) {
        String[] cadena  = total.split(":");
        if(cadena.length>0)
            this.etiqueta = cadena[0];
        if(cadena.length>1)
            this.valor = cadena[1];
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
