/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.SisVar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Acer
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoTributaria",
    "infoFactura",
    "detalles",
    "infoAdicional"
})
@XmlRootElement(name = "factura")
public class FacturaXml {
    @XmlElement(required = true)
    protected FacturaXml.InfoTributaria infoTributaria;
    @XmlElement(required = true)
    protected FacturaXml.InfoFactura infoFactura;
    @XmlElement(required = true)
    protected FacturaXml.Detalles detalles;
    @XmlElement(required = true)
    protected FacturaXml.InfoAdicional infoAdicional;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlTransient
    protected List<DatoAdicional> listDatos;
    @XmlTransient
    protected String correo;
    @XmlTransient
    protected String secuencia;
    @XmlTransient
    protected String numdoc;
    @XmlTransient
    private TbUsuario usuario;
    
    public void cargarDatosAdicionales(){
        this.listDatos = new ArrayList<>();
        this.numdoc=this.infoTributaria.getEstab()+this.infoTributaria.getPtoEmi()+this.infoTributaria.getSecuencial();
        for (InfoAdicional.CampoAdicional campoAdicional : this.infoAdicional.campoAdicional) {
            String[] cadenaValues=campoAdicional.getValue().split("\\|");
            for (int i = 0; i < cadenaValues.length; i++) {                
                String[] cadena  = cadenaValues[i].split(":");
                if(cadena.length>0 && SisVar.etiquetasInformacionAdicional.contains(cadena[0].toUpperCase())){
                    listDatos.add(new DatoAdicional(cadenaValues[i]));
                    if("EMAILCLIENTE".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.correo=cadena[1];
                    }
                    if("SECFACTURA".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.secuencia=cadena[1];
                    }
                }
            }
        }
    }

    public InfoTributaria getInfoTributaria() {
        return infoTributaria;
    }

    public void setInfoTributaria(InfoTributaria infoTributaria) {
        this.infoTributaria = infoTributaria;
    }

    public InfoFactura getInfoFactura() {
        return infoFactura;
    }

    public void setInfoFactura(InfoFactura infoFactura) {
        this.infoFactura = infoFactura;
    }

    public Detalles getDetalles() {
        return detalles;
    }

    public void setDetalles(Detalles detalles) {
        this.detalles = detalles;
    }

    public InfoAdicional getInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(InfoAdicional infoAdicional) {
        this.infoAdicional = infoAdicional;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<DatoAdicional> getListDatos() {
        return listDatos;
    }

    public void setListDatos(List<DatoAdicional> listDatos) {
        this.listDatos = listDatos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ambiente",
        "tipoEmision",
        "razonSocial",
        "nombreComercial",
        "ruc",
        "claveAcceso",
        "codDoc",
        "estab",
        "ptoEmi",
        "secuencial",
        "dirMatriz"
    })
    public static class InfoTributaria {

        protected String ambiente;
        protected String tipoEmision;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected String nombreComercial;
        protected String ruc;
        @XmlElement(required = true)
        protected String claveAcceso;
        protected String codDoc;
        protected String estab;
        protected String ptoEmi;
        protected String secuencial;
        @XmlElement(required = true)
        protected String dirMatriz;

        /**
         * Gets the value of the ambiente property.
         * 
         */
        public String getAmbiente() {
            return ambiente;
        }

        /**
         * Sets the value of the ambiente property.
         * 
         */
        public void setAmbiente(String value) {
            this.ambiente = value;
        }

        /**
         * Gets the value of the tipoEmision property.
         * 
         */
        public String getTipoEmision() {
            return tipoEmision;
        }

        /**
         * Sets the value of the tipoEmision property.
         * 
         */
        public void setTipoEmision(String value) {
            this.tipoEmision = value;
        }

        /**
         * Gets the value of the razonSocial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocial() {
            return razonSocial;
        }

        /**
         * Sets the value of the razonSocial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocial(String value) {
            this.razonSocial = value;
        }

        /**
         * Gets the value of the nombreComercial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreComercial() {
            return nombreComercial;
        }

        /**
         * Sets the value of the nombreComercial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreComercial(String value) {
            this.nombreComercial = value;
        }

        /**
         * Gets the value of the ruc property.
         * 
         */
        public String getRuc() {
            return ruc;
        }

        /**
         * Sets the value of the ruc property.
         * 
         */
        public void setRuc(String value) {
            this.ruc = value;
        }

        /**
         * Gets the value of the claveAcceso property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public String getClaveAcceso() {
            return claveAcceso;
        }

        /**
         * Sets the value of the claveAcceso property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setClaveAcceso(String value) {
            this.claveAcceso = value;
        }

        /**
         * Gets the value of the codDoc property.
         * 
         */
        public String getCodDoc() {
            return codDoc;
        }

        /**
         * Sets the value of the codDoc property.
         * 
         */
        public void setCodDoc(String value) {
            this.codDoc = value;
        }

        /**
         * Gets the value of the estab property.
         * 
         */
        public String getEstab() {
            return estab;
        }

        /**
         * Sets the value of the estab property.
         * 
         */
        public void setEstab(String value) {
            this.estab = value;
        }

        /**
         * Gets the value of the ptoEmi property.
         * 
         */
        public String getPtoEmi() {
            return ptoEmi;
        }

        /**
         * Sets the value of the ptoEmi property.
         * 
         */
        public void setPtoEmi(String value) {
            this.ptoEmi = value;
        }

        /**
         * Gets the value of the secuencial property.
         * 
         */
        public String getSecuencial() {
            return secuencial;
        }

        /**
         * Sets the value of the secuencial property.
         * 
         */
        public void setSecuencial(String value) {
            this.secuencial = value;
        }

        /**
         * Gets the value of the dirMatriz property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirMatriz() {
            return dirMatriz;
        }

        /**
         * Sets the value of the dirMatriz property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirMatriz(String value) {
            this.dirMatriz = value;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fechaEmision",
        "dirEstablecimiento",
        "contribuyenteEspecial",
        "obligadoContabilidad",
        "tipoIdentificacionComprador",
        "razonSocialComprador",
        "identificacionComprador",
        "totalSinImpuestos",
        "totalDescuento",
        "totalConImpuestos",
        "propina",
        "importeTotal",
        "moneda",
        "pagos"
    })
    public static class InfoFactura {

        @XmlElement(required = true)
        protected String fechaEmision;
        @XmlElement(required = true)
        protected String dirEstablecimiento;
        protected String contribuyenteEspecial;
        @XmlElement(required = true)
        protected String obligadoContabilidad;
        protected String tipoIdentificacionComprador;
        @XmlElement(required = true)
        protected String razonSocialComprador;
        protected String identificacionComprador;
        protected Double totalSinImpuestos;
        protected Double totalDescuento;
        @XmlElement(required = true)
        protected FacturaXml.InfoFactura.TotalConImpuestos totalConImpuestos;
        protected Double propina;
        protected Double importeTotal;
        @XmlElement(required = true)
        protected String moneda;
        protected FacturaXml.InfoFactura.Pagos pagos;

        /**
         * Gets the value of the fechaEmision property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaEmision() {
            return fechaEmision;
        }

        /**
         * Sets the value of the fechaEmision property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaEmision(String value) {
            this.fechaEmision = value;
        }

        /**
         * Gets the value of the dirEstablecimiento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirEstablecimiento() {
            return dirEstablecimiento;
        }

        /**
         * Sets the value of the dirEstablecimiento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirEstablecimiento(String value) {
            this.dirEstablecimiento = value;
        }

        public String getContribuyenteEspecial() {
            return contribuyenteEspecial;
        }

        public void setContribuyenteEspecial(String contribuyenteEspecial) {
            this.contribuyenteEspecial = contribuyenteEspecial;
        }

        /**
         * Gets the value of the obligadoContabilidad property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObligadoContabilidad() {
            return obligadoContabilidad;
        }

        /**
         * Sets the value of the obligadoContabilidad property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObligadoContabilidad(String value) {
            this.obligadoContabilidad = value;
        }

        /**
         * Gets the value of the tipoIdentificacionComprador property.
         * 
         */
        public String getTipoIdentificacionComprador() {
            return tipoIdentificacionComprador;
        }

        /**
         * Sets the value of the tipoIdentificacionComprador property.
         * 
         */
        public void setTipoIdentificacionComprador(String value) {
            this.tipoIdentificacionComprador = value;
        }

        /**
         * Gets the value of the razonSocialComprador property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocialComprador() {
            return razonSocialComprador;
        }

        /**
         * Sets the value of the razonSocialComprador property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocialComprador(String value) {
            this.razonSocialComprador = value;
        }

        /**
         * Gets the value of the identificacionComprador property.
         * 
         */
        public String getIdentificacionComprador() {
            return identificacionComprador;
        }

        /**
         * Sets the value of the identificacionComprador property.
         * 
         */
        public void setIdentificacionComprador(String value) {
            this.identificacionComprador = value;
        }

        /**
         * Gets the value of the totalSinImpuestos property.
         * 
         */
        public Double getTotalSinImpuestos() {
            return totalSinImpuestos;
        }

        /**
         * Sets the value of the totalSinImpuestos property.
         * 
         */
        public void setTotalSinImpuestos(Double value) {
            this.totalSinImpuestos = value;
        }

        /**
         * Gets the value of the totalDescuento property.
         * 
         */
        public Double getTotalDescuento() {
            return totalDescuento;
        }

        /**
         * Sets the value of the totalDescuento property.
         * 
         */
        public void setTotalDescuento(Double value) {
            this.totalDescuento = value;
        }

        /**
         * Gets the value of the totalConImpuestos property.
         * 
         * @return
         *     possible object is
         *     {@link Factura.InfoFactura.TotalConImpuestos }
         *     
         */
        public FacturaXml.InfoFactura.TotalConImpuestos getTotalConImpuestos() {
            return totalConImpuestos;
        }

        /**
         * Sets the value of the totalConImpuestos property.
         * 
         * @param value
         *     allowed object is
         *     {@link Factura.InfoFactura.TotalConImpuestos }
         *     
         */
        public void setTotalConImpuestos(FacturaXml.InfoFactura.TotalConImpuestos value) {
            this.totalConImpuestos = value;
        }

        /**
         * Gets the value of the propina property.
         * 
         */
        public Double getPropina() {
            return propina;
        }

        /**
         * Sets the value of the propina property.
         * 
         */
        public void setPropina(Double value) {
            this.propina = value;
        }

        /**
         * Gets the value of the importeTotal property.
         * 
         */
        public Double getImporteTotal() {
            return importeTotal;
        }

        /**
         * Sets the value of the importeTotal property.
         * 
         */
        public void setImporteTotal(Double value) {
            this.importeTotal = value;
        }

        /**
         * Gets the value of the moneda property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMoneda() {
            return moneda;
        }

        /**
         * Sets the value of the moneda property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMoneda(String value) {
            this.moneda = value;
        }

        public Pagos getPagos() {
            return pagos;
        }

        public void setPagos(Pagos pagos) {
            this.pagos = pagos;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="totalImpuesto" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                   &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                   &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "totalImpuesto"
        })
        public static class TotalConImpuestos {

            protected List<FacturaXml.InfoFactura.TotalConImpuestos.TotalImpuesto> totalImpuesto;

            
            public List<FacturaXml.InfoFactura.TotalConImpuestos.TotalImpuesto> getTotalImpuesto() {
                if (totalImpuesto == null) {
                    totalImpuesto = new ArrayList<>();
                }
                return this.totalImpuesto;
            }
            
            public Double valorTotal(){
                Double valor=0.00;
                for (TotalImpuesto t : this.totalImpuesto) {
                    valor = valor +t.valor;
                }
                return valor;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *         &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *         &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "codigoPorcentaje",
                "baseImponible",
                "valor"
            })
            public static class TotalImpuesto {

                protected byte codigo;
                protected byte codigoPorcentaje;
                protected Double baseImponible;
                protected Double valor;

                /**
                 * Gets the value of the codigo property.
                 * 
                 */
                public byte getCodigo() {
                    return codigo;
                }

                /**
                 * Sets the value of the codigo property.
                 * 
                 */
                public void setCodigo(byte value) {
                    this.codigo = value;
                }

                /**
                 * Gets the value of the codigoPorcentaje property.
                 * 
                 */
                public byte getCodigoPorcentaje() {
                    return codigoPorcentaje;
                }

                /**
                 * Sets the value of the codigoPorcentaje property.
                 * 
                 */
                public void setCodigoPorcentaje(byte value) {
                    this.codigoPorcentaje = value;
                }

                /**
                 * Gets the value of the baseImponible property.
                 * 
                 */
                public Double getBaseImponible() {
                    return baseImponible;
                }

                /**
                 * Sets the value of the baseImponible property.
                 * 
                 */
                public void setBaseImponible(Double value) {
                    this.baseImponible = value;
                }

                /**
                 * Gets the value of the valor property.
                 * 
                 */
                public Double getValor() {
                    return valor;
                }

                /**
                 * Sets the value of the valor property.
                 * 
                 */
                public void setValor(Double value) {
                    this.valor = value;
                }

            }

        }
        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pago"
        })
        public static class Pagos {

            protected List<FacturaXml.InfoFactura.Pagos.Pago> pago;

            public List<Pago> getPago() {
                if(pago==null)
                    return new ArrayList<>();
                return pago;
            }

            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "formaPago",
                "total",
                "plazo",
                "unidadTiempo"
            })
            public static class Pago {

                protected String formaPago;
                protected Double total;
                protected byte plazo;
                protected String unidadTiempo;

                public String getFormaPago() {
                    return formaPago;
                }

                public void setFormaPago(String formaPago) {
                    this.formaPago = formaPago;
                }

                public Double getTotal() {
                    return total;
                }

                public void setTotal(Double total) {
                    this.total = total;
                }

                public byte getPlazo() {
                    return plazo;
                }

                public void setPlazo(byte plazo) {
                    this.plazo = plazo;
                }

                public String getUnidadTiempo() {
                    return unidadTiempo;
                }

                public void setUnidadTiempo(String unidadTiempo) {
                    this.unidadTiempo = unidadTiempo;
                }
                
                
            }
        }


    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detalle"
    })
    public static class Detalles {

        @XmlElement(required = true)
        protected List<FacturaXml.Detalles.Detalle> detalle;

        /**
         * Gets the value of the detalle property.
         * 
         * @return
         *     possible object is
         *     {@link Factura.Detalles.Detalle }
         *     
         */
        
        public List<FacturaXml.Detalles.Detalle> getDetalle() {
            if (detalle == null) {
                detalle = new ArrayList<FacturaXml.Detalles.Detalle>();
            }
            return this.detalle;
        }
        /*public FacturaXml.Detalles.Detalle getDetalle() {
            return detalle;
        }*/

        /**
         * Sets the value of the detalle property.
         * 
         * @param value
         *     allowed object is
         *     {@link Factura.Detalles.Detalle }
         *     
         */
        /*public void setDetalle(FacturaXml.Detalles.Detalle value) {
            this.detalle = value;
        }*/


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigoPrincipal" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *         &lt;element name="codigoAuxiliar" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="precioUnitario" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="descuento" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="precioTotalSinImpuesto" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="impuestos">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="impuesto">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *                             &lt;element name="tarifa" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                             &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                             &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigoPrincipal",
            "codigoAuxiliar",
            "descripcion",
            "cantidad",
            "precioUnitario",
            "descuento",
            "precioTotalSinImpuesto",
            "impuestos"
        })
        public static class Detalle {

            protected String codigoPrincipal;
            protected String codigoAuxiliar;
            @XmlElement(required = true)
            protected String descripcion;
            protected BigDecimal cantidad;
            protected BigDecimal precioUnitario;
            protected BigDecimal descuento;
            protected BigDecimal precioTotalSinImpuesto;
            @XmlElement(required = true)
            protected FacturaXml.Detalles.Detalle.Impuestos impuestos;

            /**
             * Gets the value of the codigoPrincipal property.
             * 
             */
            public String getCodigoPrincipal() {
                return codigoPrincipal;
            }

            /**
             * Sets the value of the codigoPrincipal property.
             * 
             */
            public void setCodigoPrincipal(String value) {
                this.codigoPrincipal = value;
            }

            /**
             * Gets the value of the codigoAuxiliar property.
             * 
             */
            public String getCodigoAuxiliar() {
                return codigoAuxiliar;
            }

            /**
             * Sets the value of the codigoAuxiliar property.
             * 
             */
            public void setCodigoAuxiliar(String value) {
                this.codigoAuxiliar = value;
            }

            /**
             * Gets the value of the descripcion property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescripcion() {
                return descripcion;
            }

            /**
             * Sets the value of the descripcion property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescripcion(String value) {
                this.descripcion = value;
            }

            /**
             * Gets the value of the cantidad property.
             * 
             */
            public BigDecimal getCantidad() {
                return cantidad;
            }

            /**
             * Sets the value of the cantidad property.
             * 
             */
            public void setCantidad(BigDecimal value) {
                this.cantidad = value;
            }

            /**
             * Gets the value of the precioUnitario property.
             * 
             */
            public BigDecimal getPrecioUnitario() {
                return precioUnitario;
            }

            /**
             * Sets the value of the precioUnitario property.
             * 
             */
            public void setPrecioUnitario(BigDecimal value) {
                this.precioUnitario = value;
            }

            /**
             * Gets the value of the descuento property.
             * 
             */
            public BigDecimal getDescuento() {
                return descuento;
            }

            /**
             * Sets the value of the descuento property.
             * 
             */
            public void setDescuento(BigDecimal value) {
                this.descuento = value;
            }

            /**
             * Gets the value of the precioTotalSinImpuesto property.
             * 
             */
            public BigDecimal getPrecioTotalSinImpuesto() {
                return precioTotalSinImpuesto;
            }

            /**
             * Sets the value of the precioTotalSinImpuesto property.
             * 
             */
            public void setPrecioTotalSinImpuesto(BigDecimal value) {
                this.precioTotalSinImpuesto = value;
            }

            /**
             * Gets the value of the impuestos property.
             * 
             * @return
             *     possible object is
             *     {@link Factura.Detalles.Detalle.Impuestos }
             *     
             */
            public FacturaXml.Detalles.Detalle.Impuestos getImpuestos() {
                return impuestos;
            }

            /**
             * Sets the value of the impuestos property.
             * 
             * @param value
             *     allowed object is
             *     {@link Factura.Detalles.Detalle.Impuestos }
             *     
             */
            public void setImpuestos(FacturaXml.Detalles.Detalle.Impuestos value) {
                this.impuestos = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="impuesto">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
             *                   &lt;element name="tarifa" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *                   &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "impuesto"
            })
            public static class Impuestos {

                @XmlElement(required = true)
                protected FacturaXml.Detalles.Detalle.Impuestos.Impuesto impuesto;

                /**
                 * Gets the value of the impuesto property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Factura.Detalles.Detalle.Impuestos.Impuesto }
                 *     
                 */
                public FacturaXml.Detalles.Detalle.Impuestos.Impuesto getImpuesto() {
                    return impuesto;
                }

                /**
                 * Sets the value of the impuesto property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Factura.Detalles.Detalle.Impuestos.Impuesto }
                 *     
                 */
                public void setImpuesto(FacturaXml.Detalles.Detalle.Impuestos.Impuesto value) {
                    this.impuesto = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="codigoPorcentaje" type="{http://www.w3.org/2001/XMLSchema}byte"/>
                 *         &lt;element name="tarifa" type="{http://www.w3.org/2001/XMLSchema}float"/>
                 *         &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
                 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}float"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "codigo",
                    "codigoPorcentaje",
                    "tarifa",
                    "baseImponible",
                    "valor"
                })
                public static class Impuesto {

                    protected byte codigo;
                    protected byte codigoPorcentaje;
                    protected Double tarifa;
                    protected Double baseImponible;
                    protected Double valor;

                    /**
                     * Gets the value of the codigo property.
                     * 
                     */
                    public byte getCodigo() {
                        return codigo;
                    }

                    /**
                     * Sets the value of the codigo property.
                     * 
                     */
                    public void setCodigo(byte value) {
                        this.codigo = value;
                    }

                    /**
                     * Gets the value of the codigoPorcentaje property.
                     * 
                     */
                    public byte getCodigoPorcentaje() {
                        return codigoPorcentaje;
                    }

                    /**
                     * Sets the value of the codigoPorcentaje property.
                     * 
                     */
                    public void setCodigoPorcentaje(byte value) {
                        this.codigoPorcentaje = value;
                    }

                    /**
                     * Gets the value of the tarifa property.
                     * 
                     */
                    public Double getTarifa() {
                        return tarifa;
                    }

                    /**
                     * Sets the value of the tarifa property.
                     * 
                     */
                    public void setTarifa(Double value) {
                        this.tarifa = value;
                    }

                    /**
                     * Gets the value of the baseImponible property.
                     * 
                     */
                    public Double getBaseImponible() {
                        return baseImponible;
                    }

                    /**
                     * Sets the value of the baseImponible property.
                     * 
                     */
                    public void setBaseImponible(Double value) {
                        this.baseImponible = value;
                    }

                    /**
                     * Gets the value of the valor property.
                     * 
                     */
                    public Double getValor() {
                        return valor;
                    }

                    /**
                     * Sets the value of the valor property.
                     * 
                     */
                    public void setValor(Double value) {
                        this.valor = value;
                    }

                }

            }

        }

    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "campoAdicional"
    })
    public static class InfoAdicional {

        protected List<FacturaXml.InfoAdicional.CampoAdicional> campoAdicional;

        /**
         * Gets the value of the campoAdicional property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the campoAdicional property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCampoAdicional().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Factura.InfoAdicional.CampoAdicional }
         * 
         * 
         */
        public List<FacturaXml.InfoAdicional.CampoAdicional> getCampoAdicional() {
            if (campoAdicional == null) {
                campoAdicional = new ArrayList<FacturaXml.InfoAdicional.CampoAdicional>();
            }
            return this.campoAdicional;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CampoAdicional {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "nombre")
            protected String nombre;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the nombre property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNombre() {
                return nombre;
            }

            /**
             * Sets the value of the nombre property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNombre(String value) {
                this.nombre = value;
            }

        }

    }
}
