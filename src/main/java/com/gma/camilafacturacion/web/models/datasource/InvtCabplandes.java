/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class InvtCabplandes implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private int INVCPF_numguia;
    private String INVCPF_numero;
    private int INVCPF_secuencia;
    private Date INVCPF_fecha;
    private String INVCAM_codigo;
    private String CXCRUT_codrutadis;
    private String INVCPF_motivo;
    private BigDecimal INVCPF_totalpeso;
    private BigDecimal INVCPF_totalvolum;
    private int INVCPF_totaldocum;
    private String INVCPF_estdespach;
    private Date INVCPF_fechadespa;
    private Date INVCPF_fechaliqui;
    private Date INVCPF_fecharetor;
    private Date INVCPF_fechaplani;
    private Date INVCPF_fechaegres;
    private Date INVCPF_fechafactu;
    private Date INVCPF_fecharecha;
    private String INVCPF_estelimina;	
    private String INVCPF_estuso;
    private String INVCPF_cajero;	
    private int INVCPF_numguiarem;	
    private String FACDGA_numrecicaj;
    private String FACREC_codigo;
    private String INVCPF_ESTLIQPOR;
    private BigDecimal INVCPF_TOTPLANIF;
    private String INVCPF_TIPLIQCAM;
    private BigDecimal INVCPF_kilometraje;
    private BigDecimal INVCPF_kiometrajef;	
    private String INVCPF_tiporesp;
    private String INVCPF_tipoguia;
    private String INVCPF_estaproces;	
    private int RTKMOB_id;
    private int RTKMOB_sec;	
    private String INVCPF_RTKestado;
    private String INVCPF_estaliquida;	
    private String INVCPF_codusuario;
    private int GENTAL_secuencia;
    private String INVCPF_claveacceso;
    private Date INVCPF_fechaautoriza;	
    private String INVCPF_autorizacion;
    private String xmlText;
    private GuiaRemisionXml grxml;
    private TbUsuario usuario;
    private String estadoEnvio;
    private String idMensaje;
    private String estadoAutorizado;

    public InvtCabplandes() {
    }

    public InvtCabplandes(String GENCIA_codigo, String GENOFI_codigo, int INVCPF_numguia, String INVCPF_numero, int INVCPF_secuencia, Date INVCPF_fecha, String INVCAM_codigo, String CXCRUT_codrutadis, String INVCPF_motivo, BigDecimal INVCPF_totalpeso, BigDecimal INVCPF_totalvolum, int INVCPF_totaldocum, String INVCPF_estdespach, Date INVCPF_fechadespa, Date INVCPF_fechaliqui, Date INVCPF_fecharetor, Date INVCPF_fechaplani, Date INVCPF_fechaegres, Date INVCPF_fechafactu, Date INVCPF_fecharecha, String INVCPF_estelimina, String INVCPF_estuso, String INVCPF_cajero, int INVCPF_numguiarem, String FACDGA_numrecicaj, String FACREC_codigo, String INVCPF_ESTLIQPOR, BigDecimal INVCPF_TOTPLANIF, String INVCPF_TIPLIQCAM, BigDecimal INVCPF_kilometraje, BigDecimal INVCPF_kiometrajef, String INVCPF_tiporesp, String INVCPF_tipoguia, String INVCPF_estaproces, int RTKMOB_id, int RTKMOB_sec, String INVCPF_RTKestado, String INVCPF_estaliquida, String INVCPF_codusuario, int GENTAL_secuencia, String INVCPF_claveacceso, Date INVCPF_fechaautoriza, String INVCPF_autorizacion) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.INVCPF_numguia = INVCPF_numguia;
        this.INVCPF_numero = INVCPF_numero;
        this.INVCPF_secuencia = INVCPF_secuencia;
        this.INVCPF_fecha = INVCPF_fecha;
        this.INVCAM_codigo = INVCAM_codigo;
        this.CXCRUT_codrutadis = CXCRUT_codrutadis;
        this.INVCPF_motivo = INVCPF_motivo;
        this.INVCPF_totalpeso = INVCPF_totalpeso;
        this.INVCPF_totalvolum = INVCPF_totalvolum;
        this.INVCPF_totaldocum = INVCPF_totaldocum;
        this.INVCPF_estdespach = INVCPF_estdespach;
        this.INVCPF_fechadespa = INVCPF_fechadespa;
        this.INVCPF_fechaliqui = INVCPF_fechaliqui;
        this.INVCPF_fecharetor = INVCPF_fecharetor;
        this.INVCPF_fechaplani = INVCPF_fechaplani;
        this.INVCPF_fechaegres = INVCPF_fechaegres;
        this.INVCPF_fechafactu = INVCPF_fechafactu;
        this.INVCPF_fecharecha = INVCPF_fecharecha;
        this.INVCPF_estelimina = INVCPF_estelimina;
        this.INVCPF_estuso = INVCPF_estuso;
        this.INVCPF_cajero = INVCPF_cajero;
        this.INVCPF_numguiarem = INVCPF_numguiarem;
        this.FACDGA_numrecicaj = FACDGA_numrecicaj;
        this.FACREC_codigo = FACREC_codigo;
        this.INVCPF_ESTLIQPOR = INVCPF_ESTLIQPOR;
        this.INVCPF_TOTPLANIF = INVCPF_TOTPLANIF;
        this.INVCPF_TIPLIQCAM = INVCPF_TIPLIQCAM;
        this.INVCPF_kilometraje = INVCPF_kilometraje;
        this.INVCPF_kiometrajef = INVCPF_kiometrajef;
        this.INVCPF_tiporesp = INVCPF_tiporesp;
        this.INVCPF_tipoguia = INVCPF_tipoguia;
        this.INVCPF_estaproces = INVCPF_estaproces;
        this.RTKMOB_id = RTKMOB_id;
        this.RTKMOB_sec = RTKMOB_sec;
        this.INVCPF_RTKestado = INVCPF_RTKestado;
        this.INVCPF_estaliquida = INVCPF_estaliquida;
        this.INVCPF_codusuario = INVCPF_codusuario;
        this.GENTAL_secuencia = GENTAL_secuencia;
        this.INVCPF_claveacceso = INVCPF_claveacceso;
        this.INVCPF_fechaautoriza = INVCPF_fechaautoriza;
        this.INVCPF_autorizacion = INVCPF_autorizacion;
    }

    public InvtCabplandes(String GENCIA_codigo, String GENOFI_codigo, int INVCPF_numguia, Date INVCPF_fecha, String INVCAM_codigo, String CXCRUT_codrutadis, String INVCPF_motivo, BigDecimal INVCPF_totalpeso, BigDecimal INVCPF_totalvolum, int INVCPF_totaldocum, String INVCPF_claveacceso, Date INVCPF_fechaautoriza, String INVCPF_autorizacion) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.INVCPF_numguia = INVCPF_numguia;
        this.INVCPF_fecha = INVCPF_fecha;
        this.INVCAM_codigo = INVCAM_codigo;
        this.CXCRUT_codrutadis = CXCRUT_codrutadis;
        this.INVCPF_motivo = INVCPF_motivo;
        this.INVCPF_totalpeso = INVCPF_totalpeso;
        this.INVCPF_totalvolum = INVCPF_totalvolum;
        this.INVCPF_totaldocum = INVCPF_totaldocum;
        this.INVCPF_claveacceso = INVCPF_claveacceso;
        this.INVCPF_fechaautoriza = INVCPF_fechaautoriza;
        this.INVCPF_autorizacion = INVCPF_autorizacion;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public int getINVCPF_numguia() {
        return INVCPF_numguia;
    }

    public void setINVCPF_numguia(int INVCPF_numguia) {
        this.INVCPF_numguia = INVCPF_numguia;
    }

    public String getINVCPF_numero() {
        return INVCPF_numero;
    }

    public void setINVCPF_numero(String INVCPF_numero) {
        this.INVCPF_numero = INVCPF_numero;
    }

    public int getINVCPF_secuencia() {
        return INVCPF_secuencia;
    }

    public void setINVCPF_secuencia(int INVCPF_secuencia) {
        this.INVCPF_secuencia = INVCPF_secuencia;
    }

    public Date getINVCPF_fecha() {
        return INVCPF_fecha;
    }

    public void setINVCPF_fecha(Date INVCPF_fecha) {
        this.INVCPF_fecha = INVCPF_fecha;
    }

    public String getINVCAM_codigo() {
        return INVCAM_codigo;
    }

    public void setINVCAM_codigo(String INVCAM_codigo) {
        this.INVCAM_codigo = INVCAM_codigo;
    }

    public String getCXCRUT_codrutadis() {
        return CXCRUT_codrutadis;
    }

    public void setCXCRUT_codrutadis(String CXCRUT_codrutadis) {
        this.CXCRUT_codrutadis = CXCRUT_codrutadis;
    }

    public String getINVCPF_motivo() {
        return INVCPF_motivo;
    }

    public void setINVCPF_motivo(String INVCPF_motivo) {
        this.INVCPF_motivo = INVCPF_motivo;
    }

    public BigDecimal getINVCPF_totalpeso() {
        return INVCPF_totalpeso;
    }

    public void setINVCPF_totalpeso(BigDecimal INVCPF_totalpeso) {
        this.INVCPF_totalpeso = INVCPF_totalpeso;
    }

    public BigDecimal getINVCPF_totalvolum() {
        return INVCPF_totalvolum;
    }

    public void setINVCPF_totalvolum(BigDecimal INVCPF_totalvolum) {
        this.INVCPF_totalvolum = INVCPF_totalvolum;
    }

    public int getINVCPF_totaldocum() {
        return INVCPF_totaldocum;
    }

    public void setINVCPF_totaldocum(int INVCPF_totaldocum) {
        this.INVCPF_totaldocum = INVCPF_totaldocum;
    }

    public String getINVCPF_estdespach() {
        return INVCPF_estdespach;
    }

    public void setINVCPF_estdespach(String INVCPF_estdespach) {
        this.INVCPF_estdespach = INVCPF_estdespach;
    }

    public Date getINVCPF_fechadespa() {
        return INVCPF_fechadespa;
    }

    public void setINVCPF_fechadespa(Date INVCPF_fechadespa) {
        this.INVCPF_fechadespa = INVCPF_fechadespa;
    }

    public Date getINVCPF_fechaliqui() {
        return INVCPF_fechaliqui;
    }

    public void setINVCPF_fechaliqui(Date INVCPF_fechaliqui) {
        this.INVCPF_fechaliqui = INVCPF_fechaliqui;
    }

    public Date getINVCPF_fecharetor() {
        return INVCPF_fecharetor;
    }

    public void setINVCPF_fecharetor(Date INVCPF_fecharetor) {
        this.INVCPF_fecharetor = INVCPF_fecharetor;
    }

    public Date getINVCPF_fechaplani() {
        return INVCPF_fechaplani;
    }

    public void setINVCPF_fechaplani(Date INVCPF_fechaplani) {
        this.INVCPF_fechaplani = INVCPF_fechaplani;
    }

    public Date getINVCPF_fechaegres() {
        return INVCPF_fechaegres;
    }

    public void setINVCPF_fechaegres(Date INVCPF_fechaegres) {
        this.INVCPF_fechaegres = INVCPF_fechaegres;
    }

    public Date getINVCPF_fechafactu() {
        return INVCPF_fechafactu;
    }

    public void setINVCPF_fechafactu(Date INVCPF_fechafactu) {
        this.INVCPF_fechafactu = INVCPF_fechafactu;
    }

    public Date getINVCPF_fecharecha() {
        return INVCPF_fecharecha;
    }

    public void setINVCPF_fecharecha(Date INVCPF_fecharecha) {
        this.INVCPF_fecharecha = INVCPF_fecharecha;
    }

    public String getINVCPF_estelimina() {
        return INVCPF_estelimina;
    }

    public void setINVCPF_estelimina(String INVCPF_estelimina) {
        this.INVCPF_estelimina = INVCPF_estelimina;
    }

    public String getINVCPF_estuso() {
        return INVCPF_estuso;
    }

    public void setINVCPF_estuso(String INVCPF_estuso) {
        this.INVCPF_estuso = INVCPF_estuso;
    }

    public String getINVCPF_cajero() {
        return INVCPF_cajero;
    }

    public void setINVCPF_cajero(String INVCPF_cajero) {
        this.INVCPF_cajero = INVCPF_cajero;
    }

    public int getINVCPF_numguiarem() {
        return INVCPF_numguiarem;
    }

    public void setINVCPF_numguiarem(int INVCPF_numguiarem) {
        this.INVCPF_numguiarem = INVCPF_numguiarem;
    }

    public String getFACDGA_numrecicaj() {
        return FACDGA_numrecicaj;
    }

    public void setFACDGA_numrecicaj(String FACDGA_numrecicaj) {
        this.FACDGA_numrecicaj = FACDGA_numrecicaj;
    }

    public String getFACREC_codigo() {
        return FACREC_codigo;
    }

    public void setFACREC_codigo(String FACREC_codigo) {
        this.FACREC_codigo = FACREC_codigo;
    }

    public String getINVCPF_ESTLIQPOR() {
        return INVCPF_ESTLIQPOR;
    }

    public void setINVCPF_ESTLIQPOR(String INVCPF_ESTLIQPOR) {
        this.INVCPF_ESTLIQPOR = INVCPF_ESTLIQPOR;
    }

    public BigDecimal getINVCPF_TOTPLANIF() {
        return INVCPF_TOTPLANIF;
    }

    public void setINVCPF_TOTPLANIF(BigDecimal INVCPF_TOTPLANIF) {
        this.INVCPF_TOTPLANIF = INVCPF_TOTPLANIF;
    }

    public String getINVCPF_TIPLIQCAM() {
        return INVCPF_TIPLIQCAM;
    }

    public void setINVCPF_TIPLIQCAM(String INVCPF_TIPLIQCAM) {
        this.INVCPF_TIPLIQCAM = INVCPF_TIPLIQCAM;
    }

    public BigDecimal getINVCPF_kilometraje() {
        return INVCPF_kilometraje;
    }

    public void setINVCPF_kilometraje(BigDecimal INVCPF_kilometraje) {
        this.INVCPF_kilometraje = INVCPF_kilometraje;
    }

    public BigDecimal getINVCPF_kiometrajef() {
        return INVCPF_kiometrajef;
    }

    public void setINVCPF_kiometrajef(BigDecimal INVCPF_kiometrajef) {
        this.INVCPF_kiometrajef = INVCPF_kiometrajef;
    }

    public String getINVCPF_tiporesp() {
        return INVCPF_tiporesp;
    }

    public void setINVCPF_tiporesp(String INVCPF_tiporesp) {
        this.INVCPF_tiporesp = INVCPF_tiporesp;
    }

    public String getINVCPF_tipoguia() {
        return INVCPF_tipoguia;
    }

    public void setINVCPF_tipoguia(String INVCPF_tipoguia) {
        this.INVCPF_tipoguia = INVCPF_tipoguia;
    }

    public String getINVCPF_estaproces() {
        return INVCPF_estaproces;
    }

    public void setINVCPF_estaproces(String INVCPF_estaproces) {
        this.INVCPF_estaproces = INVCPF_estaproces;
    }

    public int getRTKMOB_id() {
        return RTKMOB_id;
    }

    public void setRTKMOB_id(int RTKMOB_id) {
        this.RTKMOB_id = RTKMOB_id;
    }

    public int getRTKMOB_sec() {
        return RTKMOB_sec;
    }

    public void setRTKMOB_sec(int RTKMOB_sec) {
        this.RTKMOB_sec = RTKMOB_sec;
    }

    public String getINVCPF_RTKestado() {
        return INVCPF_RTKestado;
    }

    public void setINVCPF_RTKestado(String INVCPF_RTKestado) {
        this.INVCPF_RTKestado = INVCPF_RTKestado;
    }

    public String getINVCPF_estaliquida() {
        return INVCPF_estaliquida;
    }

    public void setINVCPF_estaliquida(String INVCPF_estaliquida) {
        this.INVCPF_estaliquida = INVCPF_estaliquida;
    }

    public String getINVCPF_codusuario() {
        return INVCPF_codusuario;
    }

    public void setINVCPF_codusuario(String INVCPF_codusuario) {
        this.INVCPF_codusuario = INVCPF_codusuario;
    }

    public int getGENTAL_secuencia() {
        return GENTAL_secuencia;
    }

    public void setGENTAL_secuencia(int GENTAL_secuencia) {
        this.GENTAL_secuencia = GENTAL_secuencia;
    }

    public String getINVCPF_claveacceso() {
        return INVCPF_claveacceso;
    }

    public void setINVCPF_claveacceso(String INVCPF_claveacceso) {
        this.INVCPF_claveacceso = INVCPF_claveacceso;
    }

    public Date getINVCPF_fechaautoriza() {
        return INVCPF_fechaautoriza;
    }

    public void setINVCPF_fechaautoriza(Date INVCPF_fechaautoriza) {
        this.INVCPF_fechaautoriza = INVCPF_fechaautoriza;
    }

    public String getINVCPF_autorizacion() {
        return INVCPF_autorizacion;
    }

    public void setINVCPF_autorizacion(String INVCPF_autorizacion) {
        this.INVCPF_autorizacion = INVCPF_autorizacion;
    }

    public String getXmlText() {
        return xmlText;
    }

    public void setXmlText(String xmlText) {
        this.xmlText = xmlText;
    }

    public GuiaRemisionXml getGrxml() {
        return grxml;
    }

    public void setGrxml(GuiaRemisionXml grxml) {
        this.grxml = grxml;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }

    public String getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(String estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public String getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(String idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getEstadoAutorizado() {
        return estadoAutorizado;
    }

    public void setEstadoAutorizado(String estadoAutorizado) {
        this.estadoAutorizado = estadoAutorizado;
    }
    
}
