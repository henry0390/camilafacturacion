/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.logica;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class DatosEmail implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String to;
    private String subject;
    private String  content;
    private String  bcc;
    private String cc;
    private String ruta;
    private String archivo;

    public DatosEmail() {
    }

    public DatosEmail(String to, String subject, String content, String bcc, String cc, String ruta, String archivo) {
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.bcc = bcc;
        this.cc = cc;
        this.ruta = ruta;
        this.archivo = archivo;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }
    
    
}
