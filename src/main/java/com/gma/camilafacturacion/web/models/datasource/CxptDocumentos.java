/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class CxptDocumentos implements Serializable {

    private static final long serialVersionUID = 1L;

    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String CXPDOC_codigo;
    private String CXPTDO_codigo;
    private String CXPDOC_proveedor;
    private String CXPDOC_moneda;
    private String CXPDOC_referencia;
    private String CXPDOC_refoficinaprov;
    private String CXPDOC_refserieprov;
    private String CXPDOC_refsecprov;
    private String CXPDOC_refimprenta;
    private String CXPDOC_refautorizacionprov;
    private Date CXPDOC_fechaingreso;
    private Date CXPDOC_fechaemision;
    private Date CXPDOC_fechavalidez;
    private Date CXPDOC_fechavencimiento;
    private BigDecimal CXPDOC_valorcambio;
    private BigDecimal CXPDOC_valorsubtotal;
    private BigDecimal CXPDOC_valoriva;
    private BigDecimal CXPDOC_valorretencion;
    private String CXPDOC_descripcion;
    private String CXPDOC_usuario;
    private BigDecimal CXPDOC_valorimpuestos;
    private String CXPDOC_codsustributa;
    private String CXPDOC_codtipodocuSRI;
    private Date CXPDOC_fechafactura;
    private String CXPDOC_rucci;
    private String CXPDOC_tipoprov;
    private String CXPDOC_frmPago;
    private int CXPDOC_secuencompra;
    private String CXPDOC_estado;
    private String xmlText;

    public CxptDocumentos() {
    }

    public CxptDocumentos(String GENCIA_codigo, String GENOFI_codigo, String CXPDOC_codigo, String CXPTDO_codigo, String CXPDOC_proveedor, String CXPDOC_moneda, String CXPDOC_referencia, String CXPDOC_refoficinaprov, String CXPDOC_refserieprov, String CXPDOC_refsecprov, String CXPDOC_refimprenta, String CXPDOC_refautorizacionprov, Date CXPDOC_fechaingreso, Date CXPDOC_fechaemision, Date CXPDOC_fechavalidez, Date CXPDOC_fechavencimiento, BigDecimal CXPDOC_valorcambio, BigDecimal CXPDOC_valorsubtotal, BigDecimal CXPDOC_valoriva, BigDecimal CXPDOC_valorretencion, String CXPDOC_descripcion, String CXPDOC_usuario, BigDecimal CXPDOC_valorimpuestos, String CXPDOC_codsustributa, String CXPDOC_codtipodocuSRI, Date CXPDOC_fechafactura, String CXPDOC_rucci, String CXPDOC_tipoprov, String CXPDOC_frmPago, int CXPDOC_secuencompra, String CXPDOC_estado) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.CXPDOC_codigo = CXPDOC_codigo;
        this.CXPTDO_codigo = CXPTDO_codigo;
        this.CXPDOC_proveedor = CXPDOC_proveedor;
        this.CXPDOC_moneda = CXPDOC_moneda;
        this.CXPDOC_referencia = CXPDOC_referencia;
        this.CXPDOC_refoficinaprov = CXPDOC_refoficinaprov;
        this.CXPDOC_refserieprov = CXPDOC_refserieprov;
        this.CXPDOC_refsecprov = CXPDOC_refsecprov;
        this.CXPDOC_refimprenta = CXPDOC_refimprenta;
        this.CXPDOC_refautorizacionprov = CXPDOC_refautorizacionprov;
        this.CXPDOC_fechaingreso = CXPDOC_fechaingreso;
        this.CXPDOC_fechaemision = CXPDOC_fechaemision;
        this.CXPDOC_fechavalidez = CXPDOC_fechavalidez;
        this.CXPDOC_fechavencimiento = CXPDOC_fechavencimiento;
        this.CXPDOC_valorcambio = CXPDOC_valorcambio;
        this.CXPDOC_valorsubtotal = CXPDOC_valorsubtotal;
        this.CXPDOC_valoriva = CXPDOC_valoriva;
        this.CXPDOC_valorretencion = CXPDOC_valorretencion;
        this.CXPDOC_descripcion = CXPDOC_descripcion;
        this.CXPDOC_usuario = CXPDOC_usuario;
        this.CXPDOC_valorimpuestos = CXPDOC_valorimpuestos;
        this.CXPDOC_codsustributa = CXPDOC_codsustributa;
        this.CXPDOC_codtipodocuSRI = CXPDOC_codtipodocuSRI;
        this.CXPDOC_fechafactura = CXPDOC_fechafactura;
        this.CXPDOC_rucci = CXPDOC_rucci;
        this.CXPDOC_tipoprov = CXPDOC_tipoprov;
        this.CXPDOC_frmPago = CXPDOC_frmPago;
        this.CXPDOC_secuencompra = CXPDOC_secuencompra;
        this.CXPDOC_estado = CXPDOC_estado;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getCXPDOC_codigo() {
        return CXPDOC_codigo;
    }

    public void setCXPDOC_codigo(String CXPDOC_codigo) {
        this.CXPDOC_codigo = CXPDOC_codigo;
    }

    public String getCXPTDO_codigo() {
        return CXPTDO_codigo;
    }

    public void setCXPTDO_codigo(String CXPTDO_codigo) {
        this.CXPTDO_codigo = CXPTDO_codigo;
    }

    public String getCXPDOC_proveedor() {
        return CXPDOC_proveedor;
    }

    public void setCXPDOC_proveedor(String CXPDOC_proveedor) {
        this.CXPDOC_proveedor = CXPDOC_proveedor;
    }

    public String getCXPDOC_moneda() {
        return CXPDOC_moneda;
    }

    public void setCXPDOC_moneda(String CXPDOC_moneda) {
        this.CXPDOC_moneda = CXPDOC_moneda;
    }

    public String getCXPDOC_referencia() {
        return CXPDOC_referencia;
    }

    public void setCXPDOC_referencia(String CXPDOC_referencia) {
        this.CXPDOC_referencia = CXPDOC_referencia;
    }

    public String getCXPDOC_refoficinaprov() {
        return CXPDOC_refoficinaprov;
    }

    public void setCXPDOC_refoficinaprov(String CXPDOC_refoficinaprov) {
        this.CXPDOC_refoficinaprov = CXPDOC_refoficinaprov;
    }

    public String getCXPDOC_refserieprov() {
        return CXPDOC_refserieprov;
    }

    public void setCXPDOC_refserieprov(String CXPDOC_refserieprov) {
        this.CXPDOC_refserieprov = CXPDOC_refserieprov;
    }

    public String getCXPDOC_refsecprov() {
        return CXPDOC_refsecprov;
    }

    public void setCXPDOC_refsecprov(String CXPDOC_refsecprov) {
        this.CXPDOC_refsecprov = CXPDOC_refsecprov;
    }

    public String getCXPDOC_refimprenta() {
        return CXPDOC_refimprenta;
    }

    public void setCXPDOC_refimprenta(String CXPDOC_refimprenta) {
        this.CXPDOC_refimprenta = CXPDOC_refimprenta;
    }

    public String getCXPDOC_refautorizacionprov() {
        return CXPDOC_refautorizacionprov;
    }

    public void setCXPDOC_refautorizacionprov(String CXPDOC_refautorizacionprov) {
        this.CXPDOC_refautorizacionprov = CXPDOC_refautorizacionprov;
    }

    public Date getCXPDOC_fechaingreso() {
        return CXPDOC_fechaingreso;
    }

    public void setCXPDOC_fechaingreso(Date CXPDOC_fechaingreso) {
        this.CXPDOC_fechaingreso = CXPDOC_fechaingreso;
    }

    public Date getCXPDOC_fechaemision() {
        return CXPDOC_fechaemision;
    }

    public void setCXPDOC_fechaemision(Date CXPDOC_fechaemision) {
        this.CXPDOC_fechaemision = CXPDOC_fechaemision;
    }

    public Date getCXPDOC_fechavalidez() {
        return CXPDOC_fechavalidez;
    }

    public void setCXPDOC_fechavalidez(Date CXPDOC_fechavalidez) {
        this.CXPDOC_fechavalidez = CXPDOC_fechavalidez;
    }

    public Date getCXPDOC_fechavencimiento() {
        return CXPDOC_fechavencimiento;
    }

    public void setCXPDOC_fechavencimiento(Date CXPDOC_fechavencimiento) {
        this.CXPDOC_fechavencimiento = CXPDOC_fechavencimiento;
    }

    public BigDecimal getCXPDOC_valorcambio() {
        return CXPDOC_valorcambio;
    }

    public void setCXPDOC_valorcambio(BigDecimal CXPDOC_valorcambio) {
        this.CXPDOC_valorcambio = CXPDOC_valorcambio;
    }

    public BigDecimal getCXPDOC_valorsubtotal() {
        return CXPDOC_valorsubtotal;
    }

    public void setCXPDOC_valorsubtotal(BigDecimal CXPDOC_valorsubtotal) {
        this.CXPDOC_valorsubtotal = CXPDOC_valorsubtotal;
    }

    public BigDecimal getCXPDOC_valoriva() {
        return CXPDOC_valoriva;
    }

    public void setCXPDOC_valoriva(BigDecimal CXPDOC_valoriva) {
        this.CXPDOC_valoriva = CXPDOC_valoriva;
    }

    public BigDecimal getCXPDOC_valorretencion() {
        return CXPDOC_valorretencion;
    }

    public void setCXPDOC_valorretencion(BigDecimal CXPDOC_valorretencion) {
        this.CXPDOC_valorretencion = CXPDOC_valorretencion;
    }

    public String getCXPDOC_descripcion() {
        return CXPDOC_descripcion;
    }

    public void setCXPDOC_descripcion(String CXPDOC_descripcion) {
        this.CXPDOC_descripcion = CXPDOC_descripcion;
    }

    public String getCXPDOC_usuario() {
        return CXPDOC_usuario;
    }

    public void setCXPDOC_usuario(String CXPDOC_usuario) {
        this.CXPDOC_usuario = CXPDOC_usuario;
    }

    public BigDecimal getCXPDOC_valorimpuestos() {
        return CXPDOC_valorimpuestos;
    }

    public void setCXPDOC_valorimpuestos(BigDecimal CXPDOC_valorimpuestos) {
        this.CXPDOC_valorimpuestos = CXPDOC_valorimpuestos;
    }

    public String getCXPDOC_codsustributa() {
        return CXPDOC_codsustributa;
    }

    public void setCXPDOC_codsustributa(String CXPDOC_codsustributa) {
        this.CXPDOC_codsustributa = CXPDOC_codsustributa;
    }

    public String getCXPDOC_codtipodocuSRI() {
        return CXPDOC_codtipodocuSRI;
    }

    public void setCXPDOC_codtipodocuSRI(String CXPDOC_codtipodocuSRI) {
        this.CXPDOC_codtipodocuSRI = CXPDOC_codtipodocuSRI;
    }

    public Date getCXPDOC_fechafactura() {
        return CXPDOC_fechafactura;
    }

    public void setCXPDOC_fechafactura(Date CXPDOC_fechafactura) {
        this.CXPDOC_fechafactura = CXPDOC_fechafactura;
    }

    public String getCXPDOC_rucci() {
        return CXPDOC_rucci;
    }

    public void setCXPDOC_rucci(String CXPDOC_rucci) {
        this.CXPDOC_rucci = CXPDOC_rucci;
    }

    public String getCXPDOC_tipoprov() {
        return CXPDOC_tipoprov;
    }

    public void setCXPDOC_tipoprov(String CXPDOC_tipoprov) {
        this.CXPDOC_tipoprov = CXPDOC_tipoprov;
    }

    public String getCXPDOC_frmPago() {
        return CXPDOC_frmPago;
    }

    public void setCXPDOC_frmPago(String CXPDOC_frmPago) {
        this.CXPDOC_frmPago = CXPDOC_frmPago;
    }

    public int getCXPDOC_secuencompra() {
        return CXPDOC_secuencompra;
    }

    public void setCXPDOC_secuencompra(int CXPDOC_secuencompra) {
        this.CXPDOC_secuencompra = CXPDOC_secuencompra;
    }

    public String getCXPDOC_estado() {
        return CXPDOC_estado;
    }

    public void setCXPDOC_estado(String CXPDOC_estado) {
        this.CXPDOC_estado = CXPDOC_estado;
    }

    public String getXmlText() {
        return xmlText;
    }

    public void setXmlText(String xmlText) {
        this.xmlText = xmlText;
    }
    
    
}
