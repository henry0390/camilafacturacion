/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.consulta;

import com.gma.camilafacturacion.web.models.datasource.FacturaXml;
import com.gma.camilafacturacion.web.models.datasource.GuiaRemisionXml;
import com.gma.camilafacturacion.web.models.datasource.NotaCreditoXml;
import com.gma.camilafacturacion.web.models.datasource.RetencionXml;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.xml.sax.InputSource;


/**
 *
 * @author Acer
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "estado",
    "numeroAutorizacion",
    "fechaAutorizacion",
    "ambiente",
    "comprobante",
    "mensajes"
})
@XmlRootElement(name = "autorizacion")
public class DocumentoAutorizado {		
    @XmlElement(required = true)
    protected String estado;
    @XmlElement(required = true)
    protected String numeroAutorizacion;
    protected String fechaAutorizacion;
    protected String ambiente;
    protected String comprobante;
    protected DocumentoAutorizado.Mensajes mensajes;    
    @XmlTransient
    protected String rucEmpresa;
    @XmlTransient
    protected String claveAcceso;
    @XmlTransient
    protected Long tipoDocumento;
    @XmlTransient
    protected String secuencia;
    @XmlTransient
    protected String numeroDocumento;
    @XmlTransient
    protected String total="0.00";
    @XmlTransient
    protected FacturaXml factura;
    @XmlTransient
    protected RetencionXml retencion;
    @XmlTransient
    protected NotaCreditoXml notaCredito;
    @XmlTransient
    protected GuiaRemisionXml guiaRemision;
        
    
    public void cargarComprobante() throws JAXBException{
    	try {
    		JAXBContext context=null;
            Unmarshaller unmarshaller = null;
            if(this.comprobante.substring(0, 100).contains("<factura id=\"comprobante\"")) {
            	context=JAXBContext.newInstance(FacturaXml.class);
            	unmarshaller = context.createUnmarshaller();
            	this.setFactura((FacturaXml) unmarshaller.unmarshal(new InputSource(new StringReader(this.comprobante))));
            	this.getFactura().cargarDatosAdicionales();
            	this.tipoDocumento= 1L;
            	this.rucEmpresa = this.getFactura().getInfoTributaria().getRuc();
                this.claveAcceso = this.getFactura().getInfoTributaria().getClaveAcceso();
                this.secuencia = this.getFactura().getSecuencia();
                this.numeroDocumento = this.getFactura().getNumdoc();
                this.total = this.getFactura().getInfoFactura().getImporteTotal()+"";
            }else if(this.comprobante.substring(0, 100).contains("<comprobanteRetencion id=\"comprobante\"")) {
            	context=JAXBContext.newInstance(RetencionXml.class);   
                unmarshaller = context.createUnmarshaller();
                this.setRetencion((RetencionXml) unmarshaller.unmarshal(new InputSource(new StringReader(this.comprobante))));
                this.getRetencion().cargarDatosAdicionales();
        		this.tipoDocumento= 2L;
            	this.rucEmpresa = this.getRetencion().getInfoTributaria().getRuc();
                this.claveAcceso = this.getRetencion().getInfoTributaria().getClaveAcceso();
                this.secuencia = this.getRetencion().getSecuencia();
                this.numeroDocumento = this.getRetencion().getNumdoc();
                this.total = "";
            }else if(this.comprobante.substring(0, 100).contains("<notaCredito id=\"comprobante\"")) {
            	context=JAXBContext.newInstance(NotaCreditoXml.class);        
                unmarshaller = context.createUnmarshaller();
                this.setNotaCredito((NotaCreditoXml) unmarshaller.unmarshal(new InputSource(new StringReader(this.comprobante))));
                this.getNotaCredito().cargarDatosAdicionales();
            	this.tipoDocumento= 3L;
            	this.rucEmpresa = this.getNotaCredito().getInfoTributaria().getRuc();
                this.claveAcceso = this.getNotaCredito().getInfoTributaria().getClaveAcceso();
                this.secuencia = this.getNotaCredito().getSecuencia();
                this.numeroDocumento = this.getNotaCredito().getNumdoc();
                this.total = this.getNotaCredito().getInfoNotaCredito().getValorModificacion()+"";
            }else if(this.comprobante.substring(0, 100).contains("<guiaRemision id=\"comprobante\"")) {
            	context=JAXBContext.newInstance(GuiaRemisionXml.class);        
                unmarshaller = context.createUnmarshaller();
                this.setGuiaRemision((GuiaRemisionXml) unmarshaller.unmarshal(new InputSource(new StringReader(this.comprobante))));
                this.getGuiaRemision().cargarDatosAdicionales();
            	this.tipoDocumento= 4L;
            	this.rucEmpresa = this.getGuiaRemision().getInfoTributaria().getRuc();
                this.claveAcceso = this.getGuiaRemision().getInfoTributaria().getClaveAcceso();
                this.secuencia = this.getGuiaRemision().getSecuencia();
                this.numeroDocumento = this.getGuiaRemision().getNumdoc();
                this.total = "";
            }
    	} catch (Exception e) {
    		e.printStackTrace();
            Logger.getLogger(DocumentoAutorizado.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /*public void cargarFecha() throws ParseException{
        //this.setFechaAutorizacion(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH).parse(this.fechaAutorizacion)));
        //this.setFechaAutorizacion(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ssZ", Locale.ENGLISH).parse(this.fechaAutorizacion.substring(0, this.fechaAutorizacion.length()-5)+this.fechaAutorizacion.substring(this.fechaAutorizacion.length()-5).replaceAll(":", ""))));
    }*/
    
    public String getEstado() {
        return estado;
    }

    public void setEstado(String value) {
        this.estado = value;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String value) {
        this.numeroAutorizacion = value;
    }

    public String getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(String value) {
        this.fechaAutorizacion = value;
    }

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String value) {
        this.comprobante = value;
    }

	public String getRucEmpresa() {
		return rucEmpresa;
	}

	public void setRucEmpresa(String rucEmpresa) {
		this.rucEmpresa = rucEmpresa;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public DocumentoAutorizado.Mensajes getMensajes() {
		return mensajes;
	}

	public void setMensajes(DocumentoAutorizado.Mensajes mensajes) {
		this.mensajes = mensajes;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public Long getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	public FacturaXml getFactura() {
        return factura;
    }

    public void setFactura(FacturaXml factura) {
        this.factura = factura;
    }

    public RetencionXml getRetencion() {
        return retencion;
    }

    public void setRetencion(RetencionXml retencion) {
        this.retencion = retencion;
    }


	public NotaCreditoXml getNotaCredito() {
		return notaCredito;
	}

	public void setNotaCredito(NotaCreditoXml notaCredito) {
		this.notaCredito = notaCredito;
	}

	public GuiaRemisionXml getGuiaRemision() {
		return guiaRemision;
	}

	public void setGuiaRemision(GuiaRemisionXml guiaRemision) {
		this.guiaRemision = guiaRemision;
	}


	@XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mensaje"
    })
    public static class Mensajes {

        @XmlElement(namespace = "http://ec.gob.sri.ws.autorizacion")
        protected List<Mensaje> mensaje;
        
        public List<Mensaje> getMensaje() {
            if (mensaje == null) {
                mensaje = new ArrayList<Mensaje>();
            }
            return this.mensaje;
        }

    }
    
}
