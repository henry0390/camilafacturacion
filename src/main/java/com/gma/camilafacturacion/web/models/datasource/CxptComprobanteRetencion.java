/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class CxptComprobanteRetencion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String CXPRDO_secuencia;
    private int CXPRDO_linea;
    private Date CXPPRO_fechadocum;
    private String CXPPRO_codigo;
    private String CXPDOC_tipodocumen;
    private String CXPDOC_codigo;
    private String CXPDOC_tipodoc;
    private String CXPRET_codigo;
    private String CXPRET_nombreret;
    private String CXPRDO_tiporetencion;
    private String CXPRDO_moneda;
    private BigDecimal CXPRDO_porcentaje;
    private BigDecimal CXPRDO_porcentajedist;
    private BigDecimal CXPRDO_valor;
    private BigDecimal CXPRDO_baseimponible;
    private int CXPRDO_numretencion;
    private String CXPRDO_codestablecim;
    private String CXPRDO_codptoemision;
    private String CXPRDO_codautorizacion;
    private Date CXPRDO_fechavalidez;
    private String CXPRDO_autorizacion;
    private Date CXPRDO_fechaautoriza;
    private String CXPRDO_claveacceso;
    private String secuencia;
    private BigDecimal valor;
    private String CXPPRO_razonsocia;
    private String xmlText;
    private String estadoEnvio;
    private String estadoAutorizado;

    public CxptComprobanteRetencion() {
    }

    public CxptComprobanteRetencion(String GENCIA_codigo, String GENOFI_codigo, String CXPRDO_secuencia, int CXPRDO_linea, Date CXPPRO_fechadocum, String CXPPRO_codigo, String CXPDOC_tipodocumen, String CXPDOC_codigo, String CXPDOC_tipodoc, String CXPRET_codigo, String CXPRET_nombreret, String CXPRDO_tiporetencion, String CXPRDO_moneda, BigDecimal CXPRDO_porcentaje, BigDecimal CXPRDO_porcentajedist, BigDecimal CXPRDO_valor, BigDecimal CXPRDO_baseimponible, int CXPRDO_numretencion, String CXPRDO_codestablecim, String CXPRDO_codptoemision, String CXPRDO_codautorizacion, Date CXPRDO_fechavalidez, String CXPRDO_autorizacion, Date CXPRDO_fechaautoriza, String CXPRDO_claveacceso) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.CXPRDO_secuencia = CXPRDO_secuencia;
        this.CXPRDO_linea = CXPRDO_linea;
        this.CXPPRO_fechadocum = CXPPRO_fechadocum;
        this.CXPPRO_codigo = CXPPRO_codigo;
        this.CXPDOC_tipodocumen = CXPDOC_tipodocumen;
        this.CXPDOC_codigo = CXPDOC_codigo;
        this.CXPDOC_tipodoc = CXPDOC_tipodoc;
        this.CXPRET_codigo = CXPRET_codigo;
        this.CXPRET_nombreret = CXPRET_nombreret;
        this.CXPRDO_tiporetencion = CXPRDO_tiporetencion;
        this.CXPRDO_moneda = CXPRDO_moneda;
        this.CXPRDO_porcentaje = CXPRDO_porcentaje;
        this.CXPRDO_porcentajedist = CXPRDO_porcentajedist;
        this.CXPRDO_valor = CXPRDO_valor;
        this.CXPRDO_baseimponible = CXPRDO_baseimponible;
        this.CXPRDO_numretencion = CXPRDO_numretencion;
        this.CXPRDO_codestablecim = CXPRDO_codestablecim;
        this.CXPRDO_codptoemision = CXPRDO_codptoemision;
        this.CXPRDO_codautorizacion = CXPRDO_codautorizacion;
        this.CXPRDO_fechavalidez = CXPRDO_fechavalidez;
        this.CXPRDO_autorizacion = CXPRDO_autorizacion;
        this.CXPRDO_fechaautoriza = CXPRDO_fechaautoriza;
        this.CXPRDO_claveacceso = CXPRDO_claveacceso;
    }    

    public CxptComprobanteRetencion(String GENCIA_codigo, String GENOFI_codigo, String CXPPRO_codigo, String CXPDOC_codigo, String secuencia, BigDecimal valor, String CXPPRO_razonsocia, Date CXPPRO_fechadocum) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.CXPPRO_codigo = CXPPRO_codigo;
        this.CXPDOC_codigo = CXPDOC_codigo;
        this.secuencia = secuencia;
        this.valor = valor;
        this.CXPPRO_razonsocia = CXPPRO_razonsocia;
        this.CXPPRO_fechadocum=CXPPRO_fechadocum;
    }
    

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getCXPRDO_secuencia() {
        return CXPRDO_secuencia;
    }

    public void setCXPRDO_secuencia(String CXPRDO_secuencia) {
        this.CXPRDO_secuencia = CXPRDO_secuencia;
    }

    public int getCXPRDO_linea() {
        return CXPRDO_linea;
    }

    public void setCXPRDO_linea(int CXPRDO_linea) {
        this.CXPRDO_linea = CXPRDO_linea;
    }

    public Date getCXPPRO_fechadocum() {
        return CXPPRO_fechadocum;
    }

    public void setCXPPRO_fechadocum(Date CXPPRO_fechadocum) {
        this.CXPPRO_fechadocum = CXPPRO_fechadocum;
    }

    public String getCXPPRO_codigo() {
        return CXPPRO_codigo;
    }

    public void setCXPPRO_codigo(String CXPPRO_codigo) {
        this.CXPPRO_codigo = CXPPRO_codigo;
    }

    public String getCXPDOC_tipodocumen() {
        return CXPDOC_tipodocumen;
    }

    public void setCXPDOC_tipodocumen(String CXPDOC_tipodocumen) {
        this.CXPDOC_tipodocumen = CXPDOC_tipodocumen;
    }

    public String getCXPDOC_codigo() {
        return CXPDOC_codigo;
    }

    public void setCXPDOC_codigo(String CXPDOC_codigo) {
        this.CXPDOC_codigo = CXPDOC_codigo;
    }

    public String getCXPDOC_tipodoc() {
        return CXPDOC_tipodoc;
    }

    public void setCXPDOC_tipodoc(String CXPDOC_tipodoc) {
        this.CXPDOC_tipodoc = CXPDOC_tipodoc;
    }

    public String getCXPRET_codigo() {
        return CXPRET_codigo;
    }

    public void setCXPRET_codigo(String CXPRET_codigo) {
        this.CXPRET_codigo = CXPRET_codigo;
    }

    public String getCXPRET_nombreret() {
        return CXPRET_nombreret;
    }

    public void setCXPRET_nombreret(String CXPRET_nombreret) {
        this.CXPRET_nombreret = CXPRET_nombreret;
    }

    public String getCXPRDO_tiporetencion() {
        return CXPRDO_tiporetencion;
    }

    public void setCXPRDO_tiporetencion(String CXPRDO_tiporetencion) {
        this.CXPRDO_tiporetencion = CXPRDO_tiporetencion;
    }

    public String getCXPRDO_moneda() {
        return CXPRDO_moneda;
    }

    public void setCXPRDO_moneda(String CXPRDO_moneda) {
        this.CXPRDO_moneda = CXPRDO_moneda;
    }

    public BigDecimal getCXPRDO_porcentaje() {
        return CXPRDO_porcentaje;
    }

    public void setCXPRDO_porcentaje(BigDecimal CXPRDO_porcentaje) {
        this.CXPRDO_porcentaje = CXPRDO_porcentaje;
    }

    public BigDecimal getCXPRDO_porcentajedist() {
        return CXPRDO_porcentajedist;
    }

    public void setCXPRDO_porcentajedist(BigDecimal CXPRDO_porcentajedist) {
        this.CXPRDO_porcentajedist = CXPRDO_porcentajedist;
    }

    public BigDecimal getCXPRDO_valor() {
        return CXPRDO_valor;
    }

    public void setCXPRDO_valor(BigDecimal CXPRDO_valor) {
        this.CXPRDO_valor = CXPRDO_valor;
    }

    public BigDecimal getCXPRDO_baseimponible() {
        return CXPRDO_baseimponible;
    }

    public void setCXPRDO_baseimponible(BigDecimal CXPRDO_baseimponible) {
        this.CXPRDO_baseimponible = CXPRDO_baseimponible;
    }

    public int getCXPRDO_numretencion() {
        return CXPRDO_numretencion;
    }

    public void setCXPRDO_numretencion(int CXPRDO_numretencion) {
        this.CXPRDO_numretencion = CXPRDO_numretencion;
    }

    public String getCXPRDO_codestablecim() {
        return CXPRDO_codestablecim;
    }

    public void setCXPRDO_codestablecim(String CXPRDO_codestablecim) {
        this.CXPRDO_codestablecim = CXPRDO_codestablecim;
    }

    public String getCXPRDO_codptoemision() {
        return CXPRDO_codptoemision;
    }

    public void setCXPRDO_codptoemision(String CXPRDO_codptoemision) {
        this.CXPRDO_codptoemision = CXPRDO_codptoemision;
    }

    public String getCXPRDO_codautorizacion() {
        return CXPRDO_codautorizacion;
    }

    public void setCXPRDO_codautorizacion(String CXPRDO_codautorizacion) {
        this.CXPRDO_codautorizacion = CXPRDO_codautorizacion;
    }

    public Date getCXPRDO_fechavalidez() {
        return CXPRDO_fechavalidez;
    }

    public void setCXPRDO_fechavalidez(Date CXPRDO_fechavalidez) {
        this.CXPRDO_fechavalidez = CXPRDO_fechavalidez;
    }

    public String getCXPRDO_autorizacion() {
        return CXPRDO_autorizacion;
    }

    public void setCXPRDO_autorizacion(String CXPRDO_autorizacion) {
        this.CXPRDO_autorizacion = CXPRDO_autorizacion;
    }

    public Date getCXPRDO_fechaautoriza() {
        return CXPRDO_fechaautoriza;
    }

    public void setCXPRDO_fechaautoriza(Date CXPRDO_fechaautoriza) {
        this.CXPRDO_fechaautoriza = CXPRDO_fechaautoriza;
    }

    public String getCXPRDO_claveacceso() {
        return CXPRDO_claveacceso;
    }

    public void setCXPRDO_claveacceso(String CXPRDO_claveacceso) {
        this.CXPRDO_claveacceso = CXPRDO_claveacceso;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getCXPPRO_razonsocia() {
        return CXPPRO_razonsocia;
    }

    public void setCXPPRO_razonsocia(String CXPPRO_razonsocia) {
        this.CXPPRO_razonsocia = CXPPRO_razonsocia;
    }

    public String getXmlText() {
        return xmlText;
    }

    public void setXmlText(String xmlText) {
        this.xmlText = xmlText;
    }

    public String getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(String estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public String getEstadoAutorizado() {
        return estadoAutorizado;
    }

    public void setEstadoAutorizado(String estadoAutorizado) {
        this.estadoAutorizado = estadoAutorizado;
    }
        
}
