/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.logica;

import com.gma.camilafacturacion.setting.SisVar;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Acer
 */
public class SriRespuestaConsulta implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String estado;
    private String numeroAutorizacion;
    private Date fechaAutorizacion;
    private String mensaje;
    private String mensajeAdicional;
    
    private String claveAcceso;
    private String documento;
    private String numeroDocumento;
    private String total;

    public SriRespuestaConsulta() {
        this.estado="NO AUTORIZADO";
    }

    public SriRespuestaConsulta(String estado, String numeroAutorizacion, String fechaAutorizacion, String mensaje, String mensajeAdicional, String claveAcceso, String documento, String numeroDocumento, String total) throws ParseException {
        this.estado = estado;
        this.numeroAutorizacion = numeroAutorizacion;
        this.fechaAutorizacion = SisVar.tipo.equalsIgnoreCase("1")?new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH).parse(fechaAutorizacion):
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH).parse(fechaAutorizacion.substring(0, fechaAutorizacion.length()-5)+fechaAutorizacion.substring(fechaAutorizacion.length()-5).replaceAll(":", ""));
        this.mensaje = mensaje;
        this.mensajeAdicional = mensajeAdicional;
        this.claveAcceso = claveAcceso;
        this.documento = documento;
        this.numeroDocumento = numeroDocumento;
        this.total = total;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public Date getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensajeAdicional() {
        return mensajeAdicional;
    }

    public void setMensajeAdicional(String mensajeAdicional) {
        this.mensajeAdicional = mensajeAdicional;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
    
}
