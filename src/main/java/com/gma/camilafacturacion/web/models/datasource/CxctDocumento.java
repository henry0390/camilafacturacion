/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class CxctDocumento implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String GENCIA_codigo;
    private BigDecimal CXCDEU_numesecuen;
    private String GENOFI_codigo;
    private String CXCCLI_codigo;
    private Date CXCDEU_fechaemisi;
    private Date CXCDEU_fechavenci;
    private String FACVDR_codigo;
    private String CXCFPG_codigo;
    private String CXCDEU_terminopag;
    private int CXCDEU_diasplazo;
    private String GENMOD_codigo;
    private String GENTDO_codigo;
    private String CXCRUT_codigodocu;
    private String CXCDEU_numdocumen;
    private String CXCDEU_numplanif;
    private BigDecimal CXCDEU_secplanif;
    private String GENCAU_codcausadb;
    private String cxcdeu_rutdistcli;
    private String cxcdeu_rutvtacli;
    private String GENCIA_coddocrela;
    private String GENOFI_coddocrela;
    private String GENMOD_coddocrela;
    private String GENTDO_coddocrel;
    private String CXCRUT_coddocrela;
    private String CXCDEU_numdocrela;
    private String GENMON_codigo;
    private BigDecimal CXCDEU_cotiza;
    private BigDecimal CXCDEU_montomov;
    private BigDecimal CXCDEU_saldomov;
    private BigDecimal CXCDEU_debitomov;
    private BigDecimal CXCDEU_creditomov;
    private BigDecimal CXCDEU_subtotalmov;
    private BigDecimal CXCDEU_descuentomov;
    private BigDecimal CXCDEU_imptomov;
    private String CXCDEU_concepto;
    private String CXCDEU_detalle;
    private String CXCDEU_tipoban;
    private String CXCDEU_codbanco;
    private String CXCDEU_numchq;
    private String CXCDEU_cuentacte;
    private String CXCDEU_estado;
    private String CXCDEU_usuaingreg;
    private String CXCDEU_usuamodreg;
    private Date CXCDEU_fechingreg;
    private Date CXCDEU_fechmodreg;
    private String CXCDEU_estcontab;
    private String CXCDEU_codigocont;
    private String CXCDEU_secubanco;
    private String CXCDEU_ctacontable;
    private String CXCDEU_codifisco;
    private String CXCDEU_codifiscoret;
    private int CXCDEU_secfactrela;
    private String CXCDEU_motdevolu;
    private String CXCDEU_estaretencion;
    private String CXCDEU_codautoriza;
    private int GENTAL_secuencia;
    private BigDecimal CXCDEU_baseimponible;
    private BigDecimal CXCDEU_basecero;
    private BigDecimal CXCDEU_imptoservicio;
    private BigDecimal CXCDEU_imptoICE;
    private String CXCDEU_autorizacion;
    private Date CXCDEU_fechaautoriza;
    private String CXCDEU_claveacceso;
    private String CXCDEU_codimpuesto;
    private String CXCDEU_codautorizaimprenta;
    private int CXCDEU_numesecuenOld;
    private String xmlText;
    private NotaCreditoXml ncxml;
    private TbUsuario usuario;
    private String estadoEnvio;
    private String idMensaje;
    private String estadoAutorizado;

    public CxctDocumento() {
    }

    public CxctDocumento(String GENCIA_codigo, BigDecimal CXCDEU_numesecuen, String GENOFI_codigo, String CXCCLI_codigo, Date CXCDEU_fechaemisi, Date CXCDEU_fechavenci, String FACVDR_codigo, String CXCFPG_codigo, String CXCDEU_terminopag, int CXCDEU_diasplazo, String GENMOD_codigo, String GENTDO_codigo, String CXCRUT_codigodocu, String CXCDEU_numdocumen, String CXCDEU_numplanif, BigDecimal CXCDEU_secplanif, String GENCAU_codcausadb, String cxcdeu_rutdistcli, String cxcdeu_rutvtacli, String GENCIA_coddocrela, String GENOFI_coddocrela, String GENMOD_coddocrela, String GENTDO_coddocrel, String CXCRUT_coddocrela, String CXCDEU_numdocrela, String GENMON_codigo, BigDecimal CXCDEU_cotiza, BigDecimal CXCDEU_montomov, BigDecimal CXCDEU_saldomov, BigDecimal CXCDEU_debitomov, BigDecimal CXCDEU_creditomov, BigDecimal CXCDEU_subtotalmov, BigDecimal CXCDEU_descuentomov, BigDecimal CXCDEU_imptomov, String CXCDEU_concepto, String CXCDEU_detalle, String CXCDEU_tipoban, String CXCDEU_codbanco, String CXCDEU_numchq, String CXCDEU_cuentacte, String CXCDEU_estado, String CXCDEU_usuaingreg, String CXCDEU_usuamodreg, Date CXCDEU_fechingreg, Date CXCDEU_fechmodreg, String CXCDEU_estcontab, String CXCDEU_codigocont, String CXCDEU_secubanco, String CXCDEU_ctacontable, String CXCDEU_codifisco, String CXCDEU_codifiscoret, int CXCDEU_secfactrela, String CXCDEU_motdevolu, String CXCDEU_estaretencion, String CXCDEU_codautoriza, int GENTAL_secuencia, BigDecimal CXCDEU_baseimponible, BigDecimal CXCDEU_basecero, BigDecimal CXCDEU_imptoservicio, BigDecimal CXCDEU_imptoICE, String CXCDEU_autorizacion, Date CXCDEU_fechaautoriza, String CXCDEU_claveacceso, String CXCDEU_codimpuesto, String CXCDEU_codautorizaimprenta, int CXCDEU_numesecuenOld) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.CXCDEU_numesecuen = CXCDEU_numesecuen;
        this.GENOFI_codigo = GENOFI_codigo;
        this.CXCCLI_codigo = CXCCLI_codigo;
        this.CXCDEU_fechaemisi = CXCDEU_fechaemisi;
        this.CXCDEU_fechavenci = CXCDEU_fechavenci;
        this.FACVDR_codigo = FACVDR_codigo;
        this.CXCFPG_codigo = CXCFPG_codigo;
        this.CXCDEU_terminopag = CXCDEU_terminopag;
        this.CXCDEU_diasplazo = CXCDEU_diasplazo;
        this.GENMOD_codigo = GENMOD_codigo;
        this.GENTDO_codigo = GENTDO_codigo;
        this.CXCRUT_codigodocu = CXCRUT_codigodocu;
        this.CXCDEU_numdocumen = CXCDEU_numdocumen;
        this.CXCDEU_numplanif = CXCDEU_numplanif;
        this.CXCDEU_secplanif = CXCDEU_secplanif;
        this.GENCAU_codcausadb = GENCAU_codcausadb;
        this.cxcdeu_rutdistcli = cxcdeu_rutdistcli;
        this.cxcdeu_rutvtacli = cxcdeu_rutvtacli;
        this.GENCIA_coddocrela = GENCIA_coddocrela;
        this.GENOFI_coddocrela = GENOFI_coddocrela;
        this.GENMOD_coddocrela = GENMOD_coddocrela;
        this.GENTDO_coddocrel = GENTDO_coddocrel;
        this.CXCRUT_coddocrela = CXCRUT_coddocrela;
        this.CXCDEU_numdocrela = CXCDEU_numdocrela;
        this.GENMON_codigo = GENMON_codigo;
        this.CXCDEU_cotiza = CXCDEU_cotiza;
        this.CXCDEU_montomov = CXCDEU_montomov;
        this.CXCDEU_saldomov = CXCDEU_saldomov;
        this.CXCDEU_debitomov = CXCDEU_debitomov;
        this.CXCDEU_creditomov = CXCDEU_creditomov;
        this.CXCDEU_subtotalmov = CXCDEU_subtotalmov;
        this.CXCDEU_descuentomov = CXCDEU_descuentomov;
        this.CXCDEU_imptomov = CXCDEU_imptomov;
        this.CXCDEU_concepto = CXCDEU_concepto;
        this.CXCDEU_detalle = CXCDEU_detalle;
        this.CXCDEU_tipoban = CXCDEU_tipoban;
        this.CXCDEU_codbanco = CXCDEU_codbanco;
        this.CXCDEU_numchq = CXCDEU_numchq;
        this.CXCDEU_cuentacte = CXCDEU_cuentacte;
        this.CXCDEU_estado = CXCDEU_estado;
        this.CXCDEU_usuaingreg = CXCDEU_usuaingreg;
        this.CXCDEU_usuamodreg = CXCDEU_usuamodreg;
        this.CXCDEU_fechingreg = CXCDEU_fechingreg;
        this.CXCDEU_fechmodreg = CXCDEU_fechmodreg;
        this.CXCDEU_estcontab = CXCDEU_estcontab;
        this.CXCDEU_codigocont = CXCDEU_codigocont;
        this.CXCDEU_secubanco = CXCDEU_secubanco;
        this.CXCDEU_ctacontable = CXCDEU_ctacontable;
        this.CXCDEU_codifisco = CXCDEU_codifisco;
        this.CXCDEU_codifiscoret = CXCDEU_codifiscoret;
        this.CXCDEU_secfactrela = CXCDEU_secfactrela;
        this.CXCDEU_motdevolu = CXCDEU_motdevolu;
        this.CXCDEU_estaretencion = CXCDEU_estaretencion;
        this.CXCDEU_codautoriza = CXCDEU_codautoriza;
        this.GENTAL_secuencia = GENTAL_secuencia;
        this.CXCDEU_baseimponible = CXCDEU_baseimponible;
        this.CXCDEU_basecero = CXCDEU_basecero;
        this.CXCDEU_imptoservicio = CXCDEU_imptoservicio;
        this.CXCDEU_imptoICE = CXCDEU_imptoICE;
        this.CXCDEU_autorizacion = CXCDEU_autorizacion;
        this.CXCDEU_fechaautoriza = CXCDEU_fechaautoriza;
        this.CXCDEU_claveacceso = CXCDEU_claveacceso;
        this.CXCDEU_codimpuesto = CXCDEU_codimpuesto;
        this.CXCDEU_codautorizaimprenta = CXCDEU_codautorizaimprenta;
        this.CXCDEU_numesecuenOld = CXCDEU_numesecuenOld;
    }

    public CxctDocumento(String GENCIA_codigo, BigDecimal CXCDEU_numesecuen, String GENOFI_codigo, String CXCCLI_codigo, Date CXCDEU_fechaemisi, String CXCDEU_numdocumen, BigDecimal CXCDEU_imptomov, String CXCDEU_concepto) {
        this.GENCIA_codigo = GENCIA_codigo;
        this.CXCDEU_numesecuen = CXCDEU_numesecuen;
        this.GENOFI_codigo = GENOFI_codigo;
        this.CXCCLI_codigo = CXCCLI_codigo;
        this.CXCDEU_fechaemisi = CXCDEU_fechaemisi;
        this.CXCDEU_numdocumen = CXCDEU_numdocumen;
        this.CXCDEU_imptomov = CXCDEU_imptomov;
        this.CXCDEU_concepto = CXCDEU_concepto;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public BigDecimal getCXCDEU_numesecuen() {
        return CXCDEU_numesecuen;
    }

    public void setCXCDEU_numesecuen(BigDecimal CXCDEU_numesecuen) {
        this.CXCDEU_numesecuen = CXCDEU_numesecuen;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getCXCCLI_codigo() {
        return CXCCLI_codigo;
    }

    public void setCXCCLI_codigo(String CXCCLI_codigo) {
        this.CXCCLI_codigo = CXCCLI_codigo;
    }

    public Date getCXCDEU_fechaemisi() {
        return CXCDEU_fechaemisi;
    }

    public void setCXCDEU_fechaemisi(Date CXCDEU_fechaemisi) {
        this.CXCDEU_fechaemisi = CXCDEU_fechaemisi;
    }

    public Date getCXCDEU_fechavenci() {
        return CXCDEU_fechavenci;
    }

    public void setCXCDEU_fechavenci(Date CXCDEU_fechavenci) {
        this.CXCDEU_fechavenci = CXCDEU_fechavenci;
    }

    public String getFACVDR_codigo() {
        return FACVDR_codigo;
    }

    public void setFACVDR_codigo(String FACVDR_codigo) {
        this.FACVDR_codigo = FACVDR_codigo;
    }

    public String getCXCFPG_codigo() {
        return CXCFPG_codigo;
    }

    public void setCXCFPG_codigo(String CXCFPG_codigo) {
        this.CXCFPG_codigo = CXCFPG_codigo;
    }

    public String getCXCDEU_terminopag() {
        return CXCDEU_terminopag;
    }

    public void setCXCDEU_terminopag(String CXCDEU_terminopag) {
        this.CXCDEU_terminopag = CXCDEU_terminopag;
    }

    public int getCXCDEU_diasplazo() {
        return CXCDEU_diasplazo;
    }

    public void setCXCDEU_diasplazo(int CXCDEU_diasplazo) {
        this.CXCDEU_diasplazo = CXCDEU_diasplazo;
    }

    public String getGENMOD_codigo() {
        return GENMOD_codigo;
    }

    public void setGENMOD_codigo(String GENMOD_codigo) {
        this.GENMOD_codigo = GENMOD_codigo;
    }

    public String getGENTDO_codigo() {
        return GENTDO_codigo;
    }

    public void setGENTDO_codigo(String GENTDO_codigo) {
        this.GENTDO_codigo = GENTDO_codigo;
    }

    public String getCXCRUT_codigodocu() {
        return CXCRUT_codigodocu;
    }

    public void setCXCRUT_codigodocu(String CXCRUT_codigodocu) {
        this.CXCRUT_codigodocu = CXCRUT_codigodocu;
    }

    public String getCXCDEU_numdocumen() {
        return CXCDEU_numdocumen;
    }

    public void setCXCDEU_numdocumen(String CXCDEU_numdocumen) {
        this.CXCDEU_numdocumen = CXCDEU_numdocumen;
    }

    public String getCXCDEU_numplanif() {
        return CXCDEU_numplanif;
    }

    public void setCXCDEU_numplanif(String CXCDEU_numplanif) {
        this.CXCDEU_numplanif = CXCDEU_numplanif;
    }

    public BigDecimal getCXCDEU_secplanif() {
        return CXCDEU_secplanif;
    }

    public void setCXCDEU_secplanif(BigDecimal CXCDEU_secplanif) {
        this.CXCDEU_secplanif = CXCDEU_secplanif;
    }

    public String getGENCAU_codcausadb() {
        return GENCAU_codcausadb;
    }

    public void setGENCAU_codcausadb(String GENCAU_codcausadb) {
        this.GENCAU_codcausadb = GENCAU_codcausadb;
    }

    public String getCxcdeu_rutdistcli() {
        return cxcdeu_rutdistcli;
    }

    public void setCxcdeu_rutdistcli(String cxcdeu_rutdistcli) {
        this.cxcdeu_rutdistcli = cxcdeu_rutdistcli;
    }

    public String getCxcdeu_rutvtacli() {
        return cxcdeu_rutvtacli;
    }

    public void setCxcdeu_rutvtacli(String cxcdeu_rutvtacli) {
        this.cxcdeu_rutvtacli = cxcdeu_rutvtacli;
    }

    public String getGENCIA_coddocrela() {
        return GENCIA_coddocrela;
    }

    public void setGENCIA_coddocrela(String GENCIA_coddocrela) {
        this.GENCIA_coddocrela = GENCIA_coddocrela;
    }

    public String getGENOFI_coddocrela() {
        return GENOFI_coddocrela;
    }

    public void setGENOFI_coddocrela(String GENOFI_coddocrela) {
        this.GENOFI_coddocrela = GENOFI_coddocrela;
    }

    public String getGENMOD_coddocrela() {
        return GENMOD_coddocrela;
    }

    public void setGENMOD_coddocrela(String GENMOD_coddocrela) {
        this.GENMOD_coddocrela = GENMOD_coddocrela;
    }

    public String getGENTDO_coddocrel() {
        return GENTDO_coddocrel;
    }

    public void setGENTDO_coddocrel(String GENTDO_coddocrel) {
        this.GENTDO_coddocrel = GENTDO_coddocrel;
    }

    public String getCXCRUT_coddocrela() {
        return CXCRUT_coddocrela;
    }

    public void setCXCRUT_coddocrela(String CXCRUT_coddocrela) {
        this.CXCRUT_coddocrela = CXCRUT_coddocrela;
    }

    public String getCXCDEU_numdocrela() {
        return CXCDEU_numdocrela;
    }

    public void setCXCDEU_numdocrela(String CXCDEU_numdocrela) {
        this.CXCDEU_numdocrela = CXCDEU_numdocrela;
    }

    public String getGENMON_codigo() {
        return GENMON_codigo;
    }

    public void setGENMON_codigo(String GENMON_codigo) {
        this.GENMON_codigo = GENMON_codigo;
    }

    public BigDecimal getCXCDEU_cotiza() {
        return CXCDEU_cotiza;
    }

    public void setCXCDEU_cotiza(BigDecimal CXCDEU_cotiza) {
        this.CXCDEU_cotiza = CXCDEU_cotiza;
    }

    public BigDecimal getCXCDEU_montomov() {
        return CXCDEU_montomov;
    }

    public void setCXCDEU_montomov(BigDecimal CXCDEU_montomov) {
        this.CXCDEU_montomov = CXCDEU_montomov;
    }

    public BigDecimal getCXCDEU_saldomov() {
        return CXCDEU_saldomov;
    }

    public void setCXCDEU_saldomov(BigDecimal CXCDEU_saldomov) {
        this.CXCDEU_saldomov = CXCDEU_saldomov;
    }

    public BigDecimal getCXCDEU_debitomov() {
        return CXCDEU_debitomov;
    }

    public void setCXCDEU_debitomov(BigDecimal CXCDEU_debitomov) {
        this.CXCDEU_debitomov = CXCDEU_debitomov;
    }

    public BigDecimal getCXCDEU_creditomov() {
        return CXCDEU_creditomov;
    }

    public void setCXCDEU_creditomov(BigDecimal CXCDEU_creditomov) {
        this.CXCDEU_creditomov = CXCDEU_creditomov;
    }

    public BigDecimal getCXCDEU_subtotalmov() {
        return CXCDEU_subtotalmov;
    }

    public void setCXCDEU_subtotalmov(BigDecimal CXCDEU_subtotalmov) {
        this.CXCDEU_subtotalmov = CXCDEU_subtotalmov;
    }

    public BigDecimal getCXCDEU_descuentomov() {
        return CXCDEU_descuentomov;
    }

    public void setCXCDEU_descuentomov(BigDecimal CXCDEU_descuentomov) {
        this.CXCDEU_descuentomov = CXCDEU_descuentomov;
    }

    public BigDecimal getCXCDEU_imptomov() {
        return CXCDEU_imptomov;
    }

    public void setCXCDEU_imptomov(BigDecimal CXCDEU_imptomov) {
        this.CXCDEU_imptomov = CXCDEU_imptomov;
    }

    public String getCXCDEU_concepto() {
        return CXCDEU_concepto;
    }

    public void setCXCDEU_concepto(String CXCDEU_concepto) {
        this.CXCDEU_concepto = CXCDEU_concepto;
    }

    public String getCXCDEU_detalle() {
        return CXCDEU_detalle;
    }

    public void setCXCDEU_detalle(String CXCDEU_detalle) {
        this.CXCDEU_detalle = CXCDEU_detalle;
    }

    public String getCXCDEU_tipoban() {
        return CXCDEU_tipoban;
    }

    public void setCXCDEU_tipoban(String CXCDEU_tipoban) {
        this.CXCDEU_tipoban = CXCDEU_tipoban;
    }

    public String getCXCDEU_codbanco() {
        return CXCDEU_codbanco;
    }

    public void setCXCDEU_codbanco(String CXCDEU_codbanco) {
        this.CXCDEU_codbanco = CXCDEU_codbanco;
    }

    public String getCXCDEU_numchq() {
        return CXCDEU_numchq;
    }

    public void setCXCDEU_numchq(String CXCDEU_numchq) {
        this.CXCDEU_numchq = CXCDEU_numchq;
    }

    public String getCXCDEU_cuentacte() {
        return CXCDEU_cuentacte;
    }

    public void setCXCDEU_cuentacte(String CXCDEU_cuentacte) {
        this.CXCDEU_cuentacte = CXCDEU_cuentacte;
    }

    public String getCXCDEU_estado() {
        return CXCDEU_estado;
    }

    public void setCXCDEU_estado(String CXCDEU_estado) {
        this.CXCDEU_estado = CXCDEU_estado;
    }

    public String getCXCDEU_usuaingreg() {
        return CXCDEU_usuaingreg;
    }

    public void setCXCDEU_usuaingreg(String CXCDEU_usuaingreg) {
        this.CXCDEU_usuaingreg = CXCDEU_usuaingreg;
    }

    public String getCXCDEU_usuamodreg() {
        return CXCDEU_usuamodreg;
    }

    public void setCXCDEU_usuamodreg(String CXCDEU_usuamodreg) {
        this.CXCDEU_usuamodreg = CXCDEU_usuamodreg;
    }

    public Date getCXCDEU_fechingreg() {
        return CXCDEU_fechingreg;
    }

    public void setCXCDEU_fechingreg(Date CXCDEU_fechingreg) {
        this.CXCDEU_fechingreg = CXCDEU_fechingreg;
    }

    public Date getCXCDEU_fechmodreg() {
        return CXCDEU_fechmodreg;
    }

    public void setCXCDEU_fechmodreg(Date CXCDEU_fechmodreg) {
        this.CXCDEU_fechmodreg = CXCDEU_fechmodreg;
    }

    public String getCXCDEU_estcontab() {
        return CXCDEU_estcontab;
    }

    public void setCXCDEU_estcontab(String CXCDEU_estcontab) {
        this.CXCDEU_estcontab = CXCDEU_estcontab;
    }

    public String getCXCDEU_codigocont() {
        return CXCDEU_codigocont;
    }

    public void setCXCDEU_codigocont(String CXCDEU_codigocont) {
        this.CXCDEU_codigocont = CXCDEU_codigocont;
    }

    public String getCXCDEU_secubanco() {
        return CXCDEU_secubanco;
    }

    public void setCXCDEU_secubanco(String CXCDEU_secubanco) {
        this.CXCDEU_secubanco = CXCDEU_secubanco;
    }

    public String getCXCDEU_ctacontable() {
        return CXCDEU_ctacontable;
    }

    public void setCXCDEU_ctacontable(String CXCDEU_ctacontable) {
        this.CXCDEU_ctacontable = CXCDEU_ctacontable;
    }

    public String getCXCDEU_codifisco() {
        return CXCDEU_codifisco;
    }

    public void setCXCDEU_codifisco(String CXCDEU_codifisco) {
        this.CXCDEU_codifisco = CXCDEU_codifisco;
    }

    public String getCXCDEU_codifiscoret() {
        return CXCDEU_codifiscoret;
    }

    public void setCXCDEU_codifiscoret(String CXCDEU_codifiscoret) {
        this.CXCDEU_codifiscoret = CXCDEU_codifiscoret;
    }

    public int getCXCDEU_secfactrela() {
        return CXCDEU_secfactrela;
    }

    public void setCXCDEU_secfactrela(int CXCDEU_secfactrela) {
        this.CXCDEU_secfactrela = CXCDEU_secfactrela;
    }

    public String getCXCDEU_motdevolu() {
        return CXCDEU_motdevolu;
    }

    public void setCXCDEU_motdevolu(String CXCDEU_motdevolu) {
        this.CXCDEU_motdevolu = CXCDEU_motdevolu;
    }

    public String getCXCDEU_estaretencion() {
        return CXCDEU_estaretencion;
    }

    public void setCXCDEU_estaretencion(String CXCDEU_estaretencion) {
        this.CXCDEU_estaretencion = CXCDEU_estaretencion;
    }

    public String getCXCDEU_codautoriza() {
        return CXCDEU_codautoriza;
    }

    public void setCXCDEU_codautoriza(String CXCDEU_codautoriza) {
        this.CXCDEU_codautoriza = CXCDEU_codautoriza;
    }

    public int getGENTAL_secuencia() {
        return GENTAL_secuencia;
    }

    public void setGENTAL_secuencia(int GENTAL_secuencia) {
        this.GENTAL_secuencia = GENTAL_secuencia;
    }

    public BigDecimal getCXCDEU_baseimponible() {
        return CXCDEU_baseimponible;
    }

    public void setCXCDEU_baseimponible(BigDecimal CXCDEU_baseimponible) {
        this.CXCDEU_baseimponible = CXCDEU_baseimponible;
    }

    public BigDecimal getCXCDEU_basecero() {
        return CXCDEU_basecero;
    }

    public void setCXCDEU_basecero(BigDecimal CXCDEU_basecero) {
        this.CXCDEU_basecero = CXCDEU_basecero;
    }

    public BigDecimal getCXCDEU_imptoservicio() {
        return CXCDEU_imptoservicio;
    }

    public void setCXCDEU_imptoservicio(BigDecimal CXCDEU_imptoservicio) {
        this.CXCDEU_imptoservicio = CXCDEU_imptoservicio;
    }

    public BigDecimal getCXCDEU_imptoICE() {
        return CXCDEU_imptoICE;
    }

    public void setCXCDEU_imptoICE(BigDecimal CXCDEU_imptoICE) {
        this.CXCDEU_imptoICE = CXCDEU_imptoICE;
    }

    public String getCXCDEU_autorizacion() {
        return CXCDEU_autorizacion;
    }

    public void setCXCDEU_autorizacion(String CXCDEU_autorizacion) {
        this.CXCDEU_autorizacion = CXCDEU_autorizacion;
    }

    public Date getCXCDEU_fechaautoriza() {
        return CXCDEU_fechaautoriza;
    }

    public void setCXCDEU_fechaautoriza(Date CXCDEU_fechaautoriza) {
        this.CXCDEU_fechaautoriza = CXCDEU_fechaautoriza;
    }

    public String getCXCDEU_claveacceso() {
        return CXCDEU_claveacceso;
    }

    public void setCXCDEU_claveacceso(String CXCDEU_claveacceso) {
        this.CXCDEU_claveacceso = CXCDEU_claveacceso;
    }

    public String getCXCDEU_codimpuesto() {
        return CXCDEU_codimpuesto;
    }

    public void setCXCDEU_codimpuesto(String CXCDEU_codimpuesto) {
        this.CXCDEU_codimpuesto = CXCDEU_codimpuesto;
    }

    public String getCXCDEU_codautorizaimprenta() {
        return CXCDEU_codautorizaimprenta;
    }

    public void setCXCDEU_codautorizaimprenta(String CXCDEU_codautorizaimprenta) {
        this.CXCDEU_codautorizaimprenta = CXCDEU_codautorizaimprenta;
    }

    public int getCXCDEU_numesecuenOld() {
        return CXCDEU_numesecuenOld;
    }

    public void setCXCDEU_numesecuenOld(int CXCDEU_numesecuenOld) {
        this.CXCDEU_numesecuenOld = CXCDEU_numesecuenOld;
    }

    public String getXmlText() {
        return xmlText;
    }

    public void setXmlText(String xmlText) {
        this.xmlText = xmlText;
    }

    public NotaCreditoXml getNcxml() {
        return ncxml;
    }

    public void setNcxml(NotaCreditoXml ncxml) {
        this.ncxml = ncxml;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }

    public String getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(String estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public String getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(String idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getEstadoAutorizado() {
        return estadoAutorizado;
    }

    public void setEstadoAutorizado(String estadoAutorizado) {
        this.estadoAutorizado = estadoAutorizado;
    }
    
}
