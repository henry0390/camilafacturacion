/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.SisVar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Acer
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoTributaria",
    "infoCompRetencion",
    "impuestos",
    "infoAdicional"
})
@XmlRootElement(name = "comprobanteRetencion")
public class RetencionXml {
    @XmlElement(required = true)
    protected RetencionXml.InfoTributaria infoTributaria;
    @XmlElement(required = true)
    protected RetencionXml.InfoCompRetencion infoCompRetencion;
    @XmlElement(required = true)
    protected RetencionXml.Impuestos impuestos;
    @XmlElement(required = true)
    protected RetencionXml.InfoAdicional infoAdicional;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlTransient
    protected List<DatoAdicional> listDatos;
    @XmlTransient
    protected String correo;
    @XmlTransient
    protected String secuencia;
    @XmlTransient
    protected String numdoc;
    @XmlTransient
    private TbUsuario usuario;
    
    public void cargarDatosAdicionales(){
        this.listDatos = new ArrayList<>();
        this.numdoc=this.infoTributaria.getEstab()+this.infoTributaria.getPtoEmi()+this.infoTributaria.getSecuencial();
        try{
        for (InfoAdicional.CampoAdicional campoAdicional : this.infoAdicional.campoAdicional) {
            String[] cadenaValues=campoAdicional.getValue().split("\\|");
            for (int i = 0; i < cadenaValues.length; i++) {                
                String[] cadena  = cadenaValues[i].split(":");
                if(cadena.length>0 && SisVar.etiquetasInformacionAdicional.contains(cadena[0].toUpperCase())){
                    listDatos.add(new DatoAdicional(cadenaValues[i]));
                    if("EMAILCLIENTE".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.correo=cadena[1];
                    }
                    if("SECDOCUMENTO".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.secuencia=cadena[1];
                    }
                }
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Gets the value of the infoTributaria property.
     * 
     * @return
     *     possible object is
     *     {@link ComprobanteRetencion.InfoTributaria }
     *     
     */
    public RetencionXml.InfoTributaria getInfoTributaria() {
        return infoTributaria;
    }

    /**
     * Sets the value of the infoTributaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComprobanteRetencion.InfoTributaria }
     *     
     */
    public void setInfoTributaria(RetencionXml.InfoTributaria value) {
        this.infoTributaria = value;
    }

    /**
     * Gets the value of the infoCompRetencion property.
     * 
     * @return
     *     possible object is
     *     {@link ComprobanteRetencion.InfoCompRetencion }
     *     
     */
    public RetencionXml.InfoCompRetencion getInfoCompRetencion() {
        return infoCompRetencion;
    }

    /**
     * Sets the value of the infoCompRetencion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComprobanteRetencion.InfoCompRetencion }
     *     
     */
    public void setInfoCompRetencion(RetencionXml.InfoCompRetencion value) {
        this.infoCompRetencion = value;
    }

    /**
     * Gets the value of the impuestos property.
     * 
     * @return
     *     possible object is
     *     {@link ComprobanteRetencion.Impuestos }
     *     
     */
    public RetencionXml.Impuestos getImpuestos() {
        return impuestos;
    }

    /**
     * Sets the value of the impuestos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComprobanteRetencion.Impuestos }
     *     
     */
    public void setImpuestos(RetencionXml.Impuestos value) {
        this.impuestos = value;
    }

    /**
     * Gets the value of the infoAdicional property.
     * 
     * @return
     *     possible object is
     *     {@link ComprobanteRetencion.InfoAdicional }
     *     
     */
    public RetencionXml.InfoAdicional getInfoAdicional() {
        return infoAdicional;
    }

    /**
     * Sets the value of the infoAdicional property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComprobanteRetencion.InfoAdicional }
     *     
     */
    public void setInfoAdicional(RetencionXml.InfoAdicional value) {
        this.infoAdicional = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    public List<DatoAdicional> getListDatos() {
        return listDatos;
    }

    public void setListDatos(List<DatoAdicional> listDatos) {
        this.listDatos = listDatos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="impuesto" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                   &lt;element name="codigoRetencion" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                   &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="porcentajeRetener" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="valorRetenido" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="codDocSustento" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                   &lt;element name="numDocSustento" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="fechaEmisionDocSustento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "impuesto"
    })
    public static class Impuestos {

        protected List<RetencionXml.Impuestos.Impuesto> impuesto;

        /**
         * Gets the value of the impuesto property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the impuesto property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getImpuesto().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ComprobanteRetencion.Impuestos.Impuesto }
         * 
         * 
         * @return 
         */
        public List<RetencionXml.Impuestos.Impuesto> getImpuesto() {
            if (impuesto == null) {
                impuesto = new ArrayList<>();
            }
            return this.impuesto;
        }
        
        public Double valorTotal(){
            Double valor=0.00;
            for (Impuesto t : this.impuesto) {
                valor = valor +t.valorRetenido;
            }
            return valor;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *         &lt;element name="codigoRetencion" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *         &lt;element name="baseImponible" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="porcentajeRetener" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="valorRetenido" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="codDocSustento" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *         &lt;element name="numDocSustento" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="fechaEmisionDocSustento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "codigo",
            "codigoRetencion",
            "baseImponible",
            "porcentajeRetener",
            "valorRetenido",
            "codDocSustento",
            "numDocSustento",
            "fechaEmisionDocSustento"
        })
        public static class Impuesto {

            protected byte codigo;
            protected short codigoRetencion;
            protected Double baseImponible;
            protected Double porcentajeRetener;
            protected Double valorRetenido;
            protected String codDocSustento;
            protected String numDocSustento;
            @XmlElement(required = true)
            protected String fechaEmisionDocSustento;

            /**
             * Gets the value of the codigo property.
             * 
             */
            public byte getCodigo() {
                return codigo;
            }

            /**
             * Sets the value of the codigo property.
             * 
             */
            public void setCodigo(byte value) {
                this.codigo = value;
            }

            /**
             * Gets the value of the codigoRetencion property.
             * 
             */
            public short getCodigoRetencion() {
                return codigoRetencion;
            }

            /**
             * Sets the value of the codigoRetencion property.
             * 
             */
            public void setCodigoRetencion(short value) {
                this.codigoRetencion = value;
            }

            /**
             * Gets the value of the baseImponible property.
             * 
             */
            public Double getBaseImponible() {
                return baseImponible;
            }

            /**
             * Sets the value of the baseImponible property.
             * 
             */
            public void setBaseImponible(Double value) {
                this.baseImponible = value;
            }

            /**
             * Gets the value of the porcentajeRetener property.
             * 
             */
            public Double getPorcentajeRetener() {
                return porcentajeRetener;
            }

            /**
             * Sets the value of the porcentajeRetener property.
             * 
             */
            public void setPorcentajeRetener(Double value) {
                this.porcentajeRetener = value;
            }

            /**
             * Gets the value of the valorRetenido property.
             * 
             */
            public Double getValorRetenido() {
                return valorRetenido;
            }

            /**
             * Sets the value of the valorRetenido property.
             * 
             */
            public void setValorRetenido(Double value) {
                this.valorRetenido = value;
            }

            /**
             * Gets the value of the codDocSustento property.
             * 
             */
            public String getCodDocSustento() {
                return codDocSustento;
            }

            /**
             * Sets the value of the codDocSustento property.
             * 
             */
            public void setCodDocSustento(String value) {
                this.codDocSustento = value;
            }

            /**
             * Gets the value of the numDocSustento property.
             * 
             */
            public String getNumDocSustento() {
                return numDocSustento;
            }

            /**
             * Sets the value of the numDocSustento property.
             * 
             */
            public void setNumDocSustento(String value) {
                this.numDocSustento = value;
            }

            /**
             * Gets the value of the fechaEmisionDocSustento property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFechaEmisionDocSustento() {
                return fechaEmisionDocSustento;
            }

            /**
             * Sets the value of the fechaEmisionDocSustento property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFechaEmisionDocSustento(String value) {
                this.fechaEmisionDocSustento = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="campoAdicional" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "campoAdicional"
    })
    public static class InfoAdicional {

        protected List<RetencionXml.InfoAdicional.CampoAdicional> campoAdicional;

        /**
         * Gets the value of the campoAdicional property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the campoAdicional property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCampoAdicional().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ComprobanteRetencion.InfoAdicional.CampoAdicional }
         * 
         * 
         */
        public List<RetencionXml.InfoAdicional.CampoAdicional> getCampoAdicional() {
            if (campoAdicional == null) {
                campoAdicional = new ArrayList<RetencionXml.InfoAdicional.CampoAdicional>();
            }
            return this.campoAdicional;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CampoAdicional {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "nombre")
            protected String nombre;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the nombre property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNombre() {
                return nombre;
            }

            /**
             * Sets the value of the nombre property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNombre(String value) {
                this.nombre = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fechaEmision" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="dirEstablecimiento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="obligadoContabilidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipoIdentificacionSujetoRetenido" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="razonSocialSujetoRetenido" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="identificacionSujetoRetenido" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="periodoFiscal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fechaEmision",
        "dirEstablecimiento",
        "obligadoContabilidad",
        "tipoIdentificacionSujetoRetenido",
        "razonSocialSujetoRetenido",
        "identificacionSujetoRetenido",
        "periodoFiscal"
    })
    public static class InfoCompRetencion {

        @XmlElement(required = true)
        protected String fechaEmision;
        @XmlElement(required = true)
        protected String dirEstablecimiento;
        @XmlElement(required = true)
        protected String obligadoContabilidad;
        protected String tipoIdentificacionSujetoRetenido;
        @XmlElement(required = true)
        protected String razonSocialSujetoRetenido;
        protected String identificacionSujetoRetenido;
        @XmlElement(required = true)
        protected String periodoFiscal;

        /**
         * Gets the value of the fechaEmision property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFechaEmision() {
            return fechaEmision;
        }

        /**
         * Sets the value of the fechaEmision property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFechaEmision(String value) {
            this.fechaEmision = value;
        }

        /**
         * Gets the value of the dirEstablecimiento property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirEstablecimiento() {
            return dirEstablecimiento;
        }

        /**
         * Sets the value of the dirEstablecimiento property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirEstablecimiento(String value) {
            this.dirEstablecimiento = value;
        }

        /**
         * Gets the value of the obligadoContabilidad property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObligadoContabilidad() {
            return obligadoContabilidad;
        }

        /**
         * Sets the value of the obligadoContabilidad property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObligadoContabilidad(String value) {
            this.obligadoContabilidad = value;
        }

        /**
         * Gets the value of the tipoIdentificacionSujetoRetenido property.
         * 
         */
        public String getTipoIdentificacionSujetoRetenido() {
            return tipoIdentificacionSujetoRetenido;
        }

        /**
         * Sets the value of the tipoIdentificacionSujetoRetenido property.
         * 
         */
        public void setTipoIdentificacionSujetoRetenido(String value) {
            this.tipoIdentificacionSujetoRetenido = value;
        }

        /**
         * Gets the value of the razonSocialSujetoRetenido property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocialSujetoRetenido() {
            return razonSocialSujetoRetenido;
        }

        /**
         * Sets the value of the razonSocialSujetoRetenido property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocialSujetoRetenido(String value) {
            this.razonSocialSujetoRetenido = value;
        }

        /**
         * Gets the value of the identificacionSujetoRetenido property.
         * 
         */
        public String getIdentificacionSujetoRetenido() {
            return identificacionSujetoRetenido;
        }

        /**
         * Sets the value of the identificacionSujetoRetenido property.
         * 
         */
        public void setIdentificacionSujetoRetenido(String value) {
            this.identificacionSujetoRetenido = value;
        }

        /**
         * Gets the value of the periodoFiscal property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPeriodoFiscal() {
            return periodoFiscal;
        }

        /**
         * Sets the value of the periodoFiscal property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPeriodoFiscal(String value) {
            this.periodoFiscal = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ambiente" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="tipoEmision" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="razonSocial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nombreComercial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ruc" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="claveAcceso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="codDoc" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="estab" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="ptoEmi" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *         &lt;element name="secuencial" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *         &lt;element name="dirMatriz" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ambiente",
        "tipoEmision",
        "razonSocial",
        "nombreComercial",
        "ruc",
        "claveAcceso",
        "codDoc",
        "estab",
        "ptoEmi",
        "secuencial",
        "dirMatriz"
    })
    public static class InfoTributaria {

        protected String ambiente;
        protected String tipoEmision;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected String nombreComercial;
        protected String ruc;
        @XmlElement(required = true)
        protected String claveAcceso;
        protected String codDoc;
        protected String estab;
        protected String ptoEmi;
        protected String secuencial;
        @XmlElement(required = true)
        protected String dirMatriz;

        /**
         * Gets the value of the ambiente property.
         * 
         */
        public String getAmbiente() {
            return ambiente;
        }

        /**
         * Sets the value of the ambiente property.
         * 
         */
        public void setAmbiente(String value) {
            this.ambiente = value;
        }

        /**
         * Gets the value of the tipoEmision property.
         * 
         */
        public String getTipoEmision() {
            return tipoEmision;
        }

        /**
         * Sets the value of the tipoEmision property.
         * 
         */
        public void setTipoEmision(String value) {
            this.tipoEmision = value;
        }

        /**
         * Gets the value of the razonSocial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRazonSocial() {
            return razonSocial;
        }

        /**
         * Sets the value of the razonSocial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRazonSocial(String value) {
            this.razonSocial = value;
        }

        /**
         * Gets the value of the nombreComercial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreComercial() {
            return nombreComercial;
        }

        /**
         * Sets the value of the nombreComercial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreComercial(String value) {
            this.nombreComercial = value;
        }

        /**
         * Gets the value of the ruc property.
         * 
         */
        public String getRuc() {
            return ruc;
        }

        /**
         * Sets the value of the ruc property.
         * 
         */
        public void setRuc(String value) {
            this.ruc = value;
        }

        /**
         * Gets the value of the claveAcceso property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public String getClaveAcceso() {
            return claveAcceso;
        }

        /**
         * Sets the value of the claveAcceso property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setClaveAcceso(String value) {
            this.claveAcceso = value;
        }

        /**
         * Gets the value of the codDoc property.
         * 
         */
        public String getCodDoc() {
            return codDoc;
        }

        /**
         * Sets the value of the codDoc property.
         * 
         */
        public void setCodDoc(String value) {
            this.codDoc = value;
        }

        /**
         * Gets the value of the estab property.
         * 
         */
        public String getEstab() {
            return estab;
        }

        /**
         * Sets the value of the estab property.
         * 
         */
        public void setEstab(String value) {
            this.estab = value;
        }

        /**
         * Gets the value of the ptoEmi property.
         * 
         */
        public String getPtoEmi() {
            return ptoEmi;
        }

        /**
         * Sets the value of the ptoEmi property.
         * 
         */
        public void setPtoEmi(String value) {
            this.ptoEmi = value;
        }

        /**
         * Gets the value of the secuencial property.
         * 
         */
        public String getSecuencial() {
            return secuencial;
        }

        /**
         * Sets the value of the secuencial property.
         * 
         */
        public void setSecuencial(String value) {
            this.secuencial = value;
        }

        /**
         * Gets the value of the dirMatriz property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirMatriz() {
            return dirMatriz;
        }

        /**
         * Sets the value of the dirMatriz property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirMatriz(String value) {
            this.dirMatriz = value;
        }

    }
}
