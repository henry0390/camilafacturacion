/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Acer
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoTributaria",
    "infoNotaDebito",
    "motivos",
    "infoAdicional"
})
@XmlRootElement(name = "notaDebito")
public class NotaDebitoXml {

    @XmlElement(required = true)
    protected NotaDebitoXml.InfoTributaria infoTributaria;
    @XmlElement(required = true)
    protected NotaDebitoXml.InfoNotaDebito infoNotaDebito;
    @XmlElement(required = true)
    protected NotaDebitoXml.Motivos motivos;
    @XmlElement(required = true)
    protected NotaDebitoXml.InfoAdicional infoAdicional;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlAttribute(name = "id")
    protected String id;

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "campoAdicional"
    })
    public static class InfoAdicional {

        protected List<NotaDebitoXml.InfoAdicional.CampoAdicional> campoAdicional;

        public List<NotaDebitoXml.InfoAdicional.CampoAdicional> getCampoAdicional() {
            if (campoAdicional == null) {
                campoAdicional = new ArrayList<>();
            }
            return this.campoAdicional;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CampoAdicional {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "nombre")
            protected String nombre;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String value) {
                this.nombre = value;
            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fechaEmision",
        "dirEstablecimiento",
        "tipoIdentificacionComprador",
        "razonSocialComprador",
        "identificacionComprador",
        "obligadoContabilidad",
        "codDocModificado",
        "numDocModificado",
        "fechaEmisionDocSustento",
        "totalSinImpuestos",
        "impuestos",
        "valorTotal",
        "pagos"
    })
    public static class InfoNotaDebito {

        @XmlElement(required = true)
        protected String fechaEmision;
        @XmlElement(required = true)
        protected String dirEstablecimiento;
        protected byte tipoIdentificacionComprador;
        @XmlElement(required = true)
        protected String razonSocialComprador;
        protected String identificacionComprador;
        @XmlElement(required = true)
        protected String obligadoContabilidad;
        protected String codDocModificado;
        @XmlElement(required = true)
        protected String numDocModificado;
        @XmlElement(required = true)
        protected String fechaEmisionDocSustento;
        protected Double totalSinImpuestos;
        @XmlElement(required = true)
        protected NotaDebitoXml.InfoNotaDebito.Impuestos impuestos;
        protected Double valorTotal;
        @XmlElement(required = true)
        protected NotaDebitoXml.InfoNotaDebito.Pagos pagos;

        public String getFechaEmision() {
            return fechaEmision;
        }

        public void setFechaEmision(String value) {
            this.fechaEmision = value;
        }

        public String getDirEstablecimiento() {
            return dirEstablecimiento;
        }

        public void setDirEstablecimiento(String value) {
            this.dirEstablecimiento = value;
        }

        public byte getTipoIdentificacionComprador() {
            return tipoIdentificacionComprador;
        }

        public void setTipoIdentificacionComprador(byte value) {
            this.tipoIdentificacionComprador = value;
        }

        public String getRazonSocialComprador() {
            return razonSocialComprador;
        }

        public void setRazonSocialComprador(String value) {
            this.razonSocialComprador = value;
        }

        public String getIdentificacionComprador() {
            return identificacionComprador;
        }

        public void setIdentificacionComprador(String identificacionComprador) {
            this.identificacionComprador = identificacionComprador;
        }

        public String getObligadoContabilidad() {
            return obligadoContabilidad;
        }

        public void setObligadoContabilidad(String value) {
            this.obligadoContabilidad = value;
        }

        public String getCodDocModificado() {
            return codDocModificado;
        }

        public void setCodDocModificado(String codDocModificado) {
            this.codDocModificado = codDocModificado;
        }

        public String getNumDocModificado() {
            return numDocModificado;
        }

        public void setNumDocModificado(String value) {
            this.numDocModificado = value;
        }

        public String getFechaEmisionDocSustento() {
            return fechaEmisionDocSustento;
        }

        public void setFechaEmisionDocSustento(String value) {
            this.fechaEmisionDocSustento = value;
        }

        public Double getTotalSinImpuestos() {
            return totalSinImpuestos;
        }

        public void setTotalSinImpuestos(Double totalSinImpuestos) {
            this.totalSinImpuestos = totalSinImpuestos;
        }

        public NotaDebitoXml.InfoNotaDebito.Impuestos getImpuestos() {
            return impuestos;
        }

        public void setImpuestos(NotaDebitoXml.InfoNotaDebito.Impuestos value) {
            this.impuestos = value;
        }

        public Double getValorTotal() {
            return valorTotal;
        }

        public void setValorTotal(Double value) {
            this.valorTotal = value;
        }

        public NotaDebitoXml.InfoNotaDebito.Pagos getPagos() {
            return pagos;
        }

        public void setPagos(NotaDebitoXml.InfoNotaDebito.Pagos value) {
            this.pagos = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "impuesto"
        })
        public static class Impuestos {

            @XmlElement(required = true)
            protected NotaDebitoXml.InfoNotaDebito.Impuestos.Impuesto impuesto;

            public NotaDebitoXml.InfoNotaDebito.Impuestos.Impuesto getImpuesto() {
                return impuesto;
            }

            public void setImpuesto(NotaDebitoXml.InfoNotaDebito.Impuestos.Impuesto value) {
                this.impuesto = value;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "codigo",
                "codigoPorcentaje",
                "tarifa",
                "baseImponible",
                "valor"
            })
            public static class Impuesto {

                protected String codigo;
                protected String codigoPorcentaje;
                protected Double tarifa;
                protected Double baseImponible;
                protected Double valor;

                public String getCodigo() {
                    return codigo;
                }

                public void setCodigo(String codigo) {
                    this.codigo = codigo;
                }

                public String getCodigoPorcentaje() {
                    return codigoPorcentaje;
                }

                public void setCodigoPorcentaje(String codigoPorcentaje) {
                    this.codigoPorcentaje = codigoPorcentaje;
                }

                public Double getTarifa() {
                    return tarifa;
                }

                public void setTarifa(Double tarifa) {
                    this.tarifa = tarifa;
                }

                public Double getBaseImponible() {
                    return baseImponible;
                }

                public void setBaseImponible(Double baseImponible) {
                    this.baseImponible = baseImponible;
                }

                public Double getValor() {
                    return valor;
                }

                public void setValor(Double valor) {
                    this.valor = valor;
                }

            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pago"
        })
        public static class Pagos {

            @XmlElement(required = true)
            protected NotaDebitoXml.InfoNotaDebito.Pagos.Pago pago;

            public NotaDebitoXml.InfoNotaDebito.Pagos.Pago getPago() {
                return pago;
            }

            public void setPago(NotaDebitoXml.InfoNotaDebito.Pagos.Pago value) {
                this.pago = value;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "formaPago",
                "total",
                "plazo",
                "unidadTiempo"
            })
            public static class Pago {

                protected String formaPago;
                protected Double total;
                protected String plazo;
                @XmlElement(required = true)
                protected String unidadTiempo;

                public String getFormaPago() {
                    return formaPago;
                }

                public void setFormaPago(String formaPago) {
                    this.formaPago = formaPago;
                }

                public Double getTotal() {
                    return total;
                }

                public void setTotal(Double total) {
                    this.total = total;
                }

                public String getPlazo() {
                    return plazo;
                }

                public void setPlazo(String plazo) {
                    this.plazo = plazo;
                }

                public String getUnidadTiempo() {
                    return unidadTiempo;
                }

                public void setUnidadTiempo(String unidadTiempo) {
                    this.unidadTiempo = unidadTiempo;
                }

            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ambiente",
        "tipoEmision",
        "razonSocial",
        "nombreComercial",
        "ruc",
        "claveAcceso",
        "codDoc",
        "estab",
        "ptoEmi",
        "secuencial",
        "dirMatriz"
    })
    public static class InfoTributaria {

        protected String ambiente;
        protected String tipoEmision;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected String nombreComercial;
        protected String ruc;
        @XmlElement(required = true)
        protected BigInteger claveAcceso;
        protected String codDoc;
        protected String estab;
        protected String ptoEmi;
        protected String secuencial;
        @XmlElement(required = true)
        protected String dirMatriz;

        public String getAmbiente() {
            return ambiente;
        }

        public void setAmbiente(String ambiente) {
            this.ambiente = ambiente;
        }

        public String getTipoEmision() {
            return tipoEmision;
        }

        public void setTipoEmision(String tipoEmision) {
            this.tipoEmision = tipoEmision;
        }

        public String getRazonSocial() {
            return razonSocial;
        }

        public void setRazonSocial(String razonSocial) {
            this.razonSocial = razonSocial;
        }

        public String getNombreComercial() {
            return nombreComercial;
        }

        public void setNombreComercial(String nombreComercial) {
            this.nombreComercial = nombreComercial;
        }

        public String getRuc() {
            return ruc;
        }

        public void setRuc(String ruc) {
            this.ruc = ruc;
        }

        public BigInteger getClaveAcceso() {
            return claveAcceso;
        }

        public void setClaveAcceso(BigInteger claveAcceso) {
            this.claveAcceso = claveAcceso;
        }

        public String getCodDoc() {
            return codDoc;
        }

        public void setCodDoc(String codDoc) {
            this.codDoc = codDoc;
        }

        public String getEstab() {
            return estab;
        }

        public void setEstab(String estab) {
            this.estab = estab;
        }

        public String getPtoEmi() {
            return ptoEmi;
        }

        public void setPtoEmi(String ptoEmi) {
            this.ptoEmi = ptoEmi;
        }

        public String getSecuencial() {
            return secuencial;
        }

        public void setSecuencial(String secuencial) {
            this.secuencial = secuencial;
        }

        public String getDirMatriz() {
            return dirMatriz;
        }

        public void setDirMatriz(String dirMatriz) {
            this.dirMatriz = dirMatriz;
        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "motivo"
    })
    public static class Motivos {

        @XmlElement(required = true)
        protected NotaDebitoXml.Motivos.Motivo motivo;

        public NotaDebitoXml.Motivos.Motivo getMotivo() {
            return motivo;
        }

        public void setMotivo(NotaDebitoXml.Motivos.Motivo value) {
            this.motivo = value;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "razon",
            "valor"
        })
        public static class Motivo {

            @XmlElement(required = true)
            protected String razon;
            protected Double valor;

            public String getRazon() {
                return razon;
            }

            public void setRazon(String value) {
                this.razon = value;
            }

            public Double getValor() {
                return valor;
            }

            public void setValor(Double value) {
                this.valor = value;
            }

        }

    }
}
