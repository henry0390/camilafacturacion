/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class FactCabventa implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private int FACCVT_numsecuenc;
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String GENMOD_codigo;
    private String GENTDO_codigo;
    private String FACCVT_numdocumen;
    private int FACCVT_numrelplan;
    private String GENTDO_docrelacio;
    private String CXCRUT_rutarela;
    private int FACCVT_numdocrela;
    private int FACCVT_secdocurel;
    private Date FACCVT_fechaemisi;
    private Date FACCVT_fechadespa;
    private String FACVDR_codigo;
    private String CXCCLI_codigo;
    private String CXCFPG_codigo;
    private String CXCTPG_codigo;
    private String FACCVT_codrutavta;
    private String FACCVT_rutdistcli;
    private String FACCVT_descrilarg;
    private String FACCVT_motvdevolu;
    private BigDecimal FACCVT_subtotamov;
    private BigDecimal FACCVT_desctomov;
    private BigDecimal FACCVT_totimpumov;
    //private BigDecimal FACCVT_imptoiva;
    //private BigDecimal FACCVT_imptoser;
    //private BigDecimal FACCVT_imptoice;
    private BigDecimal FACCVT_netomov;
    private BigDecimal FACCVT_transpmov;
    private BigDecimal FACCVT_recargomov;
    private String FACCVT_estado;
    private String FACCVT_usuaingreg;
    private String FACCVT_usuamodreg;
    private Date FACCVT_fechingreg;
    private Date FACCVT_fechmodreg;
    private String FACDVT_listaprecio;
    private int FACCVT_idsecuencia;
    private String FACCVT_autorizacion;
    private Date FACCVT_fechaautoriza;
    private String FACCVT_claveacceso;
    private String FACGUI_autorizacion;
    private Date FACGUI_fechaautoriza;
    private String FACGUI_claveacceso;
    //private String ID_RELACIONAL;
    private String CXCCLI_razonsocia;
    private String xmlText;
    private String estadoEnvio;
    private String idMensaje;
    private String estadoAutorizado;
    
    public FactCabventa() {
    }

    public FactCabventa(int FACCVT_numsecuenc, String GENCIA_codigo, String GENOFI_codigo, String GENMOD_codigo, String GENTDO_codigo, String FACCVT_numdocumen, int FACCVT_numrelplan, 
            String GENTDO_docrelacio, String CXCRUT_rutarela, int FACCVT_numdocrela, int FACCVT_secdocurel, Date FACCVT_fechaemisi, Date FACCVT_fechadespa, String FACVDR_codigo, 
            String CXCCLI_codigo, String CXCFPG_codigo, String CXCTPG_codigo, String FACCVT_codrutavta, String FACCVT_rutdistcli, String FACCVT_descrilarg, String FACCVT_motvdevolu, 
            BigDecimal FACCVT_subtotamov, BigDecimal FACCVT_desctomov, BigDecimal FACCVT_totimpumov, BigDecimal FACCVT_netomov, 
            BigDecimal FACCVT_transpmov, BigDecimal FACCVT_recargomov, String FACCVT_estado, String FACCVT_usuaingreg, String FACCVT_usuamodreg, Date FACCVT_fechingreg, 
            Date FACCVT_fechmodreg, String FACDVT_listaprecio, int FACCVT_idsecuencia, String FACCVT_autorizacion, Date FACCVT_fechaautoriza, String FACCVT_claveacceso, 
            String FACGUI_autorizacion, Date FACGUI_fechaautoriza, String FACGUI_claveacceso, String CXCCLI_razonsocia) {
        this.FACCVT_numsecuenc = FACCVT_numsecuenc;
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.GENMOD_codigo = GENMOD_codigo;
        this.GENTDO_codigo = GENTDO_codigo;
        this.FACCVT_numdocumen = FACCVT_numdocumen;
        this.FACCVT_numrelplan = FACCVT_numrelplan;
        this.GENTDO_docrelacio = GENTDO_docrelacio;
        this.CXCRUT_rutarela = CXCRUT_rutarela;
        this.FACCVT_numdocrela = FACCVT_numdocrela;
        this.FACCVT_secdocurel = FACCVT_secdocurel;
        this.FACCVT_fechaemisi = FACCVT_fechaemisi;
        this.FACCVT_fechadespa = FACCVT_fechadespa;
        this.FACVDR_codigo = FACVDR_codigo;
        this.CXCCLI_codigo = CXCCLI_codigo;
        this.CXCFPG_codigo = CXCFPG_codigo;
        this.CXCTPG_codigo = CXCTPG_codigo;
        this.FACCVT_codrutavta = FACCVT_codrutavta;
        this.FACCVT_rutdistcli = FACCVT_rutdistcli;
        this.FACCVT_descrilarg = FACCVT_descrilarg;
        this.FACCVT_motvdevolu = FACCVT_motvdevolu;
        this.FACCVT_subtotamov = FACCVT_subtotamov;
        this.FACCVT_desctomov = FACCVT_desctomov;
        this.FACCVT_totimpumov = FACCVT_totimpumov;
        //this.FACCVT_imptoiva = FACCVT_imptoiva;
        //this.FACCVT_imptoser = FACCVT_imptoser;
        //this.FACCVT_imptoice = FACCVT_imptoice;
        this.FACCVT_netomov = FACCVT_netomov;
        this.FACCVT_transpmov = FACCVT_transpmov;
        this.FACCVT_recargomov = FACCVT_recargomov;
        this.FACCVT_estado = FACCVT_estado;
        this.FACCVT_usuaingreg = FACCVT_usuaingreg;
        this.FACCVT_usuamodreg = FACCVT_usuamodreg;
        this.FACCVT_fechingreg = FACCVT_fechingreg;
        this.FACCVT_fechmodreg = FACCVT_fechmodreg;
        this.FACDVT_listaprecio = FACDVT_listaprecio;
        this.FACCVT_idsecuencia = FACCVT_idsecuencia;
        this.FACCVT_autorizacion = FACCVT_autorizacion;
        this.FACCVT_fechaautoriza = FACCVT_fechaautoriza;
        this.FACCVT_claveacceso = FACCVT_claveacceso;
        this.FACGUI_autorizacion = FACGUI_autorizacion;
        this.FACGUI_fechaautoriza = FACGUI_fechaautoriza;
        this.FACGUI_claveacceso = FACGUI_claveacceso;
        //this.ID_RELACIONAL = ID_RELACIONAL;
        this.CXCCLI_razonsocia = CXCCLI_razonsocia;
    }

    

    public int getFACCVT_numsecuenc() {
        return FACCVT_numsecuenc;
    }

    public void setFACCVT_numsecuenc(int FACCVT_numsecuenc) {
        this.FACCVT_numsecuenc = FACCVT_numsecuenc;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENMOD_codigo() {
        return GENMOD_codigo;
    }

    public void setGENMOD_codigo(String GENMOD_codigo) {
        this.GENMOD_codigo = GENMOD_codigo;
    }

    public String getGENTDO_codigo() {
        return GENTDO_codigo;
    }

    public void setGENTDO_codigo(String GENTDO_codigo) {
        this.GENTDO_codigo = GENTDO_codigo;
    }

    public String getFACCVT_numdocumen() {
        return FACCVT_numdocumen;
    }

    public void setFACCVT_numdocumen(String FACCVT_numdocumen) {
        this.FACCVT_numdocumen = FACCVT_numdocumen;
    }

    public int getFACCVT_numrelplan() {
        return FACCVT_numrelplan;
    }

    public void setFACCVT_numrelplan(int FACCVT_numrelplan) {
        this.FACCVT_numrelplan = FACCVT_numrelplan;
    }

    public String getGENTDO_docrelacio() {
        return GENTDO_docrelacio;
    }

    public void setGENTDO_docrelacio(String GENTDO_docrelacio) {
        this.GENTDO_docrelacio = GENTDO_docrelacio;
    }

    public String getCXCRUT_rutarela() {
        return CXCRUT_rutarela;
    }

    public void setCXCRUT_rutarela(String CXCRUT_rutarela) {
        this.CXCRUT_rutarela = CXCRUT_rutarela;
    }

    public int getFACCVT_numdocrela() {
        return FACCVT_numdocrela;
    }

    public void setFACCVT_numdocrela(int FACCVT_numdocrela) {
        this.FACCVT_numdocrela = FACCVT_numdocrela;
    }

    public int getFACCVT_secdocurel() {
        return FACCVT_secdocurel;
    }

    public void setFACCVT_secdocurel(int FACCVT_secdocurel) {
        this.FACCVT_secdocurel = FACCVT_secdocurel;
    }

    public Date getFACCVT_fechaemisi() {
        return FACCVT_fechaemisi;
    }

    public void setFACCVT_fechaemisi(Date FACCVT_fechaemisi) {
        this.FACCVT_fechaemisi = FACCVT_fechaemisi;
    }

    public Date getFACCVT_fechadespa() {
        return FACCVT_fechadespa;
    }

    public void setFACCVT_fechadespa(Date FACCVT_fechadespa) {
        this.FACCVT_fechadespa = FACCVT_fechadespa;
    }

    public String getFACVDR_codigo() {
        return FACVDR_codigo;
    }

    public void setFACVDR_codigo(String FACVDR_codigo) {
        this.FACVDR_codigo = FACVDR_codigo;
    }

    public String getCXCCLI_codigo() {
        return CXCCLI_codigo;
    }

    public void setCXCCLI_codigo(String CXCCLI_codigo) {
        this.CXCCLI_codigo = CXCCLI_codigo;
    }

    public String getCXCFPG_codigo() {
        return CXCFPG_codigo;
    }

    public void setCXCFPG_codigo(String CXCFPG_codigo) {
        this.CXCFPG_codigo = CXCFPG_codigo;
    }

    public String getCXCTPG_codigo() {
        return CXCTPG_codigo;
    }

    public void setCXCTPG_codigo(String CXCTPG_codigo) {
        this.CXCTPG_codigo = CXCTPG_codigo;
    }

    public String getFACCVT_codrutavta() {
        return FACCVT_codrutavta;
    }

    public void setFACCVT_codrutavta(String FACCVT_codrutavta) {
        this.FACCVT_codrutavta = FACCVT_codrutavta;
    }

    public String getFACCVT_rutdistcli() {
        return FACCVT_rutdistcli;
    }

    public void setFACCVT_rutdistcli(String FACCVT_rutdistcli) {
        this.FACCVT_rutdistcli = FACCVT_rutdistcli;
    }

    public String getFACCVT_descrilarg() {
        return FACCVT_descrilarg;
    }

    public void setFACCVT_descrilarg(String FACCVT_descrilarg) {
        this.FACCVT_descrilarg = FACCVT_descrilarg;
    }

    public String getFACCVT_motvdevolu() {
        return FACCVT_motvdevolu;
    }

    public void setFACCVT_motvdevolu(String FACCVT_motvdevolu) {
        this.FACCVT_motvdevolu = FACCVT_motvdevolu;
    }

    public BigDecimal getFACCVT_subtotamov() {
        return FACCVT_subtotamov;
    }

    public void setFACCVT_subtotamov(BigDecimal FACCVT_subtotamov) {
        this.FACCVT_subtotamov = FACCVT_subtotamov;
    }

    public BigDecimal getFACCVT_desctomov() {
        return FACCVT_desctomov;
    }

    public void setFACCVT_desctomov(BigDecimal FACCVT_desctomov) {
        this.FACCVT_desctomov = FACCVT_desctomov;
    }

    public BigDecimal getFACCVT_totimpumov() {
        return FACCVT_totimpumov;
    }

    public void setFACCVT_totimpumov(BigDecimal FACCVT_totimpumov) {
        this.FACCVT_totimpumov = FACCVT_totimpumov;
    }

    public BigDecimal getFACCVT_netomov() {
        return FACCVT_netomov;
    }

    public void setFACCVT_netomov(BigDecimal FACCVT_netomov) {
        this.FACCVT_netomov = FACCVT_netomov;
    }

    public BigDecimal getFACCVT_transpmov() {
        return FACCVT_transpmov;
    }

    public void setFACCVT_transpmov(BigDecimal FACCVT_transpmov) {
        this.FACCVT_transpmov = FACCVT_transpmov;
    }

    public BigDecimal getFACCVT_recargomov() {
        return FACCVT_recargomov;
    }

    public void setFACCVT_recargomov(BigDecimal FACCVT_recargomov) {
        this.FACCVT_recargomov = FACCVT_recargomov;
    }

    public String getFACCVT_estado() {
        return FACCVT_estado;
    }

    public void setFACCVT_estado(String FACCVT_estado) {
        this.FACCVT_estado = FACCVT_estado;
    }

    public String getFACCVT_usuaingreg() {
        return FACCVT_usuaingreg;
    }

    public void setFACCVT_usuaingreg(String FACCVT_usuaingreg) {
        this.FACCVT_usuaingreg = FACCVT_usuaingreg;
    }

    public String getFACCVT_usuamodreg() {
        return FACCVT_usuamodreg;
    }

    public void setFACCVT_usuamodreg(String FACCVT_usuamodreg) {
        this.FACCVT_usuamodreg = FACCVT_usuamodreg;
    }

    public Date getFACCVT_fechingreg() {
        return FACCVT_fechingreg;
    }

    public void setFACCVT_fechingreg(Date FACCVT_fechingreg) {
        this.FACCVT_fechingreg = FACCVT_fechingreg;
    }

    public Date getFACCVT_fechmodreg() {
        return FACCVT_fechmodreg;
    }

    public void setFACCVT_fechmodreg(Date FACCVT_fechmodreg) {
        this.FACCVT_fechmodreg = FACCVT_fechmodreg;
    }

    public String getFACDVT_listaprecio() {
        return FACDVT_listaprecio;
    }

    public void setFACDVT_listaprecio(String FACDVT_listaprecio) {
        this.FACDVT_listaprecio = FACDVT_listaprecio;
    }

    public int getFACCVT_idsecuencia() {
        return FACCVT_idsecuencia;
    }

    public void setFACCVT_idsecuencia(int FACCVT_idsecuencia) {
        this.FACCVT_idsecuencia = FACCVT_idsecuencia;
    }

    public String getFACCVT_autorizacion() {
        return FACCVT_autorizacion;
    }

    public void setFACCVT_autorizacion(String FACCVT_autorizacion) {
        this.FACCVT_autorizacion = FACCVT_autorizacion;
    }

    public Date getFACCVT_fechaautoriza() {
        return FACCVT_fechaautoriza;
    }

    public void setFACCVT_fechaautoriza(Date FACCVT_fechaautoriza) {
        this.FACCVT_fechaautoriza = FACCVT_fechaautoriza;
    }

    public String getFACCVT_claveacceso() {
        return FACCVT_claveacceso;
    }

    public void setFACCVT_claveacceso(String FACCVT_claveacceso) {
        this.FACCVT_claveacceso = FACCVT_claveacceso;
    }

    public String getFACGUI_autorizacion() {
        return FACGUI_autorizacion;
    }

    public void setFACGUI_autorizacion(String FACGUI_autorizacion) {
        this.FACGUI_autorizacion = FACGUI_autorizacion;
    }

    public Date getFACGUI_fechaautoriza() {
        return FACGUI_fechaautoriza;
    }

    public void setFACGUI_fechaautoriza(Date FACGUI_fechaautoriza) {
        this.FACGUI_fechaautoriza = FACGUI_fechaautoriza;
    }

    public String getFACGUI_claveacceso() {
        return FACGUI_claveacceso;
    }

    public void setFACGUI_claveacceso(String FACGUI_claveacceso) {
        this.FACGUI_claveacceso = FACGUI_claveacceso;
    }

    public String getCXCCLI_razonsocia() {
        return CXCCLI_razonsocia;
    }

    public void setCXCCLI_razonsocia(String CXCCLI_razonsocia) {
        this.CXCCLI_razonsocia = CXCCLI_razonsocia;
    }

    public String getXmlText() {
        return xmlText;
    }

    public void setXmlText(String xmlText) {
        this.xmlText = xmlText;
    }

    public String getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(String estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public String getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(String idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getEstadoAutorizado() {
        return estadoAutorizado;
    }

    public void setEstadoAutorizado(String estadoAutorizado) {
        this.estadoAutorizado = estadoAutorizado;
    }
    
}
