/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.datasource;

import com.gma.camilafacturacion.setting.SisVar;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author Acer
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoTributaria",
    "infoGuiaRemision",
    "destinatarios",
    "infoAdicional"
})
@XmlRootElement(name = "guiaRemision")
public class GuiaRemisionXml {
    
    @XmlElement(required = true)
    protected GuiaRemisionXml.InfoTributaria infoTributaria;
    @XmlElement(required = true)
    protected GuiaRemisionXml.InfoGuiaRemision infoGuiaRemision;
    @XmlElement(required = true)
    protected GuiaRemisionXml.Destinatarios destinatarios;
    @XmlElement(required = true)
    protected GuiaRemisionXml.InfoAdicional infoAdicional;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "version")
    protected String version;
    @XmlTransient
    protected String numdoc;
    @XmlTransient
    protected String secuencia;
    
    public void cargarDatosAdicionales(){
        this.numdoc=this.infoTributaria.getEstab()+this.infoTributaria.getPtoEmi()+this.infoTributaria.getSecuencial();
        for (InfoAdicional.CampoAdicional campoAdicional : this.infoAdicional.campoAdicional) {
            String[] cadenaValues=campoAdicional.getValue().split("\\|");
            for (int i = 0; i < cadenaValues.length; i++) {                
                String[] cadena  = cadenaValues[i].split(":");
                if(cadena.length>0 && SisVar.etiquetasInformacionAdicional.contains(cadena[0].toUpperCase())){                    
                    if("SECFACTURA".equalsIgnoreCase(cadena[0].toUpperCase())){
                        this.secuencia=cadena[1];
                    }
                }
            }
        }  
    }

    public GuiaRemisionXml.InfoTributaria getInfoTributaria() {
        return infoTributaria;
    }

    public void setInfoTributaria(GuiaRemisionXml.InfoTributaria value) {
        this.infoTributaria = value;
    }

    public GuiaRemisionXml.InfoGuiaRemision getInfoGuiaRemision() {
        return infoGuiaRemision;
    }

    public void setInfoGuiaRemision(GuiaRemisionXml.InfoGuiaRemision value) {
        this.infoGuiaRemision = value;
    }

    public GuiaRemisionXml.Destinatarios getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(GuiaRemisionXml.Destinatarios value) {
        this.destinatarios = value;
    }

    public GuiaRemisionXml.InfoAdicional getInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(GuiaRemisionXml.InfoAdicional value) {
        this.infoAdicional = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        this.version = value;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destinatario"
    })
    public static class Destinatarios {

        protected List<GuiaRemisionXml.Destinatarios.Destinatario> destinatario;

        
        public List<GuiaRemisionXml.Destinatarios.Destinatario> getDestinatario() {
            if (destinatario == null) {
                destinatario = new ArrayList<>();
            }
            return this.destinatario;
        }


        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "identificacionDestinatario",
            "razonSocialDestinatario",
            "dirDestinatario",
            "motivoTraslado",
            "codDocSustento",
            "numDocSustento",
            "numAutDocSustento",
            "fechaEmisionDocSustento",
            "detalles"
        })
        public static class Destinatario {

            protected String identificacionDestinatario;
            @XmlElement(required = true)
            protected String razonSocialDestinatario;
            @XmlElement(required = true)
            protected String dirDestinatario;
            @XmlElement(required = true)
            protected String motivoTraslado;
            protected String codDocSustento;
            @XmlElement(required = true)
            protected String numDocSustento;
            @XmlElement(required = true)
            protected String numAutDocSustento;
            @XmlElement(required = true)
            protected String fechaEmisionDocSustento;
            @XmlElement(required = true)
            protected GuiaRemisionXml.Destinatarios.Destinatario.Detalles detalles;

            public String getIdentificacionDestinatario() {
                return identificacionDestinatario;
            }

            public void setIdentificacionDestinatario(String value) {
                this.identificacionDestinatario = value;
            }

            public String getRazonSocialDestinatario() {
                return razonSocialDestinatario;
            }

            public void setRazonSocialDestinatario(String value) {
                this.razonSocialDestinatario = value;
            }

            public String getDirDestinatario() {
                return dirDestinatario;
            }

            public void setDirDestinatario(String value) {
                this.dirDestinatario = value;
            }

            public String getMotivoTraslado() {
                return motivoTraslado;
            }

            public void setMotivoTraslado(String value) {
                this.motivoTraslado = value;
            }

            public String getCodDocSustento() {
                return codDocSustento;
            }

            public void setCodDocSustento(String value) {
                this.codDocSustento = value;
            }

            public String getNumDocSustento() {
                return numDocSustento;
            }

            public void setNumDocSustento(String value) {
                this.numDocSustento = value;
            }

            public String getNumAutDocSustento() {
                return numAutDocSustento;
            }

            public void setNumAutDocSustento(String value) {
                this.numAutDocSustento = value;
            }

            public String getFechaEmisionDocSustento() {
                return fechaEmisionDocSustento;
            }

            public void setFechaEmisionDocSustento(String value) {
                this.fechaEmisionDocSustento = value;
            }

            public GuiaRemisionXml.Destinatarios.Destinatario.Detalles getDetalles() {
                return detalles;
            }

            public void setDetalles(GuiaRemisionXml.Destinatarios.Destinatario.Detalles value) {
                this.detalles = value;
            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "detalle"
            })
            public static class Detalles {

                protected List<GuiaRemisionXml.Destinatarios.Destinatario.Detalles.Detalle> detalle;

                public List<GuiaRemisionXml.Destinatarios.Destinatario.Detalles.Detalle> getDetalle() {
                    if (detalle == null) {
                        detalle = new ArrayList<>();
                    }
                    return this.detalle;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "codigoInterno",
                    "codigoAdicional",
                    "descripcion",
                    "cantidad"
                })
                public static class Detalle {

                    protected String codigoInterno;
                    protected String codigoAdicional;
                    @XmlElement(required = true)
                    protected String descripcion;
                    protected Double cantidad;

                    public String getCodigoInterno() {
                        return codigoInterno;
                    }

                    public void setCodigoInterno(String value) {
                        this.codigoInterno = value;
                    }

                    public String getCodigoAdicional() {
                        return codigoAdicional;
                    }

                    public void setCodigoAdicional(String value) {
                        this.codigoAdicional = value;
                    }

                    public String getDescripcion() {
                        return descripcion;
                    }

                    public void setDescripcion(String value) {
                        this.descripcion = value;
                    }

                    public Double getCantidad() {
                        return cantidad;
                    }

                    public void setCantidad(Double value) {
                        this.cantidad = value;
                    }

                }

            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "campoAdicional"
    })
    public static class InfoAdicional {

        protected List<GuiaRemisionXml.InfoAdicional.CampoAdicional> campoAdicional;

        public List<GuiaRemisionXml.InfoAdicional.CampoAdicional> getCampoAdicional() {
            if (campoAdicional == null) {
                campoAdicional = new ArrayList<GuiaRemisionXml.InfoAdicional.CampoAdicional>();
            }
            return this.campoAdicional;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CampoAdicional {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "nombre")
            protected String nombre;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String value) {
                this.nombre = value;
            }

        }

    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dirEstablecimiento",
        "dirPartida",
        "razonSocialTransportista",
        "tipoIdentificacionTransportista",
        "rucTransportista",
        "obligadoContabilidad",
        "contribuyenteEspecial",
        "fechaIniTransporte",
        "fechaFinTransporte",
        "placa"
    })
    public static class InfoGuiaRemision {

        @XmlElement(required = true)
        protected String dirEstablecimiento;
        @XmlElement(required = true)
        protected String dirPartida;
        @XmlElement(required = true)
        protected String razonSocialTransportista;
        protected String tipoIdentificacionTransportista;
        protected String rucTransportista;
        @XmlElement(required = true)
        protected String obligadoContabilidad;
        protected String contribuyenteEspecial;
        @XmlElement(required = true)
        protected String fechaIniTransporte;
        @XmlElement(required = true)
        protected String fechaFinTransporte;
        protected String placa;

        public String getDirEstablecimiento() {
            return dirEstablecimiento;
        }

        public void setDirEstablecimiento(String value) {
            this.dirEstablecimiento = value;
        }

        public String getDirPartida() {
            return dirPartida;
        }

        public void setDirPartida(String value) {
            this.dirPartida = value;
        }

        public String getRazonSocialTransportista() {
            return razonSocialTransportista;
        }

        public void setRazonSocialTransportista(String value) {
            this.razonSocialTransportista = value;
        }

        public String getTipoIdentificacionTransportista() {
            return tipoIdentificacionTransportista;
        }

        public void setTipoIdentificacionTransportista(String value) {
            this.tipoIdentificacionTransportista = value;
        }

        public String getRucTransportista() {
            return rucTransportista;
        }

        public void setRucTransportista(String value) {
            this.rucTransportista = value;
        }

        public String getObligadoContabilidad() {
            return obligadoContabilidad;
        }
        
        public void setObligadoContabilidad(String value) {
            this.obligadoContabilidad = value;
        }
        
        public String getContribuyenteEspecial() {
            return contribuyenteEspecial;
        }

        public void setContribuyenteEspecial(String value) {
            this.contribuyenteEspecial = value;
        }

        public String getFechaIniTransporte() {
            return fechaIniTransporte;
        }

        public void setFechaIniTransporte(String value) {
            this.fechaIniTransporte = value;
        }

        public String getFechaFinTransporte() {
            return fechaFinTransporte;
        }

        public void setFechaFinTransporte(String value) {
            this.fechaFinTransporte = value;
        }

        public String getPlaca() {
            return placa;
        }

        public void setPlaca(String value) {
            this.placa = value;
        }

    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ambiente",
        "tipoEmision",
        "razonSocial",
        "nombreComercial",
        "ruc",
        "claveAcceso",
        "codDoc",
        "estab",
        "ptoEmi",
        "secuencial",
        "dirMatriz"
    })
    public static class InfoTributaria {

        protected String ambiente;
        protected String tipoEmision;
        @XmlElement(required = true)
        protected String razonSocial;
        @XmlElement(required = true)
        protected String nombreComercial;
        protected String ruc;
        @XmlElement(required = true)
        protected String claveAcceso;
        protected String codDoc;
        protected String estab;
        protected String ptoEmi;
        protected String secuencial;
        @XmlElement(required = true)
        protected String dirMatriz;

        public String getAmbiente() {
            return ambiente;
        }

        public void setAmbiente(String value) {
            this.ambiente = value;
        }

        public String getTipoEmision() {
            return tipoEmision;
        }
        
        public void setTipoEmision(String value) {
            this.tipoEmision = value;
        }

        public String getRazonSocial() {
            return razonSocial;
        }

        public void setRazonSocial(String value) {
            this.razonSocial = value;
        }

        public String getNombreComercial() {
            return nombreComercial;
        }

        public void setNombreComercial(String value) {
            this.nombreComercial = value;
        }

        public String getRuc() {
            return ruc;
        }

        public void setRuc(String value) {
            this.ruc = value;
        }

        public String getClaveAcceso() {
            return claveAcceso;
        }
        
        public void setClaveAcceso(String value) {
            this.claveAcceso = value;
        }

        public String getCodDoc() {
            return codDoc;
        }

        public void setCodDoc(String value) {
            this.codDoc = value;
        }

        public String getEstab() {
            return estab;
        }

        public void setEstab(String value) {
            this.estab = value;
        }

        public String getPtoEmi() {
            return ptoEmi;
        }

        public void setPtoEmi(String value) {
            this.ptoEmi = value;
        }

        public String getSecuencial() {
            return secuencial;
        }

        public void setSecuencial(String value) {
            this.secuencial = value;
        }
        
        public String getDirMatriz() {
            return dirMatriz;
        }

        public void setDirMatriz(String value) {
            this.dirMatriz = value;
        }

    }
}
