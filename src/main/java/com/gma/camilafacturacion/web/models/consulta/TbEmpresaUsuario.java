/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.models.consulta;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class TbEmpresaUsuario implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer empresaid;
    private Integer usuarioid;

    public TbEmpresaUsuario() {
    }

    public Integer getEmpresaid() {
        return empresaid;
    }

    public void setEmpresaid(Integer empresaid) {
        this.empresaid = empresaid;
    }

    public Integer getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(Integer usuarioid) {
        this.usuarioid = usuarioid;
    }
    
    
}
