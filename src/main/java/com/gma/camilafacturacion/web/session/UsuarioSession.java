/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.web.session;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Acer
 */
@Named(value = "usuarioSession")
@SessionScoped
public class UsuarioSession implements Serializable {
    protected TbEmpresa empresa;
    protected TbUsuario usuario;
    protected String urlSolicitada;
    /**
     * Creates a new instance of UsuarioSession
     */
    public UsuarioSession() {
    }
    
    public void redirectInvitado() {
        if (this.usuario != null) {
            HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();            
            return;
        }
        
        this.persistReqUrl();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/faces/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UsuarioSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void persistReqUrl() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        // devuelve /myaplicacion/faces + /page.xhtml
        HttpServletRequest servletRequest = (HttpServletRequest) ctx.getExternalContext().getRequest();
        // returns something like "/myapplication/faces/home.faces?param=123"
        String fullURI = servletRequest.getRequestURI();
        if(servletRequest.getQueryString()!=null && servletRequest.getQueryString().length()>0)
            fullURI = fullURI + "?"+ servletRequest.getQueryString();
        this.setUrlSolicitada(fullURI);
        System.out.println("Url Solicidata Persistida: " + this.getUrlSolicitada());
    }
    
    public void redirectUrlSolicitada(){
        if(this.getUrlSolicitada()!=null){
            try {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(this.getUrlSolicitada());
                this.setUrlSolicitada(null);                
            } catch (IOException ex) {
                Logger.getLogger(UsuarioSession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            try {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(ec.getRequestContextPath() + "/faces/index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(UsuarioSession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void logout() {
        this.setUsuario(null);        
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/faces/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UsuarioSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public TbEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TbEmpresa empresa) {
        this.empresa = empresa;
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }

    public String getUrlSolicitada() {
        return urlSolicitada;
    }

    public void setUrlSolicitada(String urlSolicitada) {
        this.urlSolicitada = urlSolicitada;
    }
    
}
