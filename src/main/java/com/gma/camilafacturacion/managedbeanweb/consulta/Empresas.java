/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.consulta;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.lazy.facturacion.TbEmpresaLazy;
import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Acer
 */
@Named(value = "empresas")
@ViewScoped
public class Empresas implements Serializable{
    @EJB
    private EntityManagerLocal manager;
    
    protected TbEmpresaLazy empresaLazy;
    protected TbEmpresa empresa;
    
    @PostConstruct
    public void init() {
        this.empresaLazy= new TbEmpresaLazy();
    }
    
    public void actualizarEmpresa(){
        try{
            this.manager.persist(empresa);
            JsfUtil.messageInfo(null, "PROCESO EXITOSO", "PROCESO EXITOSO");
        } catch (Exception e) {
            Logger.getLogger(Empresas.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public TbEmpresaLazy getEmpresaLazy() {
        return empresaLazy;
    }

    public void setEmpresaLazy(TbEmpresaLazy empresaLazy) {
        this.empresaLazy = empresaLazy;
    }

    public TbEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TbEmpresa empresa) {
        this.empresa = empresa;
    }
    
    
}
