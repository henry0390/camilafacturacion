/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.consulta;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.lazy.facturacion.TbUsuarioLazy;
import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Acer
 */
@Named(value = "usuarios")
@ViewScoped
public class Usuarios implements Serializable{
    @EJB
    private EntityManagerLocal manager;
    
    private TbUsuarioLazy usuariosLazy;
    
    protected TbUsuario user;
    
    @PostConstruct
    public void init() {
        this.usuariosLazy= new TbUsuarioLazy();
    }

    public TbUsuarioLazy getUsuariosLazy() {
        return usuariosLazy;
    }

    public void setUsuariosLazy(TbUsuarioLazy usuariosLazy) {
        this.usuariosLazy = usuariosLazy;
    }

    public void resetarPassword(TbUsuario usuario){
        try{
            usuario.setClave(usuario.getIdentificacion());
            this.manager.persist(usuario);
            JsfUtil.messageInfo(null, "PROCESO EXITOSO", "PROCESO EXITOSO");
        } catch (Exception e) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void editarUsuario(TbUsuario usuario){
        try{
            if(usuario.getCorreo()==null)
                usuario.setCorreo("");
            usuario=(TbUsuario) this.manager.persist(usuario);
            JsfUtil.messageInfo(null, "PROCESO EXITOSO", "PROCESO EXITOSO");
        } catch (Exception e) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public EntityManagerLocal getManager() {
        return manager;
    }

    public void setManager(EntityManagerLocal manager) {
        this.manager = manager;
    }

    public TbUsuario getUser() {
        return user;
    }

    public void setUser(TbUsuario user) {
        this.user = user;
    }
    
}
