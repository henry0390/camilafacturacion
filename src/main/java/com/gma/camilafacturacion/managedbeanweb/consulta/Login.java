/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.consulta;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.MenuView;
import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.models.consulta.TbEmpresaUsuario;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import com.gma.camilafacturacion.web.session.UsuarioSession;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "login")
@ViewScoped
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UsuarioSession usuarioSession;
    @Inject
    private MenuView menuView;
    @EJB
    private EntityManagerLocal manager;

    protected String identificacion;
    protected String password;
    protected String empresa;

    /**
     * Creates a new instance of Login
     */
    public Login() {
    }

    public void acceder() {
        if (identificacion != null && password != null && identificacion.length() > 0 && password.length() > 0) {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("identificacion", this.identificacion);
            parametros.put("clave", this.password);
            TbUsuario user = this.manager.findObject(TbUsuario.class, parametros);
            if (user != null) {
                if (this.identificacion.equalsIgnoreCase("administrador")) {
                    parametros = new HashMap<>();
                    parametros.put("1", this.empresa);
                    parametros.put("2", user.getId());
                    TbEmpresaUsuario empresaUsuario = (TbEmpresaUsuario) this.manager.findObject("SQL", "SELECT eu.empresaid, eu.usuarioid "
                            + "FROM autorizacion.tbempresausuario eu INNER JOIN autorizacion.tbempresa e ON (eu.empresaid=e.id) WHERE e.identificacion=? AND eu.usuarioid=?", parametros, TbEmpresaUsuario.class, Boolean.FALSE);
                    /*TbEmpresaUsuario empresaUsuario= (TbEmpresaUsuario)this.manager.findObject("SQL", "SELECT eu.empresaid, eu.usuarioid "
                            + "FROM tbempresausuario eu INNER JOIN tbempresa e ON (eu.empresaid=e.id) WHERE e.identificacion=? AND eu.usuarioid=?", parametros, TbEmpresaUsuario.class, Boolean.FALSE);*/
                    if (empresaUsuario != null) {
                        JsfUtil.messageInfo(null, "Bienvenid@ " + user.getNombre(), "");
                        this.usuarioSession.setUsuario(user);
                        this.usuarioSession.setEmpresa(this.manager.find(TbEmpresa.class, new Long(empresaUsuario.getEmpresaid())));
                        this.menuView.init();
                        this.usuarioSession.redirectUrlSolicitada();
                    } else {
                        JsfUtil.messageError(null, "Usuario o Clave incorrecta", "");
                    }
                } else {
                    JsfUtil.messageInfo(null, "Bienvenid@ " + user, "");
                    this.usuarioSession.setUsuario(user);
                    this.menuView.init();
                    this.usuarioSession.redirectUrlSolicitada();
                }
            } else {
                JsfUtil.messageError(null, "Usuario o Clave incorrecta", "");
            }
        } else {
            JsfUtil.messageError(null, "Usuario y/o Contraseña vacios", "");
        }
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

}
