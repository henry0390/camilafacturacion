/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.consulta;

import autorizacion.ws.sri.gob.ec.AutorizacionComprobantesOffline;
import autorizacion.ws.sri.gob.ec.AutorizacionComprobantesOfflineService;
import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.lazy.facturacion.TbAutorizadosLazy;
import com.gma.camilafacturacion.setting.PropertiesLoader;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.Propiedades;
import com.gma.camilafacturacion.util.Utilidades;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import com.gma.camilafacturacion.web.models.consulta.RespuestaComprobante;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
import com.gma.camilafacturacion.web.session.UsuarioSession;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "documentos")
@ViewScoped
public class Documentos implements Serializable {

    @Inject
    private UsuarioSession usuarioSession;
    @EJB
    private FacturacionLocal facturacionService;
    @EJB
    private AutorizacionLocal autorizacionMainService;
    private TbAutorizadosLazy autorizadosLazy;
    private AutorizacionComprobantesOffline servicioConsulta;

    protected List<TbAutorizados> autorizadosList;

    @PostConstruct
    public void init() {
        this.autorizadosLazy = new TbAutorizadosLazy(this.usuarioSession.getEmpresa(), null, null, null, null, null);
        this.servicioConsulta = this.obtenerServicioConsultaSri();
    }

    public TbAutorizadosLazy getAutorizadosLazy() {
        return autorizadosLazy;
    }

    public void setAutorizadosLazy(TbAutorizadosLazy autorizadosLazy) {
        this.autorizadosLazy = autorizadosLazy;
    }

    public List<TbAutorizados> getAutorizadosList() {
        return autorizadosList;
    }

    public void setAutorizadosList(List<TbAutorizados> autorizadosList) {
        this.autorizadosList = autorizadosList;
    }

    public void procesoCosulta() {
        try{
            RespuestaComprobante rc;
            if (this.autorizadosList != null && !this.autorizadosList.isEmpty()) {
                for (TbAutorizados tbAutorizados : this.autorizadosList) {
                    if (tbAutorizados.getTbDocumentoXML() == null) {
                        rc = this.servicioConsulta.autorizacionComprobante(tbAutorizados.getClaveAcceso());
                        if (rc != null && rc.getAutorizaciones() != null && !rc.getAutorizaciones().getAutorizacion().isEmpty()) {
                            for (DocumentoAutorizado da : rc.getAutorizaciones().getAutorizacion()) {                            
                                if ("AUTORIZADO".equalsIgnoreCase(rc.getAutorizaciones().getAutorizacion().get(0).getEstado())) {
                                    String xml = this.facturacionService.consultarTextDocumentoAutorizado(da);
                                    this.autorizacionMainService.grabarXmlAutorizado(tbAutorizados, xml);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void descargarPdf(TbAutorizados doc) throws IOException {
        try {
            this.facturacionService.descargarDocumentoPdf(doc, Boolean.FALSE, FacesContext.getCurrentInstance().getExternalContext());
        } catch (Exception e) {
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void descargarXml(TbAutorizados doc) throws IOException {
        try {
            this.facturacionService.descargarDocumentoXml(doc);
        } catch (Exception e) {
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void enviarCorreo(TbAutorizados doc) {
        try {
            this.facturacionService.descargarDocumentoPdf(doc, Boolean.TRUE, FacesContext.getCurrentInstance().getExternalContext());
            //this.facturacionService.generarDocumentoXml(doc);
            this.facturacionService.generarXmlBuilder(doc);
            Propiedades.formatoEmail = Utilidades.cargarPantillaHtml(SisVar.rutaReportes+doc.getEmpresaId().getIdentificacion()+".html");
            Propiedades.formatoEmail = Utilidades.setearValoresContentEmail(Propiedades.formatoEmail , doc);                
            this.facturacionService.enviarCorreo(doc.getUsuarioId().getCorreo(), doc.getDocumento()+"-"+doc.getNumeroDocumento(), Propiedades.plantilla?Propiedades.formatoEmail:"<h2>TEST</h2>",SisVar.rutaConsulta.replaceAll("empresa", doc.getEmpresaId().getIdentificacion()),doc.getClaveAcceso());
        } catch (Exception e) {
            Logger.getLogger(Documentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public AutorizacionComprobantesOffline obtenerServicioConsultaSri() {
        AutorizacionComprobantesOfflineService service = null;
        try {
            switch (SisVar.ambiente) {
                case "1"://DESARROLLO
                    switch (SisVar.tipo) {
                        case "1"://ONLINE
                            break;
                        case "2"://OFFLINE
                            service = new AutorizacionComprobantesOfflineService(new URL("https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl"));
                            break;
                        default:
                            break;
                    }
                    break;
                case "2"://PRODUCCION
                    switch (SisVar.tipo) {
                        case "1"://ONLINE
                            break;
                        case "2"://OFFLINE
                            service = new AutorizacionComprobantesOfflineService(new URL("https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl"));
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            return service.getAutorizacionComprobantesOfflinePort();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        /*
        private static RespuestaComprobante autorizacionComprobante(java.lang.String claveAccesoComprobante, URL url) {
        autorizacion.ws.sri.gob.ec.AutorizacionComprobantesOfflineService service = new autorizacion.ws.sri.gob.ec.AutorizacionComprobantesOfflineService(url);
        autorizacion.ws.sri.gob.ec.AutorizacionComprobantesOffline port = service.getAutorizacionComprobantesOfflinePort();
        return port.autorizacionComprobante(claveAccesoComprobante);
    }
        */
    }

}
