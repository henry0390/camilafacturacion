/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.consulta;

import com.gma.camilafacturacion.dto.AutorizadoDTO;
import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.lazy.facturacion.TbAutorizadosLazy;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.Propiedades;
import com.gma.camilafacturacion.util.Utilidades;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
import com.gma.camilafacturacion.web.session.UsuarioSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author Acer
 */
@Named
@ViewScoped
public class Consulta implements Serializable{
    @EJB
    private EntityManagerLocal entity;
    @Inject 
    private UsuarioSession usuarioSession;
    @EJB
    private AutorizacionLocal autorizacionMainService;
    @EJB
    private FacturacionLocal facturacionService;
    
    protected List<TbEmpresa> empresasList;
    protected TbEmpresa empresaSeleccionada;
    protected String identificacion;
    protected Date fecha, desde, hasta;
    protected String docSeleccionado;
    protected String numeroDoc;
    protected String claveAcceso;
    protected String correo;
    protected Boolean administrador=Boolean.FALSE;
    protected Boolean todosDocs=Boolean.FALSE;
    
    protected TbAutorizadosLazy autorizadosLazy;
    protected List<AutorizadoDTO> autorizados;

    @PostConstruct
    public void init() {
        Map<String, Object> parametros = new HashMap<>();
        try{
            this.fecha =new Date();
            this.empresasList = new ArrayList<>();
            this.administrador=this.usuarioSession!=null && this.usuarioSession.getUsuario().getIdentificacion().equalsIgnoreCase("administrador");
            if(this.administrador){
                this.empresasList.add(this.usuarioSession.getEmpresa());
            }else{
                this.identificacion = this.usuarioSession.getUsuario().getIdentificacion();
                parametros.put("0", this.usuarioSession.getUsuario().getId());
                /*List<Integer> idsEmpresa = this.entity.findAllSQL("SELECT EmpresaId FROM tbEmpresaUsuario WHERE UsuarioId=?", parametros, null);*/
                List<Integer> idsEmpresa = this.entity.findAllSQL("SELECT EmpresaId FROM autorizacion.tbEmpresaUsuario WHERE UsuarioId=?", parametros, null);
                if(idsEmpresa!=null && !idsEmpresa.isEmpty()){
                    parametros = new HashMap<>();
                    parametros.put("id", idsEmpresa);
                    this.empresasList = this.entity.findAllByParameter(TbEmpresa.class, parametros, null);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Consulta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void realizarConsulta(){
        this.autorizadosLazy = null;
        if((this.identificacion!=null &&this.identificacion.length()>0) || (this.numeroDoc!=null && this.numeroDoc.length()>0) || (this.claveAcceso!=null && this.claveAcceso.length()>0)){
            this.autorizadosLazy = new TbAutorizadosLazy(this.administrador?this.usuarioSession.getEmpresa():this.empresaSeleccionada, this.identificacion, this.docSeleccionado, this.numeroDoc!=null?(this.numeroDoc.length()>0?this.numeroDoc:null):null, this.claveAcceso!=null?(this.claveAcceso.length()>0?this.claveAcceso:null):null, this.todosDocs?null:this.fecha);
        }
    }
    
    public void enviarCorreos(){
        try{
            TbAutorizados temp;
            for(AutorizadoDTO aut : autorizados){
                temp = entity.find(TbAutorizados.class, new Long(aut.getId()));
                this.enviarCorreo(temp);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void realizarConsulta2(){
        HashMap m = new HashMap();
        m.put("1", desde);
        m.put("2", hasta);
        m.put("3", this.administrador?this.usuarioSession.getEmpresa():this.empresaSeleccionada);
        
        autorizados = (List<AutorizadoDTO>)entity.findAllSQL("SELECT aut.id, us.nombre, aut.documento, aut.numerodocumento, aut.fechaemision, aut.total FROM autorizacion.tbautorizados aut "
                + "INNER JOIN autorizacion.tbusuario us ON aut.usuarioid = us.id "
                + "WHERE aut.fechaemision BETWEEN ? AND ? AND aut.empresaid = ?", m, AutorizadoDTO.class);
        
        /*this.autorizadosLazy2 = null;
        if(this.desde != null && this.hasta != null){
            this.autorizadosLazy = new TbAutorizadosLazy(this.administrador?this.usuarioSession.getEmpresa():this.empresaSeleccionada, this.desde, this.hasta);
        }*/
    }
    
    public void descargarPdf(TbAutorizados doc) throws IOException{
        try{
            this.facturacionService.descargarDocumentoPdf(doc, Boolean.FALSE, FacesContext.getCurrentInstance().getExternalContext());
        } catch (Exception e) {
            Logger.getLogger(Consulta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void descargarXml(TbAutorizados doc) throws IOException{
        try{
            this.facturacionService.descargarDocumentoXml(doc);
        } catch (Exception e) {
            Logger.getLogger(Consulta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void enviarCorreo(TbAutorizados doc){
        try{
            this.facturacionService.descargarDocumentoPdf(doc, Boolean.TRUE, FacesContext.getCurrentInstance().getExternalContext());
            this.facturacionService.generarXmlBuilder(doc);
            Propiedades.formatoEmail = Utilidades.cargarPantillaHtml(SisVar.rutaReportes+doc.getEmpresaId().getIdentificacion()+".html");
            Propiedades.formatoEmail = Utilidades.setearValoresContentEmail(Propiedades.formatoEmail , doc);                
            this.facturacionService.enviarCorreo((this.correo!=null && this.correo.length()>0)?this.correo:doc.getUsuarioId().getCorreo(), doc.getDocumento()+"-"+doc.getNumeroDocumento(), Propiedades.plantilla?Propiedades.formatoEmail:"<h2>TEST</h2>",SisVar.rutaConsulta.replaceAll("empresa", doc.getEmpresaId().getIdentificacion()),doc.getClaveAcceso());
        } catch (Exception e) {
            Logger.getLogger(Consulta.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public List<TbEmpresa> getEmpresasList() {
        return empresasList;
    }

    public void setEmpresasList(List<TbEmpresa> empresasList) {
        this.empresasList = empresasList;
    }

    public TbEmpresa getEmpresaSeleccionada() {
        return empresaSeleccionada;
    }

    public void setEmpresaSeleccionada(TbEmpresa empresaSeleccionada) {
        this.empresaSeleccionada = empresaSeleccionada;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDocSeleccionado() {
        return docSeleccionado;
    }

    public void setDocSeleccionado(String docSeleccionado) {
        this.docSeleccionado = docSeleccionado;
    }

    public String getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(String numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Boolean getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }

    public Boolean getTodosDocs() {
        return todosDocs;
    }

    public void setTodosDocs(Boolean todosDocs) {
        this.todosDocs = todosDocs;
    }

    public TbAutorizadosLazy getAutorizadosLazy() {
        return autorizadosLazy;
    }

    public void setAutorizadosLazy(TbAutorizadosLazy autorizadosLazy) {
        this.autorizadosLazy = autorizadosLazy;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public List<AutorizadoDTO> getAutorizados() {
        return autorizados;
    }

    public void setAutorizados(List<AutorizadoDTO> autorizados) {
        this.autorizados = autorizados;
    }
    
}
