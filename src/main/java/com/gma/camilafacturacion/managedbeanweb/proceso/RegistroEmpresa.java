/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.proceso;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Acer
 */
@Named(value = "registroEmpresa")
@ViewScoped
public class RegistroEmpresa implements Serializable{
    @EJB
    private EntityManagerLocal manager;
    @EJB
    private AutorizacionLocal autorizacionMainService;
    
    protected TbEmpresa empresa;
    private Map<String, Object> parametros;
    private TbUsuario usuario;

    @PostConstruct
    public void init() {
        try{
            this.empresa = new TbEmpresa();
            this.parametros = new HashMap<>();
        } catch (Exception e) {
            Logger.getLogger(RegistroEmpresa.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void registrarEmpresa(){
        try{
            if(this.empresa.getIdentificacion()!=null && this.empresa.getNombre()!=null && this.empresa.getRutaCertificado()!=null
                    && this.empresa.getClave()!=null && this.empresa.getRutaCertificado().length()>0 && this.empresa.getClave().length()>0){
                this.parametros.put("identificacion", this.empresa.getIdentificacion());
                TbEmpresa e= this.manager.findObject(TbEmpresa.class, this.parametros);
                if(e==null){
                    e= (TbEmpresa) this.manager.persist(this.empresa);
                    if(e!=null){
                        //REGISTRO DE DIRECTORIO
                        String ruta = "/facturacion/repositorio/";
                        File dir = new File(ruta+e.getIdentificacion());
                        if(!dir.exists()){
                            dir.mkdir();
                            File dirc = new File(ruta+e.getIdentificacion()+"/consulta");
                            dirc.mkdir();
                            File dirf = new File(ruta+e.getIdentificacion()+"/firma");
                            dirf.mkdir();
                            File dirg = new File(ruta+e.getIdentificacion()+"/generado");
                            dirg.mkdir();
                        }
                        this.parametros.put("identificacion", "administrador");
                        this.usuario = this.manager.findObject(TbUsuario.class, this.parametros);
                        this.autorizacionMainService.registrarUsuarioCore(e, this.usuario.getIdentificacion(), this.usuario.getNombre(), this.usuario.getCorreo(), this.usuario.getClave());
                        JsfUtil.messageInfo(null, "Registro Exitoso!!! "+e.getId()+" - "+e.getNombre(), "");
                        this.init();
                    }else{
                        JsfUtil.messageError(null, "Error al realizar Proceso!! ", "");
                    }                    
                }else{
                    JsfUtil.messageWarning(null, "Empresa se ecuentra registrada!!! "+e.getId()+" - "+e.getNombre(), "");
                }
            }else{
                JsfUtil.messageWarning(null, "Registrar datos obligatorios!!!", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroEmpresa.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public TbEmpresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(TbEmpresa empresa) {
        this.empresa = empresa;
    }
    
}
