/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.proceso;

import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.service.local.AutorizacionDataSourceLocal;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Acer
 */
@Named(value = "cargaDatos")
@ViewScoped
public class CargaDatos implements Serializable{    
    @EJB
    private AutorizacionDataSourceLocal autorizacionService;
    
    protected Date fechaDesde;
    protected Date fechaHasta;
    
    @PostConstruct
    public void init() {
        try{
            this.fechaDesde = new Date();
            this.fechaHasta = new Date();
        } catch (Exception e) {
            Logger.getLogger(CargaDatos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void procesarDatos(){
        try{
            this.autorizacionService.cargarDatos(this.fechaDesde, this.fechaHasta);
            JsfUtil.messageInfo(null, "Proceso Exitoso!!!", "");
        } catch (Exception e) {
            Logger.getLogger(CargaDatos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
    
}
