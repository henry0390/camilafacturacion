/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.proceso;

import com.gma.camilafacturacion.setting.SisVar;
//import ec.sri.firma.principal.SriIntegracion;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author Acer
 */
@Named(value = "testMb")
@ViewScoped
public class TestMb implements Serializable{

    @PostConstruct
    public void init() {
        try{
            System.out.println("PRINT ");
        } catch (Exception e) {
            Logger.getLogger(TestMb.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void testConsulta(){
        try{
            System.out.println("CONSULTA ");
            /*SriIntegracion sri= new SriIntegracion();
            sri.setConsultaclaveAcceso("2506201901099100268500110010020000002300000078016");
            sri.setRutaManualArchivoRespuesta(SisVar.rutaConsulta+"2506201901099100268500110010020000002300000078016.xml");
            sri.validaComprobanteOffline();*/
            
        } catch (Exception e) {
            Logger.getLogger(TestMb.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
}
