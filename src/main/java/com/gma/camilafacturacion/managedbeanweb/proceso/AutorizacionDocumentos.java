/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.managedbeanweb.proceso;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.JsfUtil;
import com.gma.camilafacturacion.web.models.datasource.CxctDocumento;
import com.gma.camilafacturacion.web.models.datasource.CxptComprobanteRetencion;
import com.gma.camilafacturacion.web.models.datasource.CxptDocumentos;
import com.gma.camilafacturacion.web.models.datasource.FactCabventa;
import com.gma.camilafacturacion.web.models.datasource.FacturaXml;
import com.gma.camilafacturacion.web.models.datasource.GuiaRemisionXml;
import com.gma.camilafacturacion.web.models.datasource.InvtCabplandes;
import com.gma.camilafacturacion.web.models.datasource.NotaCreditoXml;
import com.gma.camilafacturacion.web.models.datasource.RetencionXml;
import com.gma.camilafacturacion.web.service.local.AutorizacionDataSourceLocal;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
import com.gma.camilafacturacion.web.session.UsuarioSession;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;


/**
 *
 * @author Acer
 */
@Named(value = "autorizacionDocumentos")
@ViewScoped
public class AutorizacionDocumentos implements Serializable{
    @EJB
    private AutorizacionDataSourceLocal autorizacionService;
    @EJB
    private AutorizacionLocal autorizacionMainService;
    @EJB
    private FacturacionLocal facturacionService;
    @Inject 
    private UsuarioSession usuarioSession;
    
    protected Date fechaDesde;
    protected Date fechaHasta;
    protected Boolean todos = Boolean.FALSE;
    protected Boolean todosRetenciones = Boolean.FALSE;
    protected Boolean todosGuias = Boolean.FALSE;
    protected Boolean todosNc = Boolean.FALSE;
    protected List<FactCabventa> facturas;
    protected List<CxptComprobanteRetencion> retenciones;
    protected List<FactCabventa> facturasSeleccionadas;
    protected List<CxptDocumentos> notasCreditoSeleccionadas;
    protected List<CxptComprobanteRetencion> retencionesSeleccionadas;
    
    protected List<InvtCabplandes> guiasPlanificadas;
    protected List<InvtCabplandes> guiasPlanificadasSeleccionadas;
    
    protected List<CxctDocumento> notasCreditoCxct;
    protected List<CxctDocumento> notasCreditoCxctSeleccionadas;
    
    protected String claveAcceso;
    protected String tipo;
    
    @PostConstruct
    public void init() {
        try{
            this.fechaDesde = new Date();
            this.fechaHasta = new Date();
            this.facturas =     this.autorizacionService.getFacturas(null,null);
            this.retenciones =  this.autorizacionService.getRetenciones(null,null);
            this.guiasPlanificadas = this.autorizacionService.getGuiasRemision(null, null);
            this.notasCreditoCxct = this.autorizacionService.getNotasCredito(null, null);
            this.facturasSeleccionadas= null;
            this.retencionesSeleccionadas=null;
            this.guiasPlanificadasSeleccionadas = null;
            this.notasCreditoCxctSeleccionadas = null;
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void testConsultar(){
        try{
            //this.facturacionService.enviarCorreo("henrypilco333@gmail.com", "TEST", "TEST", SisVar.rutaConsulta, "0107201901099100268500110010020000003530000078318");            
            //if(this.tipo.equalsIgnoreCase("1"))
                //this.facturacionService.desargarDocumentoPdf("0107201901099100268500110010020000003530000078318", "FAC", "2", Boolean.TRUE);
            /*SriRespuestaConsulta rc = this.autorizacionMainService.consultarByClaveAcceso("FAC", this.claveAcceso, SisVar.ambiente, this.tipo, SisVar.rutaConsulta);            
            System.out.println(rc.getEstado());*/
            JsfUtil.messageInfo(null, "MENSAJE", "mensaje");
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void consultarFacturas(){        
        try{
            this.facturas=this.autorizacionService.getFacturas(this.fechaDesde,this.fechaHasta);
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void autorizarFacturas(){
        try{
            if(this.todos?(this.facturas!=null&&!this.facturas.isEmpty()):(this.facturasSeleccionadas!=null&&!this.facturasSeleccionadas.isEmpty())){
                //GENERAR XML TEXT
                List<FactCabventa> lfXml= this.autorizacionService.consultarXmlFacturas(this.todos?this.facturas:this.facturasSeleccionadas); 
             
                //VALIDAR - REGISTRAR USUARIOS Y OBTENCION DE MODELO XML
                List<FacturaXml> listFacturaXml= this.autorizacionMainService.registarUsuarios(1,lfXml, this.usuarioSession.getEmpresa());                
                
                //FIRMAR ARCHIVO
                listFacturaXml= this.autorizacionMainService.firmar(this.usuarioSession.getEmpresa(),1,FacturaXml.class,listFacturaXml, SisVar.rutaGenerado.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()), SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()));                
                //AUTORIZAR
                List<TbAutorizados> tbAutorizados= this.autorizacionMainService.autorizar(this.usuarioSession.getEmpresa(),1,listFacturaXml,null, SisVar.ambiente, SisVar.tipo, SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()),
                        this.facturacionService, this.autorizacionMainService);                
                tbAutorizados.stream().filter((tbAutorizado) -> (tbAutorizado.getId()!=null)).forEachOrdered((tbAutorizado) -> {
                    this.autorizacionService.actualizarFactura(tbAutorizado.getClaveAcceso(), tbAutorizado.getNumeroAutorizacion(), tbAutorizado.getFechaAutorizacion());
                });                                         
                //ENVIO DE CORREO
                this.facturacionService.envioCorreos(tbAutorizados);
                this.init();
                JsfUtil.messageInfo(null, "Proceso Exitoso!!!", "");            
            }else{
                JsfUtil.messageWarning(null, "No existen registros a Procesar", "");
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void consultarRetenciones(){
        try{
            this.retenciones =  this.autorizacionService.getRetenciones(this.fechaDesde, this.fechaHasta);        
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void autorizarRetenciones(){
        try{
            if(this.todosRetenciones?(this.retenciones!=null&&!this.retenciones.isEmpty()):(this.retencionesSeleccionadas!=null&&!this.retencionesSeleccionadas.isEmpty())){
                //GENERAR XML
                List<CxptComprobanteRetencion> lrXml= this.autorizacionService.consultarXmlRetenciones(this.todosRetenciones?this.retenciones:this.retencionesSeleccionadas);
                //VALIDAR - REGISTRAR USUARIOS Y OBTENCION DE MODELO XML
                List<RetencionXml> listRetencionXml= this.autorizacionMainService.registarUsuarios(2,lrXml, this.usuarioSession.getEmpresa());
                //FIRMAR ARCHIVO
                listRetencionXml= this.autorizacionMainService.firmar(this.usuarioSession.getEmpresa(),2,RetencionXml.class,listRetencionXml, SisVar.rutaGenerado.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()), SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()));
                //AUTORIZAR
                List<TbAutorizados> tbAutorizados= this.autorizacionMainService.autorizar(this.usuarioSession.getEmpresa(),2,null,listRetencionXml, SisVar.ambiente, SisVar.tipo, SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()),
                        this.facturacionService, this.autorizacionMainService);                
                tbAutorizados.stream().filter((tbAutorizado) -> (tbAutorizado.getId()!=null)).forEachOrdered((tbAutorizado) -> {
                    this.autorizacionService.actualizarFactura(tbAutorizado.getClaveAcceso(), tbAutorizado.getNumeroAutorizacion(), tbAutorizado.getFechaAutorizacion());
                });
                //ENVIO DE CORREO
                //this.facturacionService.envioCorreos(tbAutorizados);
                JsfUtil.messageInfo(null, "Proceso Exitoso!!!", "");
                this.init();
            }else{
                JsfUtil.messageWarning(null, "No existen registros a Procesar", "");
            }
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }
    
    public void consultarGuiasPlanificadas(){
        try{
            this.guiasPlanificadas = this.autorizacionService.getGuiasRemision(this.fechaDesde, this.fechaHasta);
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void autorizarGuiasPlanificadas(){
        try{
            if(this.todosGuias?(this.guiasPlanificadas!=null&&!this.guiasPlanificadas.isEmpty()):(this.guiasPlanificadasSeleccionadas!=null&&!this.guiasPlanificadasSeleccionadas.isEmpty())){
                //GENERAR XML
                List<InvtCabplandes> lgrXml= this.autorizacionService.consultarXmlGuiasRemision(this.todosGuias?this.guiasPlanificadas:this.guiasPlanificadasSeleccionadas);
                if(lgrXml!=null && !lgrXml.isEmpty())
                    this.autorizacionMainService.firmar(this.usuarioSession.getEmpresa(),3,GuiaRemisionXml.class,lgrXml, SisVar.rutaGenerado.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()), SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()));
                else
                    JsfUtil.messageWarning(null, "No existen registros a Procesar luego de generar xml", "");
            }else{
                JsfUtil.messageWarning(null, "No existen registros a Procesar", "");
            }
                
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void consultarNotasCreditoCxct(){
        try{
            this.notasCreditoCxct = this.autorizacionService.getNotasCredito(this.fechaDesde, this.fechaHasta);
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void autorizarNotasCreditoCxct(){
        try{
            List<CxctDocumento> lncXml= this.autorizacionService.consultarXmlNotasCredito(this.todosNc?this.notasCreditoCxct:this.notasCreditoCxctSeleccionadas);
            this.autorizacionMainService.firmar(this.usuarioSession.getEmpresa(), 4,NotaCreditoXml.class,lncXml, SisVar.rutaGenerado.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()), SisVar.rutaFirma.replaceAll("empresa", this.usuarioSession.getEmpresa().getIdentificacion()));
        } catch (Exception e) {
            Logger.getLogger(AutorizacionDocumentos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Boolean getTodos() {
        return todos;
    }

    public void setTodos(Boolean todos) {
        this.todos = todos;
    }

    public Boolean getTodosRetenciones() {
        return todosRetenciones;
    }

    public void setTodosRetenciones(Boolean todosRetenciones) {
        this.todosRetenciones = todosRetenciones;
    }

    public Boolean getTodosGuias() {
        return todosGuias;
    }

    public void setTodosGuias(Boolean todosGuias) {
        this.todosGuias = todosGuias;
    }

    public Boolean getTodosNc() {
        return todosNc;
    }

    public void setTodosNc(Boolean todosNc) {
        this.todosNc = todosNc;
    }

    public List<FactCabventa> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<FactCabventa> facturas) {
        this.facturas = facturas;
    }

    public List<CxptComprobanteRetencion> getRetenciones() {
        return retenciones;
    }

    public void setRetenciones(List<CxptComprobanteRetencion> retenciones) {
        this.retenciones = retenciones;
    }

    public List<FactCabventa> getFacturasSeleccionadas() {
        return facturasSeleccionadas;
    }

    public void setFacturasSeleccionadas(List<FactCabventa> facturasSeleccionadas) {
        this.facturasSeleccionadas = facturasSeleccionadas;
    }

    public List<CxptDocumentos> getNotasCreditoSeleccionadas() {
        return notasCreditoSeleccionadas;
    }

    public void setNotasCreditoSeleccionadas(List<CxptDocumentos> notasCreditoSeleccionadas) {
        this.notasCreditoSeleccionadas = notasCreditoSeleccionadas;
    }

    public List<CxptComprobanteRetencion> getRetencionesSeleccionadas() {
        return retencionesSeleccionadas;
    }

    public void setRetencionesSeleccionadas(List<CxptComprobanteRetencion> retencionesSeleccionadas) {
        this.retencionesSeleccionadas = retencionesSeleccionadas;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public List<InvtCabplandes> getGuiasPlanificadas() {
        return guiasPlanificadas;
    }

    public void setGuiasPlanificadas(List<InvtCabplandes> guiasPlanificadas) {
        this.guiasPlanificadas = guiasPlanificadas;
    }

    public List<InvtCabplandes> getGuiasPlanificadasSeleccionadas() {
        return guiasPlanificadasSeleccionadas;
    }

    public void setGuiasPlanificadasSeleccionadas(List<InvtCabplandes> guiasPlanificadasSeleccionadas) {
        this.guiasPlanificadasSeleccionadas = guiasPlanificadasSeleccionadas;
    }

    public List<CxctDocumento> getNotasCreditoCxct() {
        return notasCreditoCxct;
    }

    public void setNotasCreditoCxct(List<CxctDocumento> notasCreditoCxct) {
        this.notasCreditoCxct = notasCreditoCxct;
    }

    public List<CxctDocumento> getNotasCreditoCxctSeleccionadas() {
        return notasCreditoCxctSeleccionadas;
    }

    public void setNotasCreditoCxctSeleccionadas(List<CxctDocumento> notasCreditoCxctSeleccionadas) {
        this.notasCreditoCxctSeleccionadas = notasCreditoCxctSeleccionadas;
    }
    
}
