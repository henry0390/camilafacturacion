/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

import com.gma.camilafacturacion.util.Propiedades;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletContext;
import jodd.props.Props;

/**
 *
 * @author Acer
 */
public class PropertiesLoader {
    protected ServletContext servletContext;

    public PropertiesLoader(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
        
    public void load() {
        Props prop = new Props();
        try{
            prop.load(servletContext.getResourceAsStream("/WEB-INF/settings.properties"));
            SisVar.driverClassFrom =    prop.getValue("fromdb.driverClass");
            SisVar.urlFrom =            prop.getValue("fromdb.url");
            SisVar.usernameFrom =       prop.getValue("fromdb.username");
            SisVar.passwordFrom =       prop.getValue("fromdb.password");
            
            SisVar.driverClassTo =      prop.getValue("todb.driverClass");
            SisVar.urlTo =              prop.getValue("todb.url");
            SisVar.usernameTo =         prop.getValue("todb.username");
            SisVar.passwordTo =         prop.getValue("todb.password");
            SisVar.integratedSecurityTo=prop.getValue("todb.integratedSecurity");
            
            SisVar.ambiente=            prop.getValue("setting.ambiente");
            SisVar.tipo=                prop.getValue("setting.tipo");
            SisVar.rutaConsulta=        prop.getValue("setting.rutaConsulta");
            SisVar.rutaFirma=           prop.getValue("setting.rutaFirma");
            SisVar.rutaGenerado=        prop.getValue("setting.rutaGenerado");
            SisVar.etiquetasInformacionAdicional= prop.getValue("setting.etiquetasInformacionAdicional");
            
    
            SisVar.rutaInicializacion=        prop.getValue("setting.rutaInicializacion");
            SisVar.archivoInicializacion=     prop.getValue("setting.archivoInicializacion");
            SisVar.rutaCertificadoInicializacion= prop.getValue("setting.rutaCertificadoInicializacion");
            SisVar.passwordCertificadoInicializacion= prop.getValue("setting.passwordCertificadoInicializacion");
            SisVar.rutaLogo= prop.getValue("setting.rutaLogo");
            SisVar.rutaReportes= prop.getValue("setting.rutaReportes");
            
            Propiedades.smtpHost    = prop.getValue("email.smtp_Host");
            Propiedades.smtpPort    = Integer.parseInt(prop.getValue("email.smtp_Port"));
            Propiedades.ssl         = Boolean.parseBoolean(prop.getValue("email.ssl"));
            Propiedades.correo      = prop.getValue("email.correo");
            Propiedades.pass        = prop.getValue("email.pass");
            Propiedades.cc          = prop.getValue("email.cc");
            Propiedades.bcc         = prop.getValue("email.bcc");
            Propiedades.adjunto     = Boolean.parseBoolean(prop.getValue("email.adjunto"));
            Propiedades.plantilla   = Boolean.parseBoolean(prop.getValue("email.plantilla"));            
        } catch (IOException ex) {
            Logger.getLogger(PropertiesLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static String cargarPantillaHtml(String plantillaHtml) throws IOException{
        File f= new File(plantillaHtml);
        if(f.exists()){
            BufferedReader reader= Files.newBufferedReader(f.toPath());
            return reader.lines().collect(Collectors.joining("\n"));            
        }
        return null;
    }
}
