/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

//import com.gma.camilafacturacion.util.HibernateUtil;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import jodd.datetime.JDateTimeDefault;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

/**
 *
 * @author Acer
 */
@WebListener()
public class ConfigServletListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");
        // timezone y locale default para el sistema
        TimeZone.setDefault(TimeZone.getTimeZone("America/Guayaquil"));
        JDateTimeDefault.timeZone = TimeZone.getTimeZone("America/Guayaquil");
        Locale.setDefault(new Locale("es", "EC"));

        SisVar.urlbaseFaces = sce.getServletContext().getContextPath() + "/faces/";

        DateConverter dateConverter = new DateConverter();
        dateConverter.setPattern("dd MM yyyy HH mm ss");
        ConvertUtils.register(dateConverter, java.util.Date.class);
        System.out.println("DATE CONVERTER REGISTERED camilafacturacion");

        // carga de propiedades
        PropertiesLoader props1 = new PropertiesLoader(sce.getServletContext());
        props1.load();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //
//        HibernateUtil.getSessionFactory().close();
        System.out.println("cerrando kiosko!!!");
    }
}
