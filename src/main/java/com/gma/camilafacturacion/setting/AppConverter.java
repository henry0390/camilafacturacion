/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.el.ELException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Acer
 */
@FacesConverter("entityConverter")
public class AppConverter implements Converter, Serializable{
    @EJB
    private EntityManagerLocal manager;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent c, String value) {
        if (value == null || value.isEmpty() == true || value.equals("")) {
            return null;
        }
        try {
            String[] p = value.split(":");
            if (p.length==1 ) {
                return null;
            }
            return this.manager.find(Class.forName(p[0]), Integer.valueOf(p[1]));
        } catch (NullPointerException | ELException | NumberFormatException e) {
            Logger.getLogger(AppConverter.class.getName()).log(Level.SEVERE, value, e);
        } catch (ClassNotFoundException | ConverterException ex) {
            Logger.getLogger(AppConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent c, Object value) {
        if (value == null) {
            return null;
        }
        return ReflexionEntity.getIdEntity(value);
    }
}
