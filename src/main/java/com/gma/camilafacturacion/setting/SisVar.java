/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

/**
 *
 * @author Acer
 */
public class SisVar {
    public static String urlbaseFaces;
    
    public static String driverClassFrom;
    public static String urlFrom;
    public static String usernameFrom;
    public static String passwordFrom;
    
    public static String driverClassTo;
    public static String urlTo;
    public static String usernameTo;
    public static String passwordTo;
    public static String integratedSecurityTo;
    
    public static String ambiente;
    public static String tipo;
    public static String rutaFirma;
    public static String rutaConsulta;
    public static String rutaGenerado;
    
    public static String rutaInicializacion;
    public static String archivoInicializacion;
    public static String rutaCertificadoInicializacion;
    public static String passwordCertificadoInicializacion;
    
    public static String etiquetasInformacionAdicional;
    public static String rutaLogo;
    public static String rutaReportes;
        
}
