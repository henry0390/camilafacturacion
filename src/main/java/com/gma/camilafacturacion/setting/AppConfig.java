/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Acer
 */
@Named(value = "appConfig")
@ApplicationScoped
public class AppConfig {

    /**
     * Creates a new instance of AppConfig
     */
    public AppConfig() {
    }
    
    public String getUrlbaseFaces() {
        return SisVar.urlbaseFaces;
    }
    
}
