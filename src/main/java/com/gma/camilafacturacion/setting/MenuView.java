/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.setting;

import com.gma.camilafacturacion.web.session.UsuarioSession;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Acer
 */
@Named(value = "menuView")
@SessionScoped
public class MenuView implements Serializable {
    @Inject 
    private UsuarioSession usuarioSession;
    
    private MenuModel model;

    public MenuView() {
        
    }
    
    @PostConstruct
    public void init(){
        this.model = new DefaultMenuModel();
        DefaultSubMenu nivel1Submenu = new DefaultSubMenu("CONSULTAS");
        nivel1Submenu.addElement(new DefaultMenuItem("CONSULTA ",null,"/faces/pages/consulta/consulta.xhtml"));        
        if(this.usuarioSession.getUsuario()!=null && this.usuarioSession.getUsuario().getIdentificacion().equalsIgnoreCase("administrador")){
            nivel1Submenu.addElement(new DefaultMenuItem("DOC. AUTORIZADOS ",null,"/faces/pages/entidades/documentos.xhtml"));
            nivel1Submenu.addElement(new DefaultMenuItem("USUARIOS ",null,"/faces/pages/entidades/usuarios.xhtml"));
        }
        this.model.addElement(nivel1Submenu);
        
        /*if(this.usuarioSession.getUsuario()!=null && this.usuarioSession.getUsuario().getIdentificacion().equalsIgnoreCase("administrador")){         
            DefaultSubMenu nivel2Submenu = new DefaultSubMenu("PROCESO");
            nivel2Submenu.addElement(new DefaultMenuItem("CARGAR DATOS",null,"/faces/pages/procesos/cargarDatos.xhtml"));
            nivel2Submenu.addElement(new DefaultMenuItem("AUTORIZAR ",null,"/faces/pages/procesos/autorizacion.xhtml"));
            this.model.addElement(nivel2Submenu);
        }*/
        if(this.usuarioSession.getUsuario()!=null && this.usuarioSession.getUsuario().getIdentificacion().equalsIgnoreCase("superadmin")){         
            DefaultSubMenu nivel3Submenu = new DefaultSubMenu("ADMINISTRADOR");
            nivel3Submenu.addElement(new DefaultMenuItem("REGISTRO DE EMPRESA",null,"/faces/pages/procesos/registroEmpresa.xhtml"));
            nivel3Submenu.addElement(new DefaultMenuItem("EMPRESAS",null,"/faces/pages/entidades/empresas.xhtml"));
            this.model.addElement(nivel3Submenu);
        }
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }
    
}
