/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.util;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.xml.sax.InputSource;

/**
 *
 * @author Acer
 */
public class Utilidades {
    public static synchronized boolean validarEmailConExpresion(String email) {
        return email!=null?validatePattern("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)*(\\.[A-Za-z]{1,})$", email):false;
    }
    
    public static synchronized boolean validatePattern(final String patron, final String valor) {
        final Pattern patter = Pattern.compile(patron);
        final Matcher matcher = patter.matcher(valor);
        return matcher.matches();
    }
    
    public static String getSubstring(String cadena){
        if(cadena!=null && cadena.length()>0)
            cadena=cadena.substring(0, cadena.length()-1);
        return cadena;
    }
    
    public static  <T> T getObjectByString(Class<T> clase, String cadena) throws JAXBException{        
        JAXBContext context = JAXBContext.newInstance(clase);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (T) unmarshaller.unmarshal(new InputSource(new StringReader(cadena)));
        
    }
    
    public static String fechaFormatoDeFechaString(String tipo, String fecha) throws ParseException{
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(tipo.equalsIgnoreCase("1")?new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy", Locale.ENGLISH).parse(fecha):
                        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH).parse(fecha.substring(0, fecha.length()-5)+fecha.substring(fecha.length()-5).replaceAll(":", "")));
                
    }
    
    public static String cargarPantillaHtml(String plantillaHtml){
        File f= new File(plantillaHtml);
        if(!f.exists())
            f= new File(SisVar.rutaReportes+"formato.html");
        if(f.exists()){
            BufferedReader reader;
            try {
                reader = Files.newBufferedReader(f.toPath());			
                return reader.lines().collect(Collectors.joining("\n"));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public static String setearValoresContentEmail(String content, TbAutorizados doc){
        return content.replaceAll("nombre_empresa", doc.getEmpresaId().getNombre())
                .replaceAll("nombre_cliente", doc.getUsuarioId().getNombre())
                .replaceAll("codigo_comprobante", doc.getNumeroDocumento())
                .replaceAll("valor_comprobante", doc.getTotal());
    }
}
