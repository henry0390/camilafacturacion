/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.util;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import com.gma.camilafacturacion.web.models.logica.SriRespuestaConsulta;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
//import ec.sri.firma.principal.SriIntegracion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class ConsultaSriRunnable implements Runnable{
    private FacturacionLocal facturacionService;
    private AutorizacionLocal autorizacionMainService;
    
    //private SriIntegracion sriIntegracion;
    private String claveAcceso;
    private String ambiente;
    private String tipo;
    private TbEmpresa empresa;
    private TbUsuario usuario;

    public ConsultaSriRunnable() {
    }

    /*public ConsultaSriRunnable(FacturacionLocal facturacionService, AutorizacionLocal autorizacionMainService, SriIntegracion sriIntegracion, 
            String claveAcceso, String ambiente, String tipo, TbEmpresa empresa, TbUsuario usuario) {
        this.facturacionService = facturacionService;
        this.autorizacionMainService = autorizacionMainService;
        this.sriIntegracion = sriIntegracion;
        this.claveAcceso = claveAcceso;
        this.ambiente = ambiente;
        this.tipo = tipo;
        this.empresa = empresa;
        this.usuario = usuario;
    } */       
    
    @Override
    public void run() {
        try{
      /*      this.sriIntegracion.setConsultaclaveAcceso(this.claveAcceso);
            this.sriIntegracion.setRutaManualArchivoRespuesta(SisVar.rutaConsulta.replaceAll("empresa", this.empresa.getIdentificacion())+claveAcceso+".xml");
            this.consultarSri();
            System.out.println(this.claveAcceso+"-"+this.sriIntegracion.getConsultaEstado()+" "+this.sriIntegracion.getConsultaNumeroAutorizacion()+" "+this.sriIntegracion.getConsultaFechaAutorizacion()+" *"+this.sriIntegracion.getConsultaMensajeAutorizacion());
            if("EN PROCESO".equalsIgnoreCase(this.sriIntegracion.getConsultaEstado())){
                Thread.sleep(3000);
                this.consultarSri();           
            }
            if("AUTORIZADO".equalsIgnoreCase(this.sriIntegracion.getConsultaEstado())){
                DocumentoAutorizado da = this.facturacionService.consultarDocumentoAutorizado(this.claveAcceso);
                String xml = this.facturacionService.consultarTextDocumentoAutorizado(da);                
                //VERIFICAR QUE SE EJECUTE PARA TODOS LOS DOCUMENTOS
                da.cargarComprobante();
                this.autorizacionMainService.grabarAutorizado(this.empresa, this.usuario, 
                        new SriRespuestaConsulta(this.sriIntegracion.getConsultaEstado(), this.sriIntegracion.getConsultaNumeroAutorizacion(), this.sriIntegracion.getConsultaFechaAutorizacion(), null, null, 
                                this.claveAcceso, 
                                da.getFactura()!=null?"FAC":da.getRetencion()!=null?"RET":"DOC" , 
                                da.getFactura()!=null?da.getFactura().getNumdoc():da.getRetencion()!=null?da.getRetencion().getNumdoc():"0", 
                                da.getFactura()!=null?da.getFactura().getInfoFactura().getImporteTotal()+"":da.getRetencion()!=null?da.getRetencion().getImpuestos().valorTotal()+"":"0")
                        ,xml);
                this.facturacionService.generarDocumentoPdf(this.claveAcceso, da.getFactura()!=null?"FAC":da.getRetencion()!=null?"RET":"DOC", SisVar.rutaLogo.replaceAll("empresa", this.empresa.getIdentificacion().trim())+this.empresa.getIdentificacion().trim()+".png",SisVar.rutaReportes);
            }*/
        } catch (Exception ex) {
            Logger.getLogger(ConsultaSriRunnable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void consultarSri(){
       /* switch (this.ambiente){
            case "1"://DESARROLLO
                switch (this.tipo){
                    case "1"://ONLINE
                        this.sriIntegracion.validaComprobante();
                        break;
                    case "2"://OFFLINE
                        this.sriIntegracion.validaComprobanteOffline();
                        break;
                    default:
                        break;
                }
                break;                    
            case "2"://PRODUCCION
                switch (this.tipo){
                    case "1"://ONLINE
                        this.sriIntegracion.validaComprobanteProduccion();
                        break;
                    case "2"://OFFLINE
                        this.sriIntegracion.validaComprobanteOfflineProd();
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }*/
    }
}
