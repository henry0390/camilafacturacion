/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.util;

import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import com.gma.camilafacturacion.web.service.local.AutorizacionLocal;
import com.gma.camilafacturacion.web.service.local.FacturacionLocal;
//import ec.sri.firma.principal.SriIntegracion;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;

/**
 *
 * @author Acer
 */
public class AutorizacionRunnable implements Runnable{
    private FacturacionLocal facturacionService;
    private AutorizacionLocal autorizacionMainService;
    
    private String ambiente;
    private String tipo;
    private String pathArchivo;
    private String claveAcceso;
    private TbEmpresa empresa;
    private TbUsuario usuario;

    public AutorizacionRunnable() {
    }

    public AutorizacionRunnable(String ambiente, String tipo, String pathArchivo, String claveAcceso,
            FacturacionLocal facturacionService, AutorizacionLocal autorizacionMainService,
            TbEmpresa empresa, TbUsuario usuario) {
        this.ambiente = ambiente;
        this.tipo = tipo;
        this.pathArchivo = pathArchivo;
        this.claveAcceso = claveAcceso;
        this.facturacionService = facturacionService;
        this.autorizacionMainService = autorizacionMainService;
        this.empresa = empresa;
        this.usuario = usuario;
    }
        
    
    @Override
    public void run() {
        //SriIntegracion sriIntegracion = new SriIntegracion();
        String respuesta="";
        try {
            switch (this.ambiente){
                case "1"://DESARROLLO
                    switch (this.tipo){
                        case "1"://ONLINE
                            //respuesta=sriIntegracion.enviaComprobante(this.pathArchivo);
                            break;
                        case "2"://OFFLINE
                            //respuesta=sriIntegracion.enviaComprobanteOffline(this.pathArchivo);
                            break;
                    }
                    break;                    
                case "2"://PRODUCCION
                    switch (this.tipo){
                        case "1"://ONLINE
                            //respuesta=sriIntegracion.enviaComprobanteProduccion(this.pathArchivo);
                            break;
                        case "2"://OFFLINE
                            //respuesta=sriIntegracion.enviaComprobanteOfflineProd(this.pathArchivo);
                            break;
                    }
                    break;
            }
            /*if(sriIntegracion.getListadoErrores()!=null && !sriIntegracion.getListadoErrores().isEmpty())
                sriIntegracion.getListadoErrores().forEach((e) -> {
                    System.out.println(this.claveAcceso+"\tID: "+e.getIdentificador()+"\tMNS: "+e.getMensaje()+"\tTP: "+e.getTipo()+"\tIA: "+e.getInformacionAdicional());
                });*/
            
            if("RECIBIDA".equalsIgnoreCase(respuesta)||"DEVUELTA".equalsIgnoreCase(respuesta)){
                /*Thread threadC = new Thread(new ConsultaSriRunnable(this.facturacionService, this.autorizacionMainService, sriIntegracion, 
                        this.claveAcceso, this.ambiente, this.tipo, this.empresa, this.usuario));
                threadC.start();*/
            }
            
        } catch (Exception ex) {
            Logger.getLogger(AutorizacionRunnable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
}
