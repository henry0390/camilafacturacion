/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Acer
 */
public class JsfUtil {
     public static String getRealPath(String subpath) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext().getRealPath(subpath);
    }

    public static Object getSessionBean(String sesionName) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                .get(sesionName);
    }

    public static Object setSessionBean(String sesionName, Object obj) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                .put(sesionName, obj);
    }

    public static void messageInfo(String id, String main, String desc) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_INFO, main, desc));
    }

    public static void messageWarning(String id, String main, String desc) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_WARN, main, desc));
    }

    public static void messageError(String id, String main, String desc) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_ERROR, main, desc));
    }

    public static void messageFatal(String id, String main, String desc) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(id, new FacesMessage(FacesMessage.SEVERITY_FATAL, main, desc));
    }

    public static void executeJS(String js) {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.execute(js);
    }

    public static void update(String target) {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(target);
    }

    public static void update(Collection<String> targets) {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(targets);
    }

    public static Boolean isAjaxRequest() {
        FacesContext fc = FacesContext.getCurrentInstance();
        return fc.isPostback();
    }

    public static void redirect(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(url /* "/home.xhtml" */);
        } catch (IOException ex) {
            Logger.getLogger(JsfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void redirectNewTab(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        RequestContext context = RequestContext.getCurrentInstance();

        context.execute("window.open('" + url + "', '_blank');");
    }

    
    public static void redirectFaces(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + url /* "/home.xhtml" */);
        } catch (IOException ex) {
            Logger.getLogger(JsfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }

    public static void redirectFaces2(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(url /* "/home.xhtml" */);
        } catch (IOException ex) {
            Logger.getLogger(JsfUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    public static void redirectFacesNewTab(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        RequestContext context = RequestContext.getCurrentInstance();

        context.execute("window.open('" + ec.getRequestContextPath() + url + "', '_newtab');");

    }

    
    public static void redirectMultipleConIP_V2(String urlMismaVentana, List<String> urlVentanasEmergentes) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        RequestContext context = RequestContext.getCurrentInstance();
        String urlCompleta = new String();
        if (urlMismaVentana != null) {
            if (urlVentanasEmergentes != null && !urlVentanasEmergentes.isEmpty()) {
                urlCompleta = "window.location='" + urlMismaVentana + "';";
                for (String url : urlVentanasEmergentes) {
                    urlCompleta = urlCompleta + "window.open('" + url + "', '_blank');";
                }
                context.execute(urlCompleta);
            } else {
                try {
                    ec.redirect(urlMismaVentana);
                } catch (IOException ex) {
                    Logger.getLogger(JsfUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            if (urlVentanasEmergentes != null && !urlVentanasEmergentes.isEmpty()) {
                for (String url : urlVentanasEmergentes) {
                    urlCompleta = urlCompleta + "window.open('" + url + "', '_blank');";
                }
                context.execute(urlCompleta);
            }
        }
    }
    
    public static String getIpCliente(){
        HttpServletRequest request=(HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) 
            ipAddress = request.getRemoteAddr();
        return ipAddress;
    }
    
    public static String getNamePC() throws UnknownHostException{
        InetAddress localHost= InetAddress.getLocalHost();
        return localHost.getHostName().toUpperCase();
    }
    
    public static void updateexecuteJS(String up,String js) {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update(up);
        rc.execute(js);
    }
}
