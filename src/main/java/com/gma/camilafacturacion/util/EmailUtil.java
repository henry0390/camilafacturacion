/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

/**
 *
 * @author Acer
 */
public class EmailUtil implements Serializable {

    private String to;
    private String subject;
    private String content;
    private String bcc;
    private String cc;

    private String mailServerHost;
    private int mailServerPort;
    private String mailServerUsername;
    private String mailServerPassword;
    private boolean mailServerUseSSL;

    public EmailUtil() {
        this.mailServerHost = Propiedades.smtpHost;
        this.mailServerPort = Propiedades.smtpPort;
        this.mailServerUsername = Propiedades.correo;
        this.mailServerPassword = Propiedades.pass;
        this.mailServerUseSSL = Propiedades.ssl;
    }

    public Boolean sendEmail(String to, String subject, String content, String bcc, String cc, String ruta, String archivo) {
        try {
            if (this.mailServerUseSSL == true) {
                return this.sendAuthEmail(to, subject, content, bcc, cc, ruta, archivo);
            } else {
                return this.sendBasicEmail(to, subject, content, bcc, cc);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailUtil.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public Boolean sendBasicEmail(String to, String subject, String content, String bcc, String cc) {
        boolean basic = true;
        try {
            Integer port = this.mailServerPort;
            Properties props = new Properties();
//            props.setProperty("mail.smtp.host", this.mailServerHost);
//            props.setProperty("mail.smtp.port", port.toString());
//            Session sess = Session.getInstance(props, null);
//            Message message = new MimeMessage(sess);
//            message.setFrom(new InternetAddress(this.mailServerUsername));
//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
//            message.setSentDate(new Date());
//            if (bcc != null) {
//                message.addRecipients(Message.RecipientType.BCC,
//                        InternetAddress.parse(bcc));
//            }
//            if (cc != null) {
//                message.addRecipients(Message.RecipientType.CC,
//                        InternetAddress.parse(cc));
//            }
//            message.setText(content);
//            message.setSubject(subject + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()));
//            message.setContent(content, "text/html; charset=utf-8");
//            Transport.send(message);

        } catch (Exception e) {
            basic = false;
            Logger.getLogger(EmailUtil.class.getName()).log(Level.SEVERE, null, e);
        }
        return basic;
    }

    public Boolean sendAuthEmail(String to, String subject, String content, String bcc, String cc, String ruta, String archivo) {
        boolean auth = true;
        try {
            MimeMultipart mm = new MimeMultipart("alternative");
            HtmlEmail email = new HtmlEmail();
            //MultiPartEmail email = new MultiPartEmail();
            email.setHostName(this.mailServerHost);
            email.setSmtpPort(this.mailServerPort);
            email.setSSLOnConnect(this.mailServerUseSSL);
            email.setAuthenticator(new DefaultAuthenticator(this.mailServerUsername, this.mailServerPassword));
            email.setBoolHasAttachments(Boolean.TRUE);
            email.setDebug(Boolean.FALSE);
            email.setStartTLSEnabled(Boolean.TRUE);
            email.setStartTLSRequired(Boolean.TRUE);
            email.setFrom(this.mailServerUsername, "GMA");
            //email.addTo(to, "Hong");
            String[] listCorreos=  to.split(",");
            for (int i = 0; i < listCorreos.length; i++) {
                email.addTo(listCorreos[i]);
            }
            email.setCharset("utf-8");
            email.setSubject(subject + " " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()));
            EmailAttachment pdf = new EmailAttachment();
            EmailAttachment xml = new EmailAttachment();
            if (Propiedades.adjunto) {
//                FileDataSource fdspdf = new FileDataSource(ruta + archivo + ".pdf");
                pdf.setPath(ruta + archivo + ".pdf");
                pdf.setName(archivo + ".pdf");
                pdf.setDescription("FACTURA FORMATO PDF");
                pdf.setDisposition(EmailAttachment.ATTACHMENT);
//                System.out.println("pdf " + pdf.getPath() + " size " + fdspdf.getFile().length());
                email.attach(pdf);
//                MimeBodyPart pdfmbp = new MimeBodyPart();
//                pdfmbp.attachFile(fdspdf.getFile());
//                mm.addBodyPart(pdfmbp);

//                FileDataSource fdsxml = new FileDataSource(ruta + archivo + ".xml");
                xml.setPath(ruta + archivo + ".xml");
                xml.setName(archivo + ".xml");
                xml.setDisposition(EmailAttachment.ATTACHMENT);
                xml.setDescription("FACTURA FORMATO XML");
//                System.out.println("xml " + xml.getPath() + " size " + fdsxml.getFile().length());
                email.attach(xml);
//                MimeBodyPart xmlmbp = new MimeBodyPart();
//                xmlmbp.attachFile(fdsxml.getFile());
//                mm.addBodyPart(xmlmbp);
            }
            if (bcc != null && bcc.length() > 0) {
                email.addBcc(bcc);
            }
            if (cc != null && cc.length() > 0) {
                email.addCc(cc);
            }
            email.setSentDate(new Date());
            //email.setMsg(content != null && !content.isEmpty() ? content : "DOCUMENTOS ELECTRONICOS");
            email.setHtmlMsg(content);
            //email.setHtmlMsg("<html>The apache logo </html>");
//            email.setContent(mm);
            email.send();
            System.out.println("CF Enviando email a ..... " + to);
        } catch (Exception e) {
            auth = false;
            Logger.getLogger(EmailUtil.class.getName()).log(Level.SEVERE, null, e);
        }
        return auth;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getMailServerHost() {
        return mailServerHost;
    }

    public void setMailServerHost(String mailServerHost) {
        this.mailServerHost = mailServerHost;
    }

    public int getMailServerPort() {
        return mailServerPort;
    }

    public void setMailServerPort(int mailServerPort) {
        this.mailServerPort = mailServerPort;
    }

    public String getMailServerUsername() {
        return mailServerUsername;
    }

    public void setMailServerUsername(String mailServerUsername) {
        this.mailServerUsername = mailServerUsername;
    }

    public String getMailServerPassword() {
        return mailServerPassword;
    }

    public void setMailServerPassword(String mailServerPassword) {
        this.mailServerPassword = mailServerPassword;
    }

    public boolean isMailServerUseSSL() {
        return mailServerUseSSL;
    }

    public void setMailServerUseSSL(boolean mailServerUseSSL) {
        this.mailServerUseSSL = mailServerUseSSL;
    }

}
