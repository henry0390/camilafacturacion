/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.servlets;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.setting.SisVar;
import com.gma.camilafacturacion.util.EjbsCaller;
import com.gma.camilafacturacion.util.Utilidades;
import com.gma.camilafacturacion.web.models.consulta.DocumentoAutorizado;
import com.gma.camilafacturacion.web.service.implement.FacturacionService;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Ing. Carlos Loor
 */
@WebServlet(name = "documentoDigital", urlPatterns = {"/documentoDigital"})
public class DescargarDocumento extends HttpServlet {

    private EntityManagerLocal manager;

    private static final Logger log = Logger.getLogger(DescargarDocumento.class.getName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("key") != null) {
            getDocumento(req, resp, req.getParameter("key"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("key") != null) {
            getDocumento(req, resp, req.getParameter("key"));
        }
    }

    private void getDocumento(HttpServletRequest req, HttpServletResponse resp, String secuencia) {
        HttpServletResponse response;
        OutputStream outputStream = null;
        Map<String, Object> parametros = new HashMap<>();
        try {
            manager = EjbsCaller.getManager();
            parametros.put("secuencia", secuencia);
            TbAutorizados documento = (TbAutorizados) manager.findObject(TbAutorizados.class, parametros);
            if (documento != null) {
                parametros = new HashMap<>();
                DocumentoAutorizado docAut = Utilidades.getObjectByString(DocumentoAutorizado.class, documento.getTbDocumentoXML().getDocumentoXML());
                docAut.cargarComprobante();
                List<DocumentoAutorizado> lf = new ArrayList<>();
                lf.add(docAut);
                JRDataSource dataSource = new JRBeanCollectionDataSource(lf);
                parametros.put("LOGO", SisVar.rutaLogo.replaceAll("empresa", documento.getEmpresaId().getIdentificacion()) + documento.getEmpresaId().getIdentificacion().trim() + ".png");
                parametros.put("SUBREPORT_DIR", SisVar.rutaReportes);
                parametros.put("FECHA_AUTORIZADO", Utilidades.fechaFormatoDeFechaString(SisVar.tipo, docAut.getFechaAutorizacion()));
                byte[] bytes = JasperExportManager.exportReportToPdf(
                        JasperFillManager.fillReport(
                                SisVar.rutaReportes + (documento.getDocumento().equalsIgnoreCase("FAC") ? "factura"
                                : documento.getDocumento().equalsIgnoreCase("RET") ? "retencion"
                                : documento.getDocumento().equalsIgnoreCase("NCR") ? "notaCredito"
                                : documento.getDocumento().equalsIgnoreCase("GUI") ? "guiaRemision" : "factura") + ".jasper",
                                parametros,
                                dataSource
                        )
                );
                if (bytes == null) {
                    return;
                }
                response = resp;
                response.reset();
                response.setContentType("application/pdf");
                response.setCharacterEncoding("UTF-8");
                response.setHeader("Content-disposition", "attachment; filename=" + documento.getDocumento() + "_" + docAut.getNumeroDocumento() + ".pdf");
                response.setContentLength(bytes.length);
                outputStream = response.getOutputStream();
                outputStream.write(bytes, 0, bytes.length);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }

    }

}
