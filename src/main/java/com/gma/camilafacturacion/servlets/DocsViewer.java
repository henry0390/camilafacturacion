/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ing. Carlos Loor
 */
@Named
@WebServlet(name = "docsViewer", urlPatterns = {"/docsViewer"})
public class DocsViewer extends HttpServlet {


    private static final Logger log = Logger.getLogger(DocsViewer.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getFile(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getFile(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "OMEGA-ICL";
    }// </editor-fold>

    protected void getFile(HttpServletRequest request, HttpServletResponse response) {
        File f = null, aux = null;
        try {
            if (request.getParameter("cod") != null && !request.getParameter("cod").isEmpty()) {
                String k = new String(Base64.getDecoder().decode(request.getParameter("cod"))).replace("\\", "//");
                f = new File(k.replace("\\", "/"));
                // System.out.println("content-type " + Files.probeContentType(f.toPath()));                
                Scanner s = new Scanner(f);
                if (f.exists()) {
                    response.setHeader("Content-Type", getServletContext().getMimeType(Files.probeContentType(f.toPath())));
                    response.setHeader("Content-Length", String.valueOf(f.length()));
                    response.setHeader("Content-Disposition", "inline; filename=\"" + f.getName() + "\"");
                    response.setContentType(getServletContext().getMimeType(null));
                    Files.copy(f.toPath(), response.getOutputStream());
                }
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, null, e);
        }
    }

}
