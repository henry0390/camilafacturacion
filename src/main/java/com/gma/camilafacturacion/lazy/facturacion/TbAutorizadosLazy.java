/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.lazy.facturacion;

import com.gma.camilafacturacion.entidades.facturacion.TbAutorizados;
import com.gma.camilafacturacion.entidades.facturacion.TbEmpresa;
import java.util.Date;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class TbAutorizadosLazy extends BaseLazyDataModel<TbAutorizados>{
    private TbEmpresa empresa;
    private String usuarioIdentificacion;
    private String documento;
    private String numeroDocumento;
    private String claveAcceso;
    private Date fecha;
    private Date desde;
    private Date hasta;

    public TbAutorizadosLazy() {
        super(TbAutorizados.class, "id", "DESC");
    }

    public TbAutorizadosLazy(TbEmpresa empresa, String usuarioIdentificacion, String documento, String numeroDocumento, String claveAcceso, Date fecha) {
        super(TbAutorizados.class, "id", "DESC");
        this.empresa = empresa;
        this.usuarioIdentificacion = usuarioIdentificacion;
        this.documento = documento;
        this.numeroDocumento = numeroDocumento;
        this.claveAcceso = claveAcceso;
        this.fecha = fecha;
    }
    
    public TbAutorizadosLazy(TbEmpresa empresa, Date desde, Date hasta) {
        super(TbAutorizados.class, "id", "ASC");
        this.empresa = empresa;
        this.desde = desde;
        this.hasta = hasta;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.empresa!=null)
            crit.add(Restrictions.eq("empresaId", this.empresa));
        if(this.usuarioIdentificacion!=null && this.usuarioIdentificacion!="")
            crit.createCriteria("usuarioId").add(Restrictions.eq("identificacion", this.usuarioIdentificacion));
        if(this.documento!=null && this.documento!="")
            crit.add(Restrictions.eq("documento", this.documento));
        if(this.numeroDocumento!=null && this.numeroDocumento!="")
            crit.add(Restrictions.eq("numeroDocumento", this.numeroDocumento));
        if(this.claveAcceso!=null && this.claveAcceso != "")
            crit.add(Restrictions.eq("claveAcceso", this.claveAcceso));
        if(this.fecha!=null){
            crit.add(Restrictions.eq("fechaEmision", this.fecha));
        }
        if(this.desde != null && this.hasta != null){
            crit.add(Restrictions.between("fechaEmision", this.desde, this.hasta));
        }
        if (filters.containsKey("id"))
            crit.add(Restrictions.eq("id", new Integer(filters.get("id").toString())));
        if (filters.containsKey("documento"))
            crit.add(Restrictions.ilike("documento", "%"+filters.get("documento")+"%"));
        if (filters.containsKey("numeroDocumento"))
            crit.add(Restrictions.ilike("numeroDocumento", "%"+filters.get("numeroDocumento")+"%"));
        if (filters.containsKey("identificacion"))
            crit.createCriteria("usuarioId").add(Restrictions.eq("identificacion", filters.get("identificacion")));
        if (filters.containsKey("nombre"))
            crit.createCriteria("usuarioId").add(Restrictions.ilike("nombre", "%"+filters.get("nombre")+"%"));
        
    }
}
