/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.lazy.facturacion;

import com.gma.camilafacturacion.util.EjbsCaller;
//import com.gma.camilafacturacion.util.HiberUtil;
import com.gma.camilafacturacion.web.service.local.EntityManagerLocal;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Acer
 */
public class BaseLazyDataModel<T extends Object> extends LazyDataModel<T> {

    private EntityManagerLocal manager;
    private Class<T> entity;
    private int rowCount = 0;
    private String defaultSorted = "id";
    private String defaultSortOrder = "ASC";

    private String defaultSorted2;
    private String defaultSortOrder2;

    private Criteria orderCrit;
    private String orderField;

    public BaseLazyDataModel() {
        manager = EjbsCaller.getManager();
    }

    public BaseLazyDataModel(Class<T> entity) {
        this.entity = entity;
        manager = EjbsCaller.getManager();
    }

    public BaseLazyDataModel(Class<T> entity, String defaultSorted) {
        this.entity = entity;
        this.defaultSorted = defaultSorted;
        manager = EjbsCaller.getManager();
    }

    public BaseLazyDataModel(Class<T> entity, String defaultSorted, String defaultSortOrder) {
        this.entity = entity;
        this.defaultSorted = defaultSorted;
        this.defaultSortOrder = defaultSortOrder;
        manager = EjbsCaller.getManager();
    }

    public BaseLazyDataModel(Class<T> entity, String defaultSorted, String defaultSortOrder, String defaultSorted2, String defaultSortOrder2) {
        this.entity = entity;
        this.defaultSorted = defaultSorted;
        this.defaultSortOrder = defaultSortOrder;

        this.defaultSorted2 = defaultSorted2;
        this.defaultSortOrder2 = defaultSortOrder2;
        manager = EjbsCaller.getManager();
    }

    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        //put your code here
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        Criteria cq, dcq;
        try {
            cq = (manager.getEm().unwrap(Session.class)).createCriteria(this.getEntity(), "entity");
            this.criteriaFilterSetup(cq, filters);
            cq.setProjection(Projections.projectionList().add(Projections.rowCount()));
            dcq = (manager.getEm().unwrap(Session.class)).createCriteria(this.getEntity(), "entity1");

            this.criteriaFilterSetup(dcq, filters);
            if (orderCrit != null) {
                this.criteriaSortSetup(orderCrit, orderField, sortOrder);
            } else {
                this.criteriaSortSetup(dcq, sortField, sortOrder);
            }
            this.criteriaPageSetup(dcq, first, pageSize);
            rowCount = 0;
            rowCount = ((Long) cq.uniqueResult()).intValue();
            this.setRowCount(rowCount);
            return dcq.list();
        } catch (Exception ex) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        List result = null;
        Criteria cq, dcq;
        try {
            Session sess = manager.getEm().unwrap(Session.class);
            cq = sess.createCriteria(this.getEntity(), "entity");
            this.criteriaFilterSetup(cq, filters);
            cq.setProjection(Projections.projectionList().add(Projections.rowCount()));
            dcq = sess.createCriteria(this.getEntity(), "entity1");

            this.criteriaFilterSetup(dcq, filters);
            if (orderCrit != null) {
                if (multiSortMeta != null) {
                    for (SortMeta sortOrder : multiSortMeta) {
                        this.criteriaSortSetup(orderCrit, sortOrder.getSortField(), sortOrder.getSortOrder());
                    }
                }
            } else {
                if (multiSortMeta != null) {
                    for (SortMeta sortOrder : multiSortMeta) {
                        this.criteriaSortSetup(dcq, sortOrder.getSortField(), sortOrder.getSortOrder());
                    }
                }
            }
            this.criteriaPageSetup(dcq, first, pageSize);
            rowCount = 0;
            rowCount = ((Long) cq.uniqueResult()).intValue();
            this.setRowCount(rowCount);
            result = dcq.list();
            Hibernate.initialize(result);
        } catch (Exception ex) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public T getRowData(Object key) {
        T ob = null;
        try {
            ob = (T) manager.find(entity, (Serializable) key);
            Hibernate.initialize(ob);
        } catch (Exception e) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, e);
        }
        return ob;
    }

    @Override
    public T getRowData(String rowKey) {
        T ob = null;
        Object x = rowKey;
        try {
            try {
                Long id = Long.parseLong(rowKey);
                ob = (T) manager.find(entity, id);
            } catch (NumberFormatException ex) {
                Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            Hibernate.initialize(ob);
        } catch (Exception e) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, e);
        }
        return ob;
    }

    @Override
    public void setRowIndex(int rowIndex) {
        try {
            if (rowIndex == -1 || getPageSize() == 0) {
                super.setRowIndex(-1);
            } else {
                super.setRowIndex(rowIndex % getPageSize());
            }
        } catch (Exception e) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void criteriaPageSetup(Criteria crit, int first, int pageSize) {
        try {
            crit.setFirstResult(first);
            crit.setMaxResults(pageSize);
        } catch (Exception e) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void criteriaSortSetup(Criteria crit, String field, SortOrder order) {
        try {
            if (field == null) {
                crit.addOrder((defaultSortOrder.equalsIgnoreCase("ASC")) ? Order.asc(defaultSorted) : Order.desc(defaultSorted));
                if (this.defaultSorted2 != null && this.defaultSortOrder2 != null) {
                    crit.addOrder((defaultSortOrder2.equalsIgnoreCase("ASC")) ? Order.asc(defaultSorted2) : Order.desc(defaultSorted2));
                }

            } else {
                if (order.equals(SortOrder.ASCENDING)) {
                    crit.addOrder(Order.asc(field));
                } else {
                    crit.addOrder(Order.desc(field));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(BaseLazyDataModel.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Class<T> getEntity() {
        return entity;
    }

    public void setEntity(Class<T> entity) {
        this.entity = entity;
    }

    public String getDefaultSorted() {
        return defaultSorted;
    }

    public void setDefaultSorted(String defaultSorted) {
        this.defaultSorted = defaultSorted;
    }

    public String getDefaultSortOrder() {
        return defaultSortOrder;
    }

    public void setDefaultSortOrder(String defaultSortOrder) {
        this.defaultSortOrder = defaultSortOrder;
    }

    public Criteria getOrderCrit() {
        return orderCrit;
    }

    public void setOrderCrit(Criteria orderCrit) {
        this.orderCrit = orderCrit;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }
}
