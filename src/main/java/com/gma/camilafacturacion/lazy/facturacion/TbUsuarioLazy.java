/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.lazy.facturacion;

import com.gma.camilafacturacion.entidades.facturacion.TbUsuario;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class TbUsuarioLazy extends BaseLazyDataModel<TbUsuario>{
    
    public TbUsuarioLazy() {
        super(TbUsuario.class,"id","ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("id")) {
            crit.add(Restrictions.eq("id", new Long(filters.get("id").toString().trim())));
        }
        if (filters.containsKey("identificacion")) {
            crit.add(Restrictions.ilike("identificacion", "%"+filters.get("identificacion").toString().trim()+"%"));
        }
        if (filters.containsKey("nombre")) {
            crit.add(Restrictions.ilike("nombre", "%"+filters.get("nombre").toString().trim()+"%"));
        }
    }
}
