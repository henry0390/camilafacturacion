/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.entidades.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Embeddable
public class TbParametrosEmpresaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "EmpresaId")
    private int empresaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Codigo")
    private String codigo;

    public TbParametrosEmpresaPK() {
    }

    public TbParametrosEmpresaPK(int empresaId, String codigo) {
        this.empresaId = empresaId;
        this.codigo = codigo;
    }

    public int getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(int empresaId) {
        this.empresaId = empresaId;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) empresaId;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbParametrosEmpresaPK)) {
            return false;
        }
        TbParametrosEmpresaPK other = (TbParametrosEmpresaPK) object;
        if (this.empresaId != other.empresaId) {
            return false;
        }
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilafacturacion.entidades.facturacion.TbParametrosEmpresaPK[ empresaId=" + empresaId + ", codigo=" + codigo + " ]";
    }
    
}
