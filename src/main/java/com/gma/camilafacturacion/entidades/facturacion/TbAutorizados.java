/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.entidades.facturacion;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "tbAutorizados", schema = "autorizacion")
@NamedQueries({
    @NamedQuery(name = "TbAutorizados.findAll", query = "SELECT t FROM TbAutorizados t")})
public class TbAutorizados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ClaveAcceso")
    private String claveAcceso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NumeroAutorizacion")
    private String numeroAutorizacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "Documento")
    private String documento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaEmision")
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaAutorizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAutorizacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ambiente")
    private int ambiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NumeroDocumento")
    private String numeroDocumento;
    @Column(name = "secuencia")
    private String secuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Total")
    private String total;
    @JoinColumn(name = "EmpresaId", referencedColumnName = "Id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbEmpresa empresaId;
    @JoinColumn(name = "UsuarioId", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private TbUsuario usuarioId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "tbAutorizados", fetch = FetchType.LAZY)
    private TbDocumentoXML tbDocumentoXML;

    public TbAutorizados() {
    }

    public TbAutorizados(Long id) {
        this.id = id;
    }

    public TbAutorizados(Long id, String claveAcceso, String numeroAutorizacion, String documento, Date fechaEmision, Date fechaAutorizacion, int ambiente, String numeroDocumento, String total) {
        this.id = id;
        this.claveAcceso = claveAcceso;
        this.numeroAutorizacion = numeroAutorizacion;
        this.documento = documento;
        this.fechaEmision = fechaEmision;
        this.fechaAutorizacion = fechaAutorizacion;
        this.ambiente = ambiente;
        this.numeroDocumento = numeroDocumento;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public int getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(int ambiente) {
        this.ambiente = ambiente;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public TbEmpresa getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(TbEmpresa empresaId) {
        this.empresaId = empresaId;
    }

    public TbUsuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(TbUsuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    public TbDocumentoXML getTbDocumentoXML() {
        return tbDocumentoXML;
    }

    public void setTbDocumentoXML(TbDocumentoXML tbDocumentoXML) {
        this.tbDocumentoXML = tbDocumentoXML;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbAutorizados)) {
            return false;
        }
        TbAutorizados other = (TbAutorizados) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilafacturacion.entidades.facturacion.TbAutorizados[ id=" + id + " ]";
    }

}
