/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.entidades.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "tbDocumentoXML", schema = "autorizacion")
@NamedQueries({
    @NamedQuery(name = "TbDocumentoXML.findAll", query = "SELECT t FROM TbDocumentoXML t")})
public class TbDocumentoXML implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AutorizadoId")
    private Long autorizadoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DocumentoXML")
    private String documentoXML;
    @JoinColumn(name = "AutorizadoId", referencedColumnName = "Id", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private TbAutorizados tbAutorizados;

    public TbDocumentoXML() {
    }

    public TbDocumentoXML(Long autorizadoId) {
        this.autorizadoId = autorizadoId;
    }

    public TbDocumentoXML(Long autorizadoId, String documentoXML) {
        this.autorizadoId = autorizadoId;
        this.documentoXML = documentoXML;
    }

    public Long getAutorizadoId() {
        return autorizadoId;
    }

    public void setAutorizadoId(Long autorizadoId) {
        this.autorizadoId = autorizadoId;
    }

    public String getDocumentoXML() {
        return documentoXML;
    }

    public void setDocumentoXML(String documentoXML) {
        this.documentoXML = documentoXML;
    }

    public TbAutorizados getTbAutorizados() {
        return tbAutorizados;
    }

    public void setTbAutorizados(TbAutorizados tbAutorizados) {
        this.tbAutorizados = tbAutorizados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autorizadoId != null ? autorizadoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDocumentoXML)) {
            return false;
        }
        TbDocumentoXML other = (TbDocumentoXML) object;
        if ((this.autorizadoId == null && other.autorizadoId != null) || (this.autorizadoId != null && !this.autorizadoId.equals(other.autorizadoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilafacturacion.entidades.facturacion.TbDocumentoXML[ autorizadoId=" + autorizadoId + " ]";
    }

}
