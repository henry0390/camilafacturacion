/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.entidades.facturacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "tbParametrosEmpresa", schema = "autorizacion")
@NamedQueries({
    @NamedQuery(name = "TbParametrosEmpresa.findAll", query = "SELECT t FROM TbParametrosEmpresa t")})
public class TbParametrosEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TbParametrosEmpresaPK tbParametrosEmpresaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tipo")
    private int tipo;
    @Column(name = "Entero")
    private Integer entero;
    @Size(max = 2147483647)
    @Column(name = "Cadena")
    private String cadena;
    @Column(name = "Buleano")
    private Boolean buleano;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Decimal")
    private BigDecimal decimal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Estado")
    private boolean estado;

    public TbParametrosEmpresa() {
    }

    public TbParametrosEmpresa(TbParametrosEmpresaPK tbParametrosEmpresaPK) {
        this.tbParametrosEmpresaPK = tbParametrosEmpresaPK;
    }

    public TbParametrosEmpresa(TbParametrosEmpresaPK tbParametrosEmpresaPK, String descripcion, int tipo, boolean estado) {
        this.tbParametrosEmpresaPK = tbParametrosEmpresaPK;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.estado = estado;
    }

    public TbParametrosEmpresa(int empresaId, String codigo) {
        this.tbParametrosEmpresaPK = new TbParametrosEmpresaPK(empresaId, codigo);
    }

    public TbParametrosEmpresaPK getTbParametrosEmpresaPK() {
        return tbParametrosEmpresaPK;
    }

    public void setTbParametrosEmpresaPK(TbParametrosEmpresaPK tbParametrosEmpresaPK) {
        this.tbParametrosEmpresaPK = tbParametrosEmpresaPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public Integer getEntero() {
        return entero;
    }

    public void setEntero(Integer entero) {
        this.entero = entero;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public Boolean getBuleano() {
        return buleano;
    }

    public void setBuleano(Boolean buleano) {
        this.buleano = buleano;
    }

    public BigDecimal getDecimal() {
        return decimal;
    }

    public void setDecimal(BigDecimal decimal) {
        this.decimal = decimal;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbParametrosEmpresaPK != null ? tbParametrosEmpresaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbParametrosEmpresa)) {
            return false;
        }
        TbParametrosEmpresa other = (TbParametrosEmpresa) object;
        if ((this.tbParametrosEmpresaPK == null && other.tbParametrosEmpresaPK != null) || (this.tbParametrosEmpresaPK != null && !this.tbParametrosEmpresaPK.equals(other.tbParametrosEmpresaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilafacturacion.entidades.facturacion.TbParametrosEmpresa[ tbParametrosEmpresaPK=" + tbParametrosEmpresaPK + " ]";
    }
    
}
