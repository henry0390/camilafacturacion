/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilafacturacion.webservice;

import java.io.Serializable;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Acer
 */
@Path("/servicio")
@RequestScoped
public class ServicioAutorizacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ServicioAutorizacion
     */
    public ServicioAutorizacion() {
    }

    /**
     * Retrieves representation of an instance of
     * com.gma.camilafacturacion.webservice.ServicioAutorizacion
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        System.out.println("WEB SERVICE .......");
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of ServicioAutorizacion
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
