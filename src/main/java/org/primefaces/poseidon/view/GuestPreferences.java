/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.primefaces.poseidon.view;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;

/**
 *
 * @author Acer
 */
@Named(value = "guestPreferences")
@SessionScoped
public class GuestPreferences implements Serializable {

    private Map<String,String> themeColors;    
    private String theme = "turquoise";        
    private String menuLayout = "static";
        
    @PostConstruct
    public void init() {
        themeColors = new HashMap<>();
        themeColors.put("turquoise", "#00acac");
        themeColors.put("blue", "#2f8ee5");
        themeColors.put("orange", "#efa64c");
        themeColors.put("purple", "#6c76af");
        themeColors.put("pink", "#f16383");
        themeColors.put("light-blue", "#63c9f1");
        themeColors.put("green", "#57c279");
        themeColors.put("deep-purple", "#7e57c2");
        themeColors.put("red", "#ff0000");
    }    	
    
    public String getMenuLayout() {	
        if(this.menuLayout.equals("static"))
            return "menu-layout-static";
        else if(this.menuLayout.equals("overlay"))
            return "menu-layout-overlay";
        else if(this.menuLayout.equals("horizontal"))
            return "menu-layout-static menu-layout-horizontal";
        else
            return "menu-layout-static";
    }
    
    public void setMenuLayout(String menuLayout) {
        this.menuLayout = menuLayout;
    }
    
    public String getTheme() {		
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }        
    
    public Map getThemeColors() {
        return this.themeColors;
    }
    
}
